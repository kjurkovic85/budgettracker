enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")
enableFeaturePreview("VERSION_CATALOGS")

rootProject.name = "BudgetTracker"

include (
    ":app",

    ":common:annotations",
    ":common:framework:initializers",
    ":common:framework:mvi",
    ":common:framework:navigation",
    ":common:resources",
    ":common:ui",
    ":common:wrappers:dispatchers",
    ":common:wrappers:logger",

    ":data:common",
    ":data:database",
    ":data:datasource:category-datasource",
    ":data:datasource:transaction-datasource",
    ":data:repo:category-repo",
    ":data:repo:transaction-repo",

    ":screens:home",
    ":screens:dashboard",
    ":screens:categories",
    ":screens:transactions",
)

fun configureBuildFiles(project: ProjectDescriptor) {
    project.children.forEach { child ->
        child.buildFileName = "${child.name}.gradle.kts"
        configureBuildFiles(child)
    }
}

configureBuildFiles(rootProject)
