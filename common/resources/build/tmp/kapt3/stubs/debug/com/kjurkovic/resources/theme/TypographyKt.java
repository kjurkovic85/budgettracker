package com.kjurkovic.resources.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"AppTypography", "Landroidx/compose/material/Typography;", "getAppTypography", "()Landroidx/compose/material/Typography;", "GTAmericaMedium", "Landroidx/compose/ui/text/font/FontFamily;", "GTAmericaRegular", "resources_debug"})
public final class TypographyKt {
    private static final androidx.compose.ui.text.font.FontFamily GTAmericaRegular = null;
    private static final androidx.compose.ui.text.font.FontFamily GTAmericaMedium = null;
    @org.jetbrains.annotations.NotNull()
    private static final androidx.compose.material.Typography AppTypography = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final androidx.compose.material.Typography getAppTypography() {
        return null;
    }
}