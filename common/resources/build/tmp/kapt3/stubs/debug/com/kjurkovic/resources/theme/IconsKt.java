package com.kjurkovic.resources.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028G\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\u00a8\u0006\u0005"}, d2 = {"icons", "Landroidx/compose/material/icons/Icons$Filled;", "Landroidx/compose/material/MaterialTheme;", "getIcons", "(Landroidx/compose/material/MaterialTheme;)Landroidx/compose/material/icons/Icons$Filled;", "resources_debug"})
public final class IconsKt {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.compose.runtime.ReadOnlyComposable()
    @androidx.compose.runtime.Composable()
    public static final androidx.compose.material.icons.Icons.Filled getIcons(@org.jetbrains.annotations.NotNull()
    androidx.compose.material.MaterialTheme $this$icons) {
        return null;
    }
}