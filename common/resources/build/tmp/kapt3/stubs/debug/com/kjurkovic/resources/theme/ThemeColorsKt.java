package com.kjurkovic.resources.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003\"\u0014\u0010\u0004\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0003\"\u001a\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00010\u0007X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\"\u0015\u0010\n\u001a\u00020\u0001*\u00020\u000b8G\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"DarkThemeColors", "Lcom/kjurkovic/resources/theme/ThemeColors;", "getDarkThemeColors", "()Lcom/kjurkovic/resources/theme/ThemeColors;", "LightThemeColors", "getLightThemeColors", "LocalColors", "Landroidx/compose/runtime/ProvidableCompositionLocal;", "getLocalColors", "()Landroidx/compose/runtime/ProvidableCompositionLocal;", "allColors", "Landroidx/compose/material/MaterialTheme;", "getAllColors", "(Landroidx/compose/material/MaterialTheme;)Lcom/kjurkovic/resources/theme/ThemeColors;", "resources_debug"})
public final class ThemeColorsKt {
    @org.jetbrains.annotations.NotNull()
    private static final com.kjurkovic.resources.theme.ThemeColors LightThemeColors = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.kjurkovic.resources.theme.ThemeColors DarkThemeColors = null;
    @org.jetbrains.annotations.NotNull()
    private static final androidx.compose.runtime.ProvidableCompositionLocal<com.kjurkovic.resources.theme.ThemeColors> LocalColors = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.resources.theme.ThemeColors getLightThemeColors() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.resources.theme.ThemeColors getDarkThemeColors() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final androidx.compose.runtime.ProvidableCompositionLocal<com.kjurkovic.resources.theme.ThemeColors> getLocalColors() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.compose.runtime.ReadOnlyComposable()
    @androidx.compose.runtime.Composable()
    public static final com.kjurkovic.resources.theme.ThemeColors getAllColors(@org.jetbrains.annotations.NotNull()
    androidx.compose.material.MaterialTheme $this$allColors) {
        return null;
    }
}