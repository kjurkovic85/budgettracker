package com.kjurkovic.resources.formatters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0002\u001a\u00020\u0003*\u00020\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"MONEY_FORMAT", "Ljava/text/DecimalFormat;", "toPrice", "", "Ljava/math/BigDecimal;", "resources_debug"})
public final class MoneyFormatterKt {
    private static final java.text.DecimalFormat MONEY_FORMAT = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toPrice(@org.jetbrains.annotations.NotNull()
    java.math.BigDecimal $this$toPrice) {
        return null;
    }
}