package com.kjurkovic.resources.formatters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0005\"\u0016\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"DATE_TIME_FORMAT", "Ljava/time/format/DateTimeFormatter;", "kotlin.jvm.PlatformType", "stringify", "", "Ljava/time/LocalDate;", "resources_debug"})
public final class DateFormatterKt {
    private static final java.time.format.DateTimeFormatter DATE_TIME_FORMAT = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String stringify(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate $this$stringify) {
        return null;
    }
}