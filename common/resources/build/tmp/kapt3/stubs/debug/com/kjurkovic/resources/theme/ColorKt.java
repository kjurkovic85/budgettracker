package com.kjurkovic.resources.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b&\"\u0016\u0010\u0000\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0005\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0006\u0010\u0003\"(\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00010\t0\b\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\b\u000b\u0010\f\"(\u0010\u000e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00010\t0\b\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\b\u000f\u0010\f\"\u0016\u0010\u0010\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0011\u0010\u0003\"\u0016\u0010\u0012\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0013\u0010\u0003\"\u0016\u0010\u0014\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0015\u0010\u0003\"\u0016\u0010\u0016\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0017\u0010\u0003\"\u0016\u0010\u0018\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0019\u0010\u0003\"\u0016\u0010\u001a\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u001b\u0010\u0003\"\u0016\u0010\u001c\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u001d\u0010\u0003\"\u0016\u0010\u001e\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u001f\u0010\u0003\"\u0016\u0010 \u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b!\u0010\u0003\"\u0016\u0010\"\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b#\u0010\u0003\"\u0016\u0010$\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b%\u0010\u0003\"\u0016\u0010&\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\'\u0010\u0003\"\u0016\u0010(\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b)\u0010\u0003\"\u0016\u0010*\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b+\u0010\u0003\"\u0016\u0010,\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b-\u0010\u0003\"\u0016\u0010.\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b/\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u00060"}, d2 = {"BackgroundColor", "Landroidx/compose/ui/graphics/Color;", "getBackgroundColor", "()J", "J", "Black", "getBlack", "ChartHorizontalBorderGradientStops", "", "Lkotlin/Pair;", "", "getChartHorizontalBorderGradientStops", "()[Lkotlin/Pair;", "[Lkotlin/Pair;", "ChartVerticalBorderGradientStops", "getChartVerticalBorderGradientStops", "ErrorColor", "getErrorColor", "NegativeAmountColor", "getNegativeAmountColor", "OnBackgroundColor", "getOnBackgroundColor", "OnErrorColor", "getOnErrorColor", "OnPrimaryColor", "getOnPrimaryColor", "OnSecondaryColor", "getOnSecondaryColor", "OnSurfaceColor", "getOnSurfaceColor", "PlaceholderColor", "getPlaceholderColor", "PositiveAmountColor", "getPositiveAmountColor", "PrimaryColor", "getPrimaryColor", "PrimaryVariantColor", "getPrimaryVariantColor", "SecondaryColor", "getSecondaryColor", "SecondaryVariantColor", "getSecondaryVariantColor", "ShadowColor", "getShadowColor", "SurfaceColor", "getSurfaceColor", "White", "getWhite", "resources_debug"})
public final class ColorKt {
    private static final long Black = 0L;
    private static final long White = 0L;
    private static final long PrimaryColor = 0L;
    private static final long PrimaryVariantColor = 0L;
    private static final long OnPrimaryColor = 0L;
    private static final long SecondaryColor = 0L;
    private static final long SecondaryVariantColor = 0L;
    private static final long OnSecondaryColor = 0L;
    private static final long SurfaceColor = 0L;
    private static final long OnSurfaceColor = 0L;
    private static final long BackgroundColor = 0L;
    private static final long OnBackgroundColor = 0L;
    private static final long ErrorColor = 0L;
    private static final long OnErrorColor = 0L;
    private static final long PlaceholderColor = 0L;
    private static final long PositiveAmountColor = 0L;
    private static final long NegativeAmountColor = 0L;
    private static final long ShadowColor = 0L;
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.Pair<java.lang.Float, androidx.compose.ui.graphics.Color>[] ChartHorizontalBorderGradientStops = null;
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.Pair<java.lang.Float, androidx.compose.ui.graphics.Color>[] ChartVerticalBorderGradientStops = null;
    
    public static final long getBlack() {
        return 0L;
    }
    
    public static final long getWhite() {
        return 0L;
    }
    
    public static final long getPrimaryColor() {
        return 0L;
    }
    
    public static final long getPrimaryVariantColor() {
        return 0L;
    }
    
    public static final long getOnPrimaryColor() {
        return 0L;
    }
    
    public static final long getSecondaryColor() {
        return 0L;
    }
    
    public static final long getSecondaryVariantColor() {
        return 0L;
    }
    
    public static final long getOnSecondaryColor() {
        return 0L;
    }
    
    public static final long getSurfaceColor() {
        return 0L;
    }
    
    public static final long getOnSurfaceColor() {
        return 0L;
    }
    
    public static final long getBackgroundColor() {
        return 0L;
    }
    
    public static final long getOnBackgroundColor() {
        return 0L;
    }
    
    public static final long getErrorColor() {
        return 0L;
    }
    
    public static final long getOnErrorColor() {
        return 0L;
    }
    
    public static final long getPlaceholderColor() {
        return 0L;
    }
    
    public static final long getPositiveAmountColor() {
        return 0L;
    }
    
    public static final long getNegativeAmountColor() {
        return 0L;
    }
    
    public static final long getShadowColor() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.Pair<java.lang.Float, androidx.compose.ui.graphics.Color>[] getChartHorizontalBorderGradientStops() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.Pair<java.lang.Float, androidx.compose.ui.graphics.Color>[] getChartVerticalBorderGradientStops() {
        return null;
    }
}