package com.kjurkovic.resources;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bJ1\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0012\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u000b\"\u00020\u0001\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bJ\u0010\u0010\u000e\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bJ\u0010\u0010\u000f\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bJ\u0010\u0010\u0010\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bJ)\u0010\u0010\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\b2\u0012\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u000b\"\u00020\u0001\u00a2\u0006\u0002\u0010\u0011J\u001b\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b2\b\b\u0001\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/kjurkovic/resources/CommonResources;", "", "resources", "Landroid/content/res/Resources;", "(Landroid/content/res/Resources;)V", "getQuantityString", "", "id", "", "quantity", "formatArgs", "", "(II[Ljava/lang/Object;)Ljava/lang/String;", "getResourceEntryName", "getResourcePackageName", "getResourceTypeName", "getString", "(I[Ljava/lang/Object;)Ljava/lang/String;", "getStringArray", "(I)[Ljava/lang/String;", "resources_debug"})
@javax.inject.Singleton()
public final class CommonResources {
    private final android.content.res.Resources resources = null;
    
    @javax.inject.Inject()
    public CommonResources(@org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getString(@androidx.annotation.StringRes()
    int id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getString(@androidx.annotation.StringRes()
    int id, @org.jetbrains.annotations.NotNull()
    java.lang.Object... formatArgs) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String[] getStringArray(@androidx.annotation.ArrayRes()
    int id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityString(@androidx.annotation.PluralsRes()
    int id, int quantity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityString(@androidx.annotation.PluralsRes()
    int id, int quantity, @org.jetbrains.annotations.NotNull()
    java.lang.Object... formatArgs) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getResourcePackageName(@androidx.annotation.AnyRes()
    int id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getResourceTypeName(@androidx.annotation.AnyRes()
    int id) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getResourceEntryName(@androidx.annotation.AnyRes()
    int id) {
        return null;
    }
}