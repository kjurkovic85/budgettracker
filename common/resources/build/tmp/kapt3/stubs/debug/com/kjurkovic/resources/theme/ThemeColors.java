package com.kjurkovic.resources.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b*\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B(\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ\t\u0010-\u001a\u00020\u0003H\u00c6\u0003J\u0019\u0010.\u001a\u00020\u0005H\u00c6\u0003\u00f8\u0001\u0000\u00f8\u0001\u0002\u00f8\u0001\u0001\u00a2\u0006\u0004\b/\u0010\u000bJ\u0019\u00100\u001a\u00020\u0005H\u00c6\u0003\u00f8\u0001\u0000\u00f8\u0001\u0002\u00f8\u0001\u0001\u00a2\u0006\u0004\b1\u0010\u000bJ\u0019\u00102\u001a\u00020\u0005H\u00c6\u0003\u00f8\u0001\u0000\u00f8\u0001\u0002\u00f8\u0001\u0001\u00a2\u0006\u0004\b3\u0010\u000bJ>\u00104\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u0005H\u00c6\u0001\u00f8\u0001\u0000\u00f8\u0001\u0001\u00a2\u0006\u0004\b5\u00106J\u0013\u00107\u001a\u00020\u000f2\b\u00108\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00109\u001a\u00020:H\u00d6\u0001J\t\u0010;\u001a\u00020<H\u00d6\u0001R\u001a\u0010\t\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u001a\u0010\f\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u000e\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0007\u001a\u00020\u0005\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0013\u0010\u000bR\u001a\u0010\u0015\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u000bR\u001a\u0010\u0017\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u000bR\u001a\u0010\u0019\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u000bR\u001a\u0010\u001b\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u000bR\u001a\u0010\u001d\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\u001e\u0010\u000bR\u001c\u0010\u0004\u001a\u00020\u0005\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u001f\u0010\u000bR\u001c\u0010\u0006\u001a\u00020\u0005\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b \u0010\u000bR\u001a\u0010!\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b\"\u0010\u000bR\u001a\u0010#\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b$\u0010\u000bR\u001c\u0010%\u001a\u00020\u0005\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b&\u0010\u000bR\u001a\u0010\'\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b(\u0010\u000bR\u001a\u0010)\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b*\u0010\u000bR\u001a\u0010+\u001a\u00020\u00058F\u00f8\u0001\u0000\u00f8\u0001\u0001\u00f8\u0001\u0002\u00a2\u0006\u0006\u001a\u0004\b,\u0010\u000b\u0082\u0002\u000f\n\u0002\b\u0019\n\u0005\b\u00a1\u001e0\u0001\n\u0002\b!\u00a8\u0006="}, d2 = {"Lcom/kjurkovic/resources/theme/ThemeColors;", "", "material", "Landroidx/compose/material/Colors;", "placeholder", "Landroidx/compose/ui/graphics/Color;", "positiveAmount", "negativeAmount", "(Landroidx/compose/material/Colors;JJJLkotlin/jvm/internal/DefaultConstructorMarker;)V", "background", "getBackground-0d7_KjU", "()J", "error", "getError-0d7_KjU", "isLight", "", "()Z", "getMaterial", "()Landroidx/compose/material/Colors;", "getNegativeAmount-0d7_KjU", "J", "onBackground", "getOnBackground-0d7_KjU", "onError", "getOnError-0d7_KjU", "onPrimary", "getOnPrimary-0d7_KjU", "onSecondary", "getOnSecondary-0d7_KjU", "onSurface", "getOnSurface-0d7_KjU", "getPlaceholder-0d7_KjU", "getPositiveAmount-0d7_KjU", "primary", "getPrimary-0d7_KjU", "primaryVariant", "getPrimaryVariant-0d7_KjU", "scrimColor", "getScrimColor-0d7_KjU", "secondary", "getSecondary-0d7_KjU", "secondaryVariant", "getSecondaryVariant-0d7_KjU", "surface", "getSurface-0d7_KjU", "component1", "component2", "component2-0d7_KjU", "component3", "component3-0d7_KjU", "component4", "component4-0d7_KjU", "copy", "copy-zSO0fhY", "(Landroidx/compose/material/Colors;JJJ)Lcom/kjurkovic/resources/theme/ThemeColors;", "equals", "other", "hashCode", "", "toString", "", "resources_debug"})
public final class ThemeColors {
    @org.jetbrains.annotations.NotNull()
    private final androidx.compose.material.Colors material = null;
    private final long placeholder = 0L;
    private final long positiveAmount = 0L;
    private final long negativeAmount = 0L;
    private final long scrimColor = 0L;
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    private ThemeColors(androidx.compose.material.Colors material, long placeholder, long positiveAmount, long negativeAmount) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.compose.material.Colors component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.compose.material.Colors getMaterial() {
        return null;
    }
    
    public final boolean isLight() {
        return false;
    }
}