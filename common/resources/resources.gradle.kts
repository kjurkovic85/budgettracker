plugins {
    id("base-android-compose-library")
}

dependencies {
    implementation(libs.bundles.compose)
    implementation(libs.hilt.android)
    kapt(libs.hilt.compiler)
}
