package com.kjurkovic.resources.theme

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

data class ThemeColors(
    val material: Colors,
    val placeholder: Color,
    val positiveAmount: Color,
    val negativeAmount: Color,
) {
    val primary: Color get() = material.primary
    val primaryVariant: Color get() = material.primaryVariant
    val secondary: Color get() = material.secondary
    val secondaryVariant: Color get() = material.secondaryVariant
    val background: Color get() = material.background
    val surface: Color get() = material.surface
    val error: Color get() = material.error
    val onPrimary: Color get() = material.onPrimary
    val onSecondary: Color get() = material.onSecondary
    val onBackground: Color get() = material.onBackground
    val onSurface: Color get() = material.onSurface
    val onError: Color get() = material.onError
    val isLight: Boolean get() = material.isLight

    val scrimColor: Color = onBackground.copy(alpha = 0.8f)
}

internal val LightThemeColors = ThemeColors(
    material = lightColors(
        primary = PrimaryColor,
        primaryVariant = PrimaryVariantColor,
        onPrimary = OnPrimaryColor,
        secondary = SecondaryColor,
        secondaryVariant = SecondaryVariantColor,
        onSecondary = OnSecondaryColor,
        surface = SurfaceColor,
        onSurface = OnSurfaceColor,
        background = BackgroundColor,
        onBackground = OnBackgroundColor,
        error = ErrorColor,
        onError = OnErrorColor,
    ),
    placeholder = PlaceholderColor,
    positiveAmount = PositiveAmountColor,
    negativeAmount = NegativeAmountColor,
)

internal val DarkThemeColors = ThemeColors(
    material = lightColors(
        primary = PrimaryColor,
        primaryVariant = PrimaryVariantColor,
        onPrimary = OnPrimaryColor,
        secondary = SecondaryColor,
        secondaryVariant = SecondaryVariantColor,
        onSecondary = OnSecondaryColor,
        surface = SurfaceColor,
        onSurface = OnSurfaceColor,
        background = BackgroundColor,
        onBackground = OnBackgroundColor,
        error = ErrorColor,
        onError = OnErrorColor,
    ),
    placeholder = PlaceholderColor,
    positiveAmount = PositiveAmountColor,
    negativeAmount = NegativeAmountColor,
)

internal val LocalColors = staticCompositionLocalOf { LightThemeColors }

val MaterialTheme.allColors: ThemeColors
    @Composable
    @ReadOnlyComposable
    get() = LocalColors.current
