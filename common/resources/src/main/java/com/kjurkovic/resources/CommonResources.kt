package com.kjurkovic.resources

import android.content.ContentResolver
import android.content.res.Resources
import androidx.annotation.AnyRes
import androidx.annotation.ArrayRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommonResources @Inject constructor(
    private val resources: Resources
){
    fun getString(@StringRes id: Int): String {
        return resources.getString(id)
    }

    fun getString(@StringRes id: Int, vararg formatArgs: Any): String {
        return resources.getString(id, *formatArgs)
    }

    fun getStringArray(@ArrayRes id: Int): Array<String> {
        return resources.getStringArray(id)
    }

    fun getQuantityString(@PluralsRes id: Int, quantity: Int): String {
        return resources.getQuantityString(id, quantity)
    }

    fun getQuantityString(@PluralsRes id: Int, quantity: Int, vararg formatArgs: Any): String {
        return resources.getQuantityString(id, quantity, *formatArgs)
    }

    fun getResourcePackageName(@AnyRes id: Int): String {
        return resources.getResourcePackageName(id)
    }

    fun getResourceTypeName(@AnyRes id: Int): String {
        return resources.getResourceTypeName(id)
    }

    fun getResourceEntryName(@AnyRes id: Int): String {
        return resources.getResourceEntryName(id)
    }
}