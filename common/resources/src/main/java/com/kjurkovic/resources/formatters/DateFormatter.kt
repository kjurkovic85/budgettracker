package com.kjurkovic.resources.formatters

import java.time.LocalDate
import java.time.format.DateTimeFormatter

private val DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy.")

fun LocalDate.stringify(): String = DATE_TIME_FORMAT.format(this)
