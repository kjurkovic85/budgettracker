package com.kjurkovic.resources.theme

import androidx.compose.ui.graphics.Color

val Black = Color(0xFF333333)
val White = Color(0xFFFFFFFF)

val PrimaryColor = Color(0xFFF36421)
val PrimaryVariantColor = Color(0xFFF5734B)
val OnPrimaryColor = Color(0xFFF9FAFC)
val SecondaryColor = Color(0xFF3DA4F7)
val SecondaryVariantColor = Color(0xFF00589E)
val OnSecondaryColor = Color(0xFFFFFFFF)
val SurfaceColor = Color(0xFFEBEBEB)
val OnSurfaceColor = Color(0xFF333333)
val BackgroundColor = Color(0xFFFFFFFF)
val OnBackgroundColor = Color(0xFF000000)
val ErrorColor = Color(0xFFBE0000)
val OnErrorColor = Color(0xFFFFFFFF)
val PlaceholderColor = Color(0xFF7A7A7A)

val PositiveAmountColor = Color(0xFF009688)
val NegativeAmountColor = Color(0xFFD63F10)

val ShadowColor = Color(0xFF000000)

val ChartHorizontalBorderGradientStops = arrayOf(
    0f to Color(1f, 1f, 1f, 0.3f),
    0.0449f to Color(0f, 0f, 0f, 0.4f),
    0.2734f to Color(0f, 0f, 0f, 0.2f),
    0.4675f to Color(0f, 0f, 0f, 0.4f),
    0.50f to Color(1f, 1f, 1f, 0.3f),
    0.5624f to Color(1f, 1f, 1f, 0.8f),
    0.6401f to Color(1f, 1f, 1f, 0.6238f),
    0.8459f to Color(1f, 1f, 1f, 0.3f),
    0.9432f to Color(1f, 1f, 1f, 0.9f),
    0.9727f to Color(1f, 1f, 1f, 0.5f),
    1.0f to Color(1f, 1f, 1f, 0.3f),
)

val ChartVerticalBorderGradientStops = ChartHorizontalBorderGradientStops.map {
    (it.first + 0.25f) % 1f to it.second
}.toTypedArray()
