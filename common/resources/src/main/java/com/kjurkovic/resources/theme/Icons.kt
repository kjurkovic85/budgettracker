package com.kjurkovic.resources.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable

val MaterialTheme.icons
    @Composable
    @ReadOnlyComposable
    get() = Icons.Default