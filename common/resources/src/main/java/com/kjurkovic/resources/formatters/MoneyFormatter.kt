package com.kjurkovic.resources.formatters

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

private val MONEY_FORMAT = DecimalFormat("#,##0.00", DecimalFormatSymbols.getInstance(Locale.US)).apply {
    roundingMode = RoundingMode.HALF_UP
}

fun BigDecimal.toPrice(): String {
    return MONEY_FORMAT.format(this)
}
