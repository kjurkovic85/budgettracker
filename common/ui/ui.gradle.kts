plugins {
    id("base-android-compose-library")
}

dependencies {
    implementation(libs.bundles.accompanist)
    implementation(libs.bundles.compose)
    implementation(libs.coil.compose)
    implementation(libs.timber)
    implementation(projects.common.resources)
}
