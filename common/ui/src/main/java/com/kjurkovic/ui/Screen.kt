package com.kjurkovic.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.luminance
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.kjurkovic.resources.theme.allColors

/**
 * Creates a Screen.
 *
 * @param backgroundColor
 * Screen background color.
 *
 * @param topBarContent
 * Content for top bar. Use [Toolbar] for best effect.
 *
 * @param topBarBackgroundColor
 * Top bar background color.
 *
 * @param topBarElevation
 * Top bar shadow elevation.
 *
 * @param addStatusBarPadding
 * Whether to increase [topBarContent] [content].[PaddingValues] top padding equal to status bar.
 *
 * @param statusBarColor
 * Sets the color of the status bar for this screen if not null.
 * Defaults to null.
 *
 * @param statusBarDarkIcons
 * Whether to use dark icons on status bar.
 * If [addStatusBarPadding] is false defaults to null.
 * If [addStatusBarPadding] is true defaults to true or false based on [statusBarColor].[luminance].
 * If [statusBarColor] is null it uses [topBarBackgroundColor].[luminance].
 * If [topBarContent] is null it uses [backgroundColor].[luminance].
 *
 * @param bottomBarContent
 * Content for bottom bar.
 *
 * @param bottomBarBackgroundColor
 * Bottom bar background color.
 *
 * @param addNavigationBarPadding
 * Whether to increase [bottomBarContent] and [content].[PaddingValues] bottom padding equal to navigation bar height.
 * If there is no navigation bar it adds [noNavigationBarBottomPadding].
 *
 * @param noNavigationBarBottomPadding
 * Padding to apply when there is no bottom navigation bar (usually on older devices).
 * If [addNavigationBarPadding] is false has no effect.
 *
 * @param navigationBarColor
 * Sets the color of the navigation bar for this screen if not null.
 *
 * @param navigationBarDarkIcons
 * If not null sets whether to use dark icons on navigation bar.
 * If [addNavigationBarPadding] is false defaults to null.
 * If [addNavigationBarPadding] is true defaults to true or false based on [navigationBarColor].[luminance].
 * If [navigationBarColor] is null it uses [bottomBarBackgroundColor].[luminance] instead.
 * If [bottomBarContent] is null it uses [backgroundColor].[luminance] instead.
 *
 * @param content
 * Composable content.
 * The lambda receives [PaddingValues] that should be applied to the content root via [Modifier.padding] to properly offset top and bottom bars.
 * If you're using VerticalScroller, apply this modifier to the child of the scroller, and not on the scroller itself.
 */
@Composable
fun ScreenComponent(
    backgroundColor: Color = MaterialTheme.allColors.background,
    topBarContent: (@Composable () -> Unit)? = null,
    topBarBackgroundColor: Color = backgroundColor,
    topBarElevation: Dp = 0.dp,
    addStatusBarPadding: Boolean = true,
    statusBarColor: Color? = null,
    statusBarDarkIcons: Boolean? = if (!addStatusBarPadding) null else when {
        statusBarColor != null -> statusBarColor
        topBarContent != null -> topBarBackgroundColor
        else -> backgroundColor
    }.luminance() > 0.5f,
    bottomBarContent: (@Composable () -> Unit)? = null,
    bottomBarBackgroundColor: Color = backgroundColor,
    addNavigationBarPadding: Boolean = true,
    noNavigationBarBottomPadding: Dp = 0.dp,
    navigationBarColor: Color? = null,
    navigationBarDarkIcons: Boolean? = if (!addNavigationBarPadding) null else when {
        navigationBarColor != null -> navigationBarColor
        bottomBarContent != null -> bottomBarBackgroundColor
        else -> backgroundColor
    }.luminance() > 0.5f,
    floatingButton: (@Composable () -> Unit)? = null,
    content: @Composable (PaddingValues) -> Unit,
) {
    val systemUiController = rememberSystemUiController()
    SideEffect {
        statusBarDarkIcons?.let {
            systemUiController.setStatusBarColor(
                color = Color.Transparent,
                darkIcons = statusBarDarkIcons,
            )
        }
        navigationBarDarkIcons?.let {
            systemUiController.setNavigationBarColor(
                color = Color.Transparent,
                darkIcons = navigationBarDarkIcons,
                navigationBarContrastEnforced = false,
            )
        }
    }

    val density = LocalDensity.current
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(backgroundColor),
    ) {
        val statusBarInsets = LocalWindowInsets.current.statusBars
        val statusBarHeight = remember(statusBarInsets) {
            density.run { statusBarInsets.top.toDp() }
        }
        val statusBarPadding = remember(addStatusBarPadding, statusBarInsets) {
            if (addStatusBarPadding) statusBarHeight
            else 0.dp
        }

        val navigationBarInsets = LocalWindowInsets.current.navigationBars
        val navigationBarHeight = remember(navigationBarInsets) {
            density.run { navigationBarInsets.bottom.toDp() }
        }
        val navigationBarPadding = remember(addNavigationBarPadding, navigationBarInsets, noNavigationBarBottomPadding) {
            when {
                addNavigationBarPadding && navigationBarInsets.bottom > 0 -> navigationBarHeight
                addNavigationBarPadding && navigationBarInsets.bottom == 0 -> noNavigationBarBottomPadding
                else -> 0.dp
            }
        }

        val topPadding = remember { mutableStateOf(statusBarPadding) }
        val bottomPadding = remember { mutableStateOf(navigationBarPadding) }
        val innerPadding = remember(topPadding.value, bottomPadding.value) {
            PaddingValues(top = topPadding.value, bottom = bottomPadding.value)
        }

        content(innerPadding)

        floatingButton?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = floatingButtonPadding(innerPadding).calculateBottomPadding())
                    .padding(horizontal = dimensionResource(R.dimen.size_16))
                    .align(Alignment.BottomCenter)
            ) {
                floatingButton()
            }
        }

        topBarContent?.let {
            Box(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .fillMaxWidth()
                    .height(dimensionResource(R.dimen.height_toolbar) + statusBarPadding)
                    .onGloballyPositioned { coordinates ->
                        topPadding.value = density.run { coordinates.size.height.toDp() }
                    }
                    .graphicsLayer {
                        shadowElevation = topBarElevation.toPx()
                    }
                    .background(topBarBackgroundColor)
                    .padding(top = statusBarPadding)
            ) {
                topBarContent()
            }
        }
        statusBarColor?.let {
            Box(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .fillMaxWidth()
                    .height(statusBarHeight)
                    .background(statusBarColor)
            )
        }

        bottomBarContent?.let {
            Box(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .onGloballyPositioned { coordinates ->
                        bottomPadding.value = density.run { navigationBarPadding + coordinates.size.height.toDp() }
                    }
                    .background(topBarBackgroundColor)
                    .padding(bottom = navigationBarPadding)
            ) {
                bottomBarContent()
            }
        }
        navigationBarColor?.let {
            Box(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .fillMaxWidth()
                    .height(navigationBarHeight)
                    .background(navigationBarColor)
            )
        }
    }
}

/**
 * Applies bottom padding equal to size of navigation bar, or [bottomPadding] if there is no navigation bar (usually on older devices).
 *
 * @param bottomPadding
 * Padding to apply when there is no navigation bar.
 */
fun Modifier.navigationBarOrBottomPadding(
    bottomPadding: Dp = 0.dp,
): Modifier = composed {
    val paddingValues = rememberInsetsPaddingValues(
        insets = LocalWindowInsets.current.navigationBars,
        applyStart = true,
        applyEnd = true,
        applyBottom = true,
    )
    padding(
        start = paddingValues.calculateStartPadding(LayoutDirection.Ltr),
        end = paddingValues.calculateEndPadding(LayoutDirection.Ltr),
        bottom = paddingValues.calculateBottomPadding().let { if (it > 0.dp) it else bottomPadding },
    )
}

/**
 * Applies PaddingValues so that the content shows up above the floating button.
 * The padding values are a sum of the navigationBarsHeight + 8.dp + buttonHeight + 8.dp
 *
 *  @param innerPadding
 *  The padding returned from Screen
 */
@Composable
fun floatingButtonPaddingValues(
    innerPadding: PaddingValues,
): PaddingValues = PaddingValues(
    bottom = floatingButtonPadding(innerPadding).calculateBottomPadding()
            + dimensionResource(R.dimen.height_button)
            + dimensionResource(R.dimen.size_8)
)

/**
 * The padding that applies to every floating button so that their position on the screens is always the same
 * The padding values are a sum of the navigationBarsHeight + 8.dp
 *
 *  @param innerPadding
 *  The padding returned from Screen
 */
@Composable
private fun floatingButtonPadding(
    innerPadding: PaddingValues,
): PaddingValues = PaddingValues(
    bottom = innerPadding.calculateBottomPadding()
            + dimensionResource(R.dimen.size_8)
)
