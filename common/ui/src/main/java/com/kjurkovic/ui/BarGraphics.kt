package com.kjurkovic.ui

import android.graphics.LinearGradient
import android.graphics.Shader
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.unit.dp
import com.kjurkovic.resources.theme.ChartHorizontalBorderGradientStops
import com.kjurkovic.resources.theme.ShadowColor

fun DrawScope.drawBar(
    barSize: Size,
    borderWidth: Float,
    cornerRadius: CornerRadius,
    barColor: Color,
    barGradientColor: Color,
    borderGradientColorStops: Array<Pair<Float, Color>> = ChartHorizontalBorderGradientStops,
    drawShadow: Boolean = true,
) {
    if (drawShadow) {
        drawBarShadow(
            barSize = barSize,
            cornerRadius = cornerRadius,
            shadowColor = barGradientColor,
        )
    }

    drawRoundRect(
        color = barColor,
        size = barSize,
        cornerRadius = cornerRadius,
    )
    drawRoundRect(
        brush = Brush.linearGradient(listOf(barGradientColor.copy(alpha = 1f), barGradientColor.copy(alpha = 0f))),
        size = barSize,
        cornerRadius = cornerRadius,
        blendMode = BlendMode.Multiply,
        alpha = 0.3f,
    )

    drawBarBorder(
        barSize = barSize,
        borderWidth = borderWidth,
        cornerRadius = cornerRadius,
        gradientColorStops = borderGradientColorStops,
    )
}

fun DrawScope.drawBarShadow(
    barSize: Size,
    cornerRadius: CornerRadius,
    shadowColor: Color,
) {
    drawBarShadow(
        barSize = barSize,
        cornerRadius = cornerRadius,
        shadowColors = listOf(shadowColor),
        shadowPositions = FloatArray(0),
    )
}

fun DrawScope.drawBarShadow(
    barSize: Size,
    cornerRadius: CornerRadius,
    shadowColors: List<Color>,
    shadowPositions: FloatArray,
) {
    if (barSize.isEmpty()) return

    val colorShadowRadius = 4.dp.toPx()
    val colorShadowHalfRadius = colorShadowRadius / 2
    val colorShadowQuarterRadius = colorShadowHalfRadius / 2

    val colorShadowPaint = Paint().also {
        if (shadowColors.size > 1) {
            it.shader = LinearGradient(
                0f, 0f, 0f, barSize.height,
                shadowColors.map { it.toArgb() }.toIntArray(),
                shadowPositions,
                Shader.TileMode.MIRROR
            )
        }
        it.asFrameworkPaint().apply {
            color = shadowColors[0].copy(alpha = 0.0f).toArgb()
            setShadowLayer(
                colorShadowRadius,
                colorShadowQuarterRadius,
                colorShadowQuarterRadius,
                shadowColors[0].copy(alpha = 0.5f).toArgb()
            )
        }
    }

    val darkShadowPaint = Paint().also {
        it.asFrameworkPaint().apply {
            color = ShadowColor.copy(alpha = 0.0f).toArgb()
            setShadowLayer(
                colorShadowHalfRadius,
                colorShadowQuarterRadius,
                colorShadowQuarterRadius,
                ShadowColor.copy(alpha = 0.3f).toArgb()
            )
        }
    }

    drawIntoCanvas {
        it.drawRoundRect(
            -colorShadowQuarterRadius,
            -colorShadowQuarterRadius,
            barSize.width + colorShadowHalfRadius,
            barSize.height + colorShadowHalfRadius,
            cornerRadius.x,
            cornerRadius.y,
            colorShadowPaint
        )
        it.drawRoundRect(
            0f,
            0f,
            barSize.width,
            barSize.height,
            cornerRadius.x,
            cornerRadius.y,
            darkShadowPaint
        )
    }
}

fun DrawScope.drawBarBorder(
    barSize: Size,
    borderWidth: Float,
    cornerRadius: CornerRadius,
    gradientColorStops: Array<Pair<Float, Color>>,
) {
    if (barSize.isEmpty() || borderWidth <= 0) return

    val borderSize = Size(barSize.width - borderWidth, barSize.height - borderWidth)
    val borderHalfWidth = borderWidth / 2

    drawRoundRect(
        brush = Brush.sweepGradient(
            colorStops = gradientColorStops,
            center = Offset(borderSize.width / 2, borderSize.height / 2),
        ),
        topLeft = Offset(borderHalfWidth, borderHalfWidth),
        size = borderSize,
        cornerRadius = cornerRadius,
        style = Stroke(borderWidth),
        blendMode = BlendMode.Overlay,
    )
}

fun DrawScope.drawBarBackground(
    barSize: Size,
    cornerRadius: CornerRadius,
    backgroundColor: Color,
    restoreClip: Boolean = true,
) {
    if (barSize.isEmpty()) return

    val backgroundPaint = Paint().apply {
        color = backgroundColor
    }

    val darkShadowRadius = 3.dp.toPx()
    val darkShadowX = 1.dp.toPx()
    val darkShadowY = 2.dp.toPx()
    val darkShadowPaint = Paint().also {
        it.asFrameworkPaint().apply {
            style = android.graphics.Paint.Style.STROKE
            color = ShadowColor.copy(alpha = 0.0f).toArgb()
            setShadowLayer(
                darkShadowRadius,
                darkShadowX,
                darkShadowY,
                ShadowColor.copy(alpha = 0.9f).toArgb()
            )
        }
    }

    val barPath = Path().apply {
        addRoundRect(RoundRect(0f, 0f, barSize.width, barSize.height, cornerRadius.x, cornerRadius.y))
    }

    drawIntoCanvas {
        if (restoreClip) it.save()

        it.clipPath(barPath)
        it.drawPath(barPath, backgroundPaint)
        it.drawPath(barPath, darkShadowPaint)

        if (restoreClip) it.restore()
    }
}
