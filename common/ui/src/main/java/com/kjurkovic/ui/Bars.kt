package com.kjurkovic.ui

import androidx.annotation.FloatRange
import androidx.compose.foundation.Canvas
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.kjurkovic.resources.theme.ChartHorizontalBorderGradientStops
import com.kjurkovic.resources.theme.allColors

@Composable
fun HorizontalBar(
    @FloatRange(from = 0.0, to = 1.0) value: Double,
    modifier: Modifier = Modifier,
    barColor: Color = MaterialTheme.allColors.negativeAmount,
    barGradientColor: Color = MaterialTheme.allColors.negativeAmount,
    borderWidth: Dp = 2.dp,
    drawBackground: Boolean = true,
    backgroundColor: Color = MaterialTheme.allColors.positiveAmount,
) {
    Canvas(modifier = modifier) {
        val cornerRadius = CornerRadius(size.height / 2)
        if (drawBackground) {
            drawBarBackground(
                barSize = size,
                cornerRadius = cornerRadius,
                backgroundColor = backgroundColor,
            )
        }
        drawBar(
            barSize = Size(size.width * value.toFloat(), size.height),
            borderWidth = borderWidth.toPx(),
            cornerRadius = cornerRadius,
            barColor = barColor,
            barGradientColor = barGradientColor,
            borderGradientColorStops = ChartHorizontalBorderGradientStops,
        )
    }
}
