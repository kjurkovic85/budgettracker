package com.kjurkovic.ui

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.kjurkovic.resources.theme.AppTheme
import com.kjurkovic.resources.theme.allColors

@Composable
fun PrimaryButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    height: Dp = dimensionResource(R.dimen.height_button),
    textStyle: TextStyle = MaterialTheme.typography.button,
    colors: ButtonColors = ButtonDefaults.buttonColors(
        backgroundColor = MaterialTheme.allColors.primary,
        contentColor = MaterialTheme.allColors.onPrimary,
    ),
    shape: Shape = MaterialTheme.shapes.medium,
    border: BorderStroke? = null,
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding,
    enabled: Boolean = true,
    elevation: ButtonElevation? = ButtonDefaults.elevation(
        defaultElevation = dimensionResource(R.dimen.elevation_medium),
        pressedElevation = dimensionResource(R.dimen.elevation_flat),
        disabledElevation = dimensionResource(R.dimen.elevation_flat)
    ),
) {
    Button(
        onClick = onClick,
        modifier = modifier.height(height),
        colors = colors,
        shape = shape,
        border = border,
        contentPadding = contentPadding,
        enabled = enabled,
        elevation = elevation,
    ) {
        Text(
            text = text,
            style = textStyle,
        )
    }
}

@Preview("Buttons")
@Preview("Buttons (dark)", uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun PreviewButtons() {
    AppTheme {
        Column(
            Modifier
                .fillMaxWidth()
                .background(MaterialTheme.allColors.background)
                .padding(8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            PrimaryButton(text = "Primary button", onClick = {})

        }
    }
}
