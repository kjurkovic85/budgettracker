package com.kjurkovic.ui

import com.kjurkovic.resources.theme.icons
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp

@Composable
fun ScreenToolbar(
    titleContent: @Composable (BoxScope.() -> Unit)? = null,
    leftContent: @Composable (BoxScope.() -> Unit)? = null,
    rightContent: @Composable (BoxScope.() -> Unit)? = null,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(dimensionResource(R.dimen.height_toolbar))
    ) {
        Box(modifier = Modifier.align(Alignment.CenterStart)) {
            leftContent?.invoke(this)
        }

        Box(modifier = Modifier.align(Alignment.Center)) {
            titleContent?.invoke(this)
        }

        Box(modifier = Modifier.align(Alignment.CenterEnd)) {
            rightContent?.invoke(this)
        }
    }
}

@Composable
fun ScreenToolbar(
    title: String,
    iconPainter: Painter? = null,
    onBackClicked: (() -> Unit)? = null,
    rightContent: @Composable (BoxScope.() -> Unit)? = null,
) {
    ScreenToolbar(
        titleContent = {
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                iconPainter?.let {
                    Image(
                        painter = iconPainter,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(end = dimensionResource(R.dimen.size_8))
                            .size(24.dp),
                    )
                }
                Body(text = title)
            }
        },
        leftContent = onBackClicked?.let { { ToolbarBackButton(onClick = onBackClicked) } },
        rightContent = rightContent,
    )
}

@Composable
fun ScreenCloseToolbar(
    title: String,
    onCloseClicked: () -> Unit,
    iconPainter: Painter? = null,
) {
    ScreenToolbar(
        titleContent = {
            Row(
                verticalAlignment = Alignment.CenterVertically,
            ) {
                iconPainter?.let {
                    Image(
                        painter = iconPainter,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(end = dimensionResource(R.dimen.size_8))
                            .size(24.dp),
                    )
                }
                Body(text = title)
            }
        },
        leftContent = null,
        rightContent = { ToolbarCloseButton(onClick = onCloseClicked) },
    )
}

@Composable
fun ToolbarButton(
    modifier: Modifier = Modifier,
    imageVector: ImageVector,
    onClick: () -> Unit,
    contentDescription: String? = null,
    tint: Color = LocalContentColor.current.copy(alpha = LocalContentAlpha.current),
) {
    IconButton(
        modifier = modifier,
        onClick = { onClick() }
    ) {
        Icon(
            imageVector = imageVector,
            contentDescription = contentDescription,
            tint = tint,
        )
    }
}

@Composable
fun ToolbarBackButton(
    modifier: Modifier = Modifier,
    tint: Color = LocalContentColor.current.copy(alpha = LocalContentAlpha.current),
    onClick: () -> Unit,
) {
    ToolbarButton(
        modifier = modifier,
        imageVector = MaterialTheme.icons.ArrowBack,
        onClick = onClick,
        contentDescription = "",
        tint = tint,
    )
}

@Composable
fun ToolbarCloseButton(
    modifier: Modifier = Modifier,
    tint: Color = LocalContentColor.current.copy(alpha = LocalContentAlpha.current),
    onClick: () -> Unit,
) {
    ToolbarButton(
        modifier = modifier,
        imageVector = MaterialTheme.icons.Close,
        onClick = onClick,
        contentDescription = "",
        tint = tint,
    )
}
