package com.kjurkovic.ui

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import java.time.LocalDate

class AppDatePicker(context: Context, listener: AppOnDateSetListener, date: LocalDate) :
    DatePickerDialog(context, adaptListener(listener), date.year, date.monthValue - 1, date.dayOfMonth) {

    interface AppOnDateSetListener {
        fun onDateSet(view: DatePicker, date: LocalDate)
    }

    companion object {
        private fun adaptListener(domainListener: AppOnDateSetListener): OnDateSetListener {
            return OnDateSetListener { view, year, month, dayOfMonth ->
                domainListener.onDateSet(view, LocalDate.of(year, month + 1, dayOfMonth))
            }
        }
    }

}
