package com.kjurkovic.ui

import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.kjurkovic.resources.theme.allColors

@Composable
fun TextInput(
    value: String,
    modifier: Modifier = Modifier,
    onValueChange: (String) -> Unit = {},
    placeholder: String? = null,
    singleLine: Boolean = true,
    enabled: Boolean = true,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    focusedBorderColor: Color = MaterialTheme.allColors.primary,
    unfocusedBorderColor: Color = MaterialTheme.allColors.primary,
    placeholderColor: Color = MaterialTheme.allColors.placeholder,
) {
    TextField(
        modifier = modifier,
        value = value,
        onValueChange = onValueChange,
        label = placeholder?.let {
            { Text(text = placeholder) }
        },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = focusedBorderColor,
            unfocusedBorderColor = unfocusedBorderColor,
            placeholderColor = placeholderColor,
        ),
        singleLine = singleLine,
        isError = isError,
        trailingIcon = trailingIcon,
        enabled = enabled,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        shape = MaterialTheme.shapes.medium,
    )
}
