package com.kjurkovic.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.toUpperCase
import com.kjurkovic.resources.theme.allColors

@Composable
fun Alert(
    title: String,
    text: String,
    confirmText: String,
    cancelText: String? = null,
    onConfirm: (() -> Unit)? = null,
    onCancel: (() -> Unit)? = null,
    onDismiss: () -> Unit = { },
) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = { Text(text = title) },
        text = { Text(text = text) },
        confirmButton = {
            Body(
                text = confirmText.uppercase(),
                color = MaterialTheme.allColors.primary,
                style = MaterialTheme.typography.body2.copy(fontWeight = FontWeight.Bold),
                modifier = Modifier
                    .clickable { onConfirm?.invoke() }
                    .padding(
                        bottom = dimensionResource(R.dimen.size_16),
                        end = dimensionResource(R.dimen.size_16),
                        start = dimensionResource(R.dimen.size_8),
                    )
            )
        },
        dismissButton = if (cancelText != null) {
            {
                Body(
                    text = cancelText.uppercase(),
                    color = MaterialTheme.allColors.primary,
                    style = MaterialTheme.typography.body2,
                    modifier = Modifier
                        .clickable { onCancel?.invoke() }
                        .padding(bottom = dimensionResource(R.dimen.size_16))
                )
            }
        } else null,
        backgroundColor = MaterialTheme.allColors.background,
        shape = MaterialTheme.shapes.large,
    )
}