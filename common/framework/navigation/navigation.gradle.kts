plugins {
    id("base-android-library")
}

dependencies {
    implementation(libs.compose.foundation)
    implementation(libs.hilt.navigationCompose)
    implementation(libs.hilt.android)
    kapt(libs.hilt.androidCompiler)
}
