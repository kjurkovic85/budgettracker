package com.kjurkovic.common.framework.navigation

sealed class Destination(route: String, vararg params: String) : NavDestination(null, route, *params) {

    object Dashboard : Destination("dashboard")
    object Categories : Destination("categories") {
        object Add : NavDestination(Categories, "add")
        object Edit : NavDestination(Categories, "edit/{id}") {
            const val CATEGORY_ID = "id"
            operator fun invoke(categoryId: Long) = getRoute(CATEGORY_ID to categoryId)
        }
    }

    object Transactions : Destination("transactions") {
        object Add : NavDestination(Transactions, "add")
        object Edit : NavDestination(Transactions, "edit/{id}") {
            const val TRANSACTION_ID = "id"
            operator fun invoke(transactionId: String) = getRoute(TRANSACTION_ID to transactionId)
        }
    }

    object Profile : Destination("profile")
}