package com.kjurkovic.common.framework.navigation

import java.net.URLEncoder

fun String.appendParams(vararg params: Pair<String, Any?>): String {
    return StringBuilder(this).append('?').apply {
        params.forEach {
            val str = it.second?.toString()
            if (!str.isNullOrEmpty()) append("${it.first}=${URLEncoder.encode(str, Charsets.UTF_8.name())}&")
        }
        deleteAt(length - 1)
    }.toString()
}