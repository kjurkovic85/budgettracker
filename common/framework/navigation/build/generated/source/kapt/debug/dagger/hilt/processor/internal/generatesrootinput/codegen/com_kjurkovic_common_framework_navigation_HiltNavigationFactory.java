package dagger.hilt.processor.internal.generatesrootinput.codegen;

import com.kjurkovic.common.framework.navigation.HiltNavigationFactory;
import dagger.hilt.internal.generatesrootinput.GeneratesRootInputPropagatedData;
import javax.annotation.processing.Generated;

/**
 * Generated class toget the list of annotations that generate input for root.
 */
@GeneratesRootInputPropagatedData(HiltNavigationFactory.class)
@Generated("dagger.hilt.processor.internal.generatesrootinput.GeneratesRootInputPropagatedDataGenerator")
class com_kjurkovic_common_framework_navigation_HiltNavigationFactory {
}
