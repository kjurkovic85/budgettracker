package com.kjurkovic.common.framework.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a?\u0010\u0000\u001a\u00020\u0001*\u00020\u00012.\u0010\u0002\u001a\u0018\u0012\u0014\b\u0001\u0012\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00040\u0003\"\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"appendParams", "", "params", "", "Lkotlin/Pair;", "", "(Ljava/lang/String;[Lkotlin/Pair;)Ljava/lang/String;", "navigation_debug"})
public final class NavExtKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String appendParams(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$appendParams, @org.jetbrains.annotations.NotNull()
    kotlin.Pair<java.lang.String, ? extends java.lang.Object>... params) {
        return null;
    }
}