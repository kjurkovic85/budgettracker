package com.kjurkovic.common.framework.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\bf\u0018\u00002\u00020\u0001J\u0011\u0010\u0007\u001a\u00020\bH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\'\u0010\n\u001a\u00020\b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\r\u001a\u00020\u000eH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ9\u0010\u0010\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u0012\u001a\u00020\u000eH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u001e\u0010\u0014\u001a\u00020\b2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\r\u001a\u00020\u000eH&J0\u0010\u0015\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u0012\u001a\u00020\u000eH&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0016"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;", "", "navigationChannel", "Lkotlinx/coroutines/flow/SharedFlow;", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent;", "getNavigationChannel", "()Lkotlinx/coroutines/flow/SharedFlow;", "closeApp", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "navigateBack", "route", "", "inclusive", "", "(Ljava/lang/String;ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "navigateTo", "popUpToRoute", "isSingleTop", "(Ljava/lang/String;Ljava/lang/String;ZZLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "tryNavigateBack", "tryNavigateTo", "navigation_debug"})
public abstract interface ScreenNavigator {
    
    @org.jetbrains.annotations.NotNull()
    public abstract kotlinx.coroutines.flow.SharedFlow<com.kjurkovic.common.framework.navigation.NavigationIntent> getNavigationChannel();
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object navigateBack(@org.jetbrains.annotations.Nullable()
    java.lang.String route, boolean inclusive, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object navigateTo(@org.jetbrains.annotations.NotNull()
    java.lang.String route, @org.jetbrains.annotations.Nullable()
    java.lang.String popUpToRoute, boolean inclusive, boolean isSingleTop, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    public abstract void tryNavigateBack(@org.jetbrains.annotations.Nullable()
    java.lang.String route, boolean inclusive);
    
    public abstract void tryNavigateTo(@org.jetbrains.annotations.NotNull()
    java.lang.String route, @org.jetbrains.annotations.Nullable()
    java.lang.String popUpToRoute, boolean inclusive, boolean isSingleTop);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object closeApp(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 3)
    public final class DefaultImpls {
    }
}