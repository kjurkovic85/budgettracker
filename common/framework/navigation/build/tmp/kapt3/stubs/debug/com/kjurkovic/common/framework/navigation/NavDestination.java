package com.kjurkovic.common.framework.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001B+\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00040\u0006\"\u00020\u0004\u00a2\u0006\u0002\u0010\u0007B+\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00040\u0006\"\u00020\u0004\u00a2\u0006\u0002\u0010\bJ9\u0010\r\u001a\u00020\u00042*\u0010\u0005\u001a\u0016\u0012\u0012\b\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u000e0\u0006\"\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u000eH\u0004\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u0010\u001a\u00020\u0004H\u0086\u0002R\u0011\u0010\t\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b\u00a8\u0006\u0011"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/NavDestination;", "", "parent", "path", "", "params", "", "(Lcom/kjurkovic/common/framework/navigation/NavDestination;Ljava/lang/String;[Ljava/lang/String;)V", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V", "fullRoute", "getFullRoute", "()Ljava/lang/String;", "route", "getRoute", "Lkotlin/Pair;", "([Lkotlin/Pair;)Ljava/lang/String;", "invoke", "navigation_debug"})
public class NavDestination {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String route = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String fullRoute = null;
    
    public NavDestination(@org.jetbrains.annotations.Nullable()
    java.lang.String parent, @org.jetbrains.annotations.NotNull()
    java.lang.String path, @org.jetbrains.annotations.NotNull()
    java.lang.String... params) {
        super();
    }
    
    public NavDestination(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.framework.navigation.NavDestination parent, @org.jetbrains.annotations.NotNull()
    java.lang.String path, @org.jetbrains.annotations.NotNull()
    java.lang.String... params) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRoute() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFullRoute() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final java.lang.String getRoute(@org.jetbrains.annotations.NotNull()
    kotlin.Pair<java.lang.String, ? extends java.lang.Object>... params) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String invoke() {
        return null;
    }
}