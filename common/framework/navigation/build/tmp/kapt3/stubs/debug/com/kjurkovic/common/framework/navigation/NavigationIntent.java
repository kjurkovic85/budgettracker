package com.kjurkovic.common.framework.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/NavigationIntent;", "", "()V", "CloseApp", "NavigateBack", "NavigateTo", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent$NavigateBack;", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent$NavigateTo;", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent$CloseApp;", "navigation_debug"})
public abstract class NavigationIntent {
    
    private NavigationIntent() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/NavigationIntent$NavigateBack;", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent;", "route", "", "inclusive", "", "(Ljava/lang/String;Z)V", "getInclusive", "()Z", "getRoute", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "other", "", "hashCode", "", "toString", "navigation_debug"})
    public static final class NavigateBack extends com.kjurkovic.common.framework.navigation.NavigationIntent {
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String route = null;
        private final boolean inclusive = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.common.framework.navigation.NavigationIntent.NavigateBack copy(@org.jetbrains.annotations.Nullable()
        java.lang.String route, boolean inclusive) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public NavigateBack() {
            super();
        }
        
        public NavigateBack(@org.jetbrains.annotations.Nullable()
        java.lang.String route, boolean inclusive) {
            super();
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getRoute() {
            return null;
        }
        
        public final boolean component2() {
            return false;
        }
        
        public final boolean getInclusive() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J3\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00062\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\f\u00a8\u0006\u0019"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/NavigationIntent$NavigateTo;", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent;", "route", "", "popUpToRoute", "inclusive", "", "isSingleTop", "(Ljava/lang/String;Ljava/lang/String;ZZ)V", "getInclusive", "()Z", "getPopUpToRoute", "()Ljava/lang/String;", "getRoute", "component1", "component2", "component3", "component4", "copy", "equals", "other", "", "hashCode", "", "toString", "navigation_debug"})
    public static final class NavigateTo extends com.kjurkovic.common.framework.navigation.NavigationIntent {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String route = null;
        @org.jetbrains.annotations.Nullable()
        private final java.lang.String popUpToRoute = null;
        private final boolean inclusive = false;
        private final boolean isSingleTop = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.common.framework.navigation.NavigationIntent.NavigateTo copy(@org.jetbrains.annotations.NotNull()
        java.lang.String route, @org.jetbrains.annotations.Nullable()
        java.lang.String popUpToRoute, boolean inclusive, boolean isSingleTop) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public NavigateTo(@org.jetbrains.annotations.NotNull()
        java.lang.String route, @org.jetbrains.annotations.Nullable()
        java.lang.String popUpToRoute, boolean inclusive, boolean isSingleTop) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getRoute() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPopUpToRoute() {
            return null;
        }
        
        public final boolean component3() {
            return false;
        }
        
        public final boolean getInclusive() {
            return false;
        }
        
        public final boolean component4() {
            return false;
        }
        
        public final boolean isSingleTop() {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/NavigationIntent$CloseApp;", "Lcom/kjurkovic/common/framework/navigation/NavigationIntent;", "()V", "navigation_debug"})
    public static final class CloseApp extends com.kjurkovic.common.framework.navigation.NavigationIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.common.framework.navigation.NavigationIntent.CloseApp INSTANCE = null;
        
        private CloseApp() {
            super();
        }
    }
}