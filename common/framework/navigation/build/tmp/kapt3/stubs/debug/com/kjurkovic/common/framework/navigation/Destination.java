package com.kjurkovic.common.framework.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0007\b\t\nB#\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0005\"\u00020\u0003\u00a2\u0006\u0002\u0010\u0006\u0082\u0001\u0004\u000b\f\r\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination;", "Lcom/kjurkovic/common/framework/navigation/NavDestination;", "route", "", "params", "", "(Ljava/lang/String;[Ljava/lang/String;)V", "Categories", "Dashboard", "Profile", "Transactions", "Lcom/kjurkovic/common/framework/navigation/Destination$Dashboard;", "Lcom/kjurkovic/common/framework/navigation/Destination$Categories;", "Lcom/kjurkovic/common/framework/navigation/Destination$Transactions;", "Lcom/kjurkovic/common/framework/navigation/Destination$Profile;", "navigation_debug"})
public abstract class Destination extends com.kjurkovic.common.framework.navigation.NavDestination {
    
    private Destination(java.lang.String route, java.lang.String... params) {
        super(null, null, null);
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Dashboard;", "Lcom/kjurkovic/common/framework/navigation/Destination;", "()V", "navigation_debug"})
    public static final class Dashboard extends com.kjurkovic.common.framework.navigation.Destination {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.common.framework.navigation.Destination.Dashboard INSTANCE = null;
        
        private Dashboard() {
            super(null, null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0005"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Categories;", "Lcom/kjurkovic/common/framework/navigation/Destination;", "()V", "Add", "Edit", "navigation_debug"})
    public static final class Categories extends com.kjurkovic.common.framework.navigation.Destination {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.common.framework.navigation.Destination.Categories INSTANCE = null;
        
        private Categories() {
            super(null, null);
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Categories$Add;", "Lcom/kjurkovic/common/framework/navigation/NavDestination;", "()V", "navigation_debug"})
        public static final class Add extends com.kjurkovic.common.framework.navigation.NavDestination {
            @org.jetbrains.annotations.NotNull()
            public static final com.kjurkovic.common.framework.navigation.Destination.Categories.Add INSTANCE = null;
            
            private Add() {
                super(null, null, null);
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0086\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Categories$Edit;", "Lcom/kjurkovic/common/framework/navigation/NavDestination;", "()V", "CATEGORY_ID", "", "invoke", "categoryId", "", "navigation_debug"})
        public static final class Edit extends com.kjurkovic.common.framework.navigation.NavDestination {
            @org.jetbrains.annotations.NotNull()
            public static final com.kjurkovic.common.framework.navigation.Destination.Categories.Edit INSTANCE = null;
            @org.jetbrains.annotations.NotNull()
            public static final java.lang.String CATEGORY_ID = "id";
            
            private Edit() {
                super(null, null, null);
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String invoke(long categoryId) {
                return null;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0005"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Transactions;", "Lcom/kjurkovic/common/framework/navigation/Destination;", "()V", "Add", "Edit", "navigation_debug"})
    public static final class Transactions extends com.kjurkovic.common.framework.navigation.Destination {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.common.framework.navigation.Destination.Transactions INSTANCE = null;
        
        private Transactions() {
            super(null, null);
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Transactions$Add;", "Lcom/kjurkovic/common/framework/navigation/NavDestination;", "()V", "navigation_debug"})
        public static final class Add extends com.kjurkovic.common.framework.navigation.NavDestination {
            @org.jetbrains.annotations.NotNull()
            public static final com.kjurkovic.common.framework.navigation.Destination.Transactions.Add INSTANCE = null;
            
            private Add() {
                super(null, null, null);
            }
        }
        
        @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004H\u0086\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Transactions$Edit;", "Lcom/kjurkovic/common/framework/navigation/NavDestination;", "()V", "TRANSACTION_ID", "", "invoke", "transactionId", "navigation_debug"})
        public static final class Edit extends com.kjurkovic.common.framework.navigation.NavDestination {
            @org.jetbrains.annotations.NotNull()
            public static final com.kjurkovic.common.framework.navigation.Destination.Transactions.Edit INSTANCE = null;
            @org.jetbrains.annotations.NotNull()
            public static final java.lang.String TRANSACTION_ID = "id";
            
            private Edit() {
                super(null, null, null);
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String invoke(@org.jetbrains.annotations.NotNull()
            java.lang.String transactionId) {
                return null;
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/common/framework/navigation/Destination$Profile;", "Lcom/kjurkovic/common/framework/navigation/Destination;", "()V", "navigation_debug"})
    public static final class Profile extends com.kjurkovic.common.framework.navigation.Destination {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.common.framework.navigation.Destination.Profile INSTANCE = null;
        
        private Profile() {
            super(null, null);
        }
    }
}