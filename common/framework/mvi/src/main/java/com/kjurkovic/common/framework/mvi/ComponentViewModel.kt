package com.kjurkovic.common.framework.mvi

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

abstract class ComponentViewModel<ViewState, Intent, SideEffect>(
    protected val navigator: ScreenNavigator,
    private val dispatcherProvider: DispatcherProvider,
    viewState: ViewState
) : ViewModel() {

    protected val _viewState: MutableStateFlow<ViewState> = MutableStateFlow(viewState)
    val viewState = _viewState.asStateFlow()

    val intentFlow = MutableSharedFlow<Intent>(
        extraBufferCapacity = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )

    val _sideEffects = MutableSharedFlow<SideEffect>(
        extraBufferCapacity = Int.MAX_VALUE
    )
    val sideEffects = _sideEffects.asSharedFlow()

    init {
        launchMain {
            intentFlow.collect { reduce(it) }
        }
    }

    protected fun setState(reducer: ViewState.() -> ViewState) {
        _viewState.update(reducer)
    }


    protected abstract suspend fun reduce(intent: Intent)

    protected open suspend fun onBackClicked() = navigator.navigateBack()

    protected fun launchMain(block: suspend CoroutineScope.() -> Unit): Job =
        viewModelScope.launch(
            context = dispatcherProvider.main(),
            block = block
        )

    protected fun launchIo(block: suspend CoroutineScope.() -> Unit): Job =
        viewModelScope.launch(
            context = dispatcherProvider.io(),
            block = block
        )

    @Composable
    private fun RegisterBackHandler() {
        BackHandler(true) {
            Timber.d("Back clicked")
            launchMain { onBackClicked() }
        }
    }

    @Composable
    fun Screen(
        block: @Composable (ViewState, MutableSharedFlow<Intent>, SharedFlow<SideEffect>) -> Unit
    ) {
        RegisterBackHandler()
        block(_viewState.collectAsState().value, intentFlow, sideEffects)
    }
}