plugins {
    id("base-android-compose-library")
}

dependencies {
    implementation(libs.androidx.lifecycle.savedState)
    implementation(libs.bundles.compose)
    implementation(libs.timber)
    implementation(libs.kotlin.coroutines)
    implementation(projects.common.framework.navigation)
    implementation(projects.common.wrappers.dispatchers)
}
