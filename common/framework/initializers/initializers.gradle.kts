plugins {
    id("base-android-library")
}

dependencies {
    implementation(libs.kotlin.coroutines)
}
