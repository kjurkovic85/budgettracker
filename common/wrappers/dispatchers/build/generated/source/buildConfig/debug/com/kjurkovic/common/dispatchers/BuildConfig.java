/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.kjurkovic.common.dispatchers;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.kjurkovic.common.dispatchers";
  public static final String BUILD_TYPE = "debug";
}
