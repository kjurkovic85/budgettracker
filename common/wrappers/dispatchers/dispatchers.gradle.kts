plugins {
    id("base-android-library")
}

dependencies {
    implementation(libs.hilt.android)
    implementation (libs.kotlin.coroutines)
    kapt(libs.hilt.androidCompiler)
}
