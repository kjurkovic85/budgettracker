package com.kjurkovic.wrappers.logger

import android.app.Application
import com.kjurkovic.annotations.qualifiers.DebugBuild
import com.kjurkovic.common.framework.initializers.AppInitializer
import timber.log.Timber
import javax.inject.Inject

class TimberInitializer @Inject constructor(
    @DebugBuild private val isDebug: Boolean
) : AppInitializer {

    override fun init(app: Application) {
        val tree = if (isDebug) Timber.DebugTree() else ProductionTree()
        Timber.plant(tree)
    }
}