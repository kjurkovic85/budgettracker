package com.kjurkovic.wrappers.logger.di

import com.kjurkovic.common.framework.initializers.AppInitializer
import com.kjurkovic.wrappers.logger.TimberInitializer
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet

@InstallIn(SingletonComponent::class)
@Module
abstract class LoggerModule {

    @Binds
    @IntoSet
    abstract fun bindTimberInitializer(bind: TimberInitializer): AppInitializer
}
