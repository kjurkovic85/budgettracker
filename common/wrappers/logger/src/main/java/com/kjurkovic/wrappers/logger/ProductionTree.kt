package com.kjurkovic.wrappers.logger

import timber.log.Timber

class ProductionTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        // when we log something in production a puppy dies somewhere
        // don't kill puppies
    }
}