package com.kjurkovic.wrappers.logger;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/kjurkovic/wrappers/logger/TimberInitializer;", "Lcom/kjurkovic/common/framework/initializers/AppInitializer;", "isDebug", "", "(Z)V", "init", "", "app", "Landroid/app/Application;", "logger_debug"})
public final class TimberInitializer implements com.kjurkovic.common.framework.initializers.AppInitializer {
    private final boolean isDebug = false;
    
    @javax.inject.Inject()
    public TimberInitializer(@com.kjurkovic.annotations.qualifiers.DebugBuild()
    boolean isDebug) {
        super();
    }
    
    @java.lang.Override()
    public void init(@org.jetbrains.annotations.NotNull()
    android.app.Application app) {
    }
}