package com.kjurkovic.wrappers.logger;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TimberInitializer_Factory implements Factory<TimberInitializer> {
  private final Provider<Boolean> isDebugProvider;

  public TimberInitializer_Factory(Provider<Boolean> isDebugProvider) {
    this.isDebugProvider = isDebugProvider;
  }

  @Override
  public TimberInitializer get() {
    return newInstance(isDebugProvider.get());
  }

  public static TimberInitializer_Factory create(Provider<Boolean> isDebugProvider) {
    return new TimberInitializer_Factory(isDebugProvider);
  }

  public static TimberInitializer newInstance(boolean isDebug) {
    return new TimberInitializer(isDebug);
  }
}
