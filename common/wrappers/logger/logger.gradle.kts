plugins {
    id("base-android-library")
}

dependencies {
    implementation(libs.hilt.android)
    implementation(libs.kotlin.coroutines)
    implementation(libs.timber)
    implementation(projects.common.annotations)
    implementation(projects.common.framework.initializers)
    kapt(libs.hilt.androidCompiler)
}
