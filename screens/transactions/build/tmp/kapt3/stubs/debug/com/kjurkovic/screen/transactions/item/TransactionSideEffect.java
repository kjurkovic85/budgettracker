package com.kjurkovic.screen.transactions.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0005\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect;", "", "()V", "CloseBottomSheet", "OpenBottomSheet", "Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect$OpenBottomSheet;", "Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect$CloseBottomSheet;", "transactions_debug"})
public abstract class TransactionSideEffect {
    
    private TransactionSideEffect() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect$OpenBottomSheet;", "Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect;", "()V", "transactions_debug"})
    public static final class OpenBottomSheet extends com.kjurkovic.screen.transactions.item.TransactionSideEffect {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionSideEffect.OpenBottomSheet INSTANCE = null;
        
        private OpenBottomSheet() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect$CloseBottomSheet;", "Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect;", "()V", "transactions_debug"})
    public static final class CloseBottomSheet extends com.kjurkovic.screen.transactions.item.TransactionSideEffect {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionSideEffect.CloseBottomSheet INSTANCE = null;
        
        private CloseBottomSheet() {
            super();
        }
    }
}