package com.kjurkovic.screen.transactions.list;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000<\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0003\u001a(\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\n\u001a\u00020\u000bH\u0003\u001a\u001e\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00010\u0010H\u0003\u001a\u0010\u0010\u0011\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u0013H\u0007\u001a\u001e\u0010\u0011\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0003\u00a8\u0006\u0014"}, d2 = {"HeaderRow", "", "date", "Ljava/time/LocalDate;", "TransactionList", "viewState", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewState;", "intentChannel", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/kjurkovic/screen/transactions/list/TransactionsIntent;", "modifier", "Landroidx/compose/ui/Modifier;", "TransactionRow", "transaction", "Lcom/kjurkovic/repo/transaction_repo/model/TransactionModel;", "onClick", "Lkotlin/Function0;", "TransactionsScreen", "viewModel", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel;", "transactions_debug"})
public final class TransactionsScreenKt {
    
    @androidx.compose.runtime.Composable()
    public static final void TransactionsScreen(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.transactions.list.TransactionsViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void TransactionsScreen(com.kjurkovic.screen.transactions.list.TransactionsViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.transactions.list.TransactionsIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void TransactionList(com.kjurkovic.screen.transactions.list.TransactionsViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.transactions.list.TransactionsIntent> intentChannel, androidx.compose.ui.Modifier modifier) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void HeaderRow(java.time.LocalDate date) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void TransactionRow(com.kjurkovic.repo.transaction_repo.model.TransactionModel transaction, kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
    }
}