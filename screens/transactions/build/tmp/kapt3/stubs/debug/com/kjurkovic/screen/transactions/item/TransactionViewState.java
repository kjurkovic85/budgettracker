package com.kjurkovic.screen.transactions.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b$\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B\u0087\u0001\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0011\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u0005\u00a2\u0006\u0002\u0010\u0015J\u000b\u0010(\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003J\u000f\u0010*\u001a\b\u0012\u0004\u0012\u00020\f0\u0005H\u00c6\u0003J\u000f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010,\u001a\u00020\u0003H\u00c6\u0003J\t\u0010-\u001a\u00020\u0003H\u00c6\u0003J\t\u0010.\u001a\u00020\nH\u00c6\u0003J\t\u0010/\u001a\u00020\fH\u00c6\u0003J\u0010\u00100\u001a\u0004\u0018\u00010\u000eH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001bJ\u000b\u00101\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u00102\u001a\u00020\u0011H\u00c6\u0003J\u0090\u0001\u00103\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0010\u001a\u00020\u00112\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u0005H\u00c6\u0001\u00a2\u0006\u0002\u00104J\u0013\u00105\u001a\u00020\u00112\b\u00106\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00107\u001a\u000208H\u00d6\u0001J\u0006\u00109\u001a\u00020\u0011J\t\u0010:\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0015\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\n\n\u0002\u0010\u001c\u001a\u0004\b\u001a\u0010\u001bR\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0017R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0017R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010#R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0017R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\f0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0019R\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\'\u00a8\u0006;"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionViewState;", "", "screenTitle", "", "categories", "", "Lcom/kjurkovic/category_repo/models/CategoryModel;", "description", "amount", "date", "Ljava/time/LocalDate;", "type", "Lcom/kjurkovic/data/common/TransactionType;", "categoryId", "", "categoryName", "isEditMode", "", "dialogType", "Lcom/kjurkovic/screen/transactions/item/TransactionAction;", "transactionTypes", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/time/LocalDate;Lcom/kjurkovic/data/common/TransactionType;Ljava/lang/Long;Ljava/lang/String;ZLcom/kjurkovic/screen/transactions/item/TransactionAction;Ljava/util/List;)V", "getAmount", "()Ljava/lang/String;", "getCategories", "()Ljava/util/List;", "getCategoryId", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getCategoryName", "getDate", "()Ljava/time/LocalDate;", "getDescription", "getDialogType", "()Lcom/kjurkovic/screen/transactions/item/TransactionAction;", "()Z", "getScreenTitle", "getTransactionTypes", "getType", "()Lcom/kjurkovic/data/common/TransactionType;", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/time/LocalDate;Lcom/kjurkovic/data/common/TransactionType;Ljava/lang/Long;Ljava/lang/String;ZLcom/kjurkovic/screen/transactions/item/TransactionAction;Ljava/util/List;)Lcom/kjurkovic/screen/transactions/item/TransactionViewState;", "equals", "other", "hashCode", "", "isFormValid", "toString", "transactions_debug"})
public final class TransactionViewState {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String screenTitle = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.kjurkovic.category_repo.models.CategoryModel> categories = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String description = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String amount = null;
    @org.jetbrains.annotations.NotNull()
    private final java.time.LocalDate date = null;
    @org.jetbrains.annotations.NotNull()
    private final com.kjurkovic.data.common.TransactionType type = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Long categoryId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String categoryName = null;
    private final boolean isEditMode = false;
    @org.jetbrains.annotations.Nullable()
    private final com.kjurkovic.screen.transactions.item.TransactionAction dialogType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.kjurkovic.data.common.TransactionType> transactionTypes = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.screen.transactions.item.TransactionViewState copy(@org.jetbrains.annotations.Nullable()
    java.lang.String screenTitle, @org.jetbrains.annotations.NotNull()
    java.util.List<com.kjurkovic.category_repo.models.CategoryModel> categories, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    java.lang.String amount, @org.jetbrains.annotations.NotNull()
    java.time.LocalDate date, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.common.TransactionType type, @org.jetbrains.annotations.Nullable()
    java.lang.Long categoryId, @org.jetbrains.annotations.Nullable()
    java.lang.String categoryName, boolean isEditMode, @org.jetbrains.annotations.Nullable()
    com.kjurkovic.screen.transactions.item.TransactionAction dialogType, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.kjurkovic.data.common.TransactionType> transactionTypes) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public TransactionViewState() {
        super();
    }
    
    public TransactionViewState(@org.jetbrains.annotations.Nullable()
    java.lang.String screenTitle, @org.jetbrains.annotations.NotNull()
    java.util.List<com.kjurkovic.category_repo.models.CategoryModel> categories, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    java.lang.String amount, @org.jetbrains.annotations.NotNull()
    java.time.LocalDate date, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.common.TransactionType type, @org.jetbrains.annotations.Nullable()
    java.lang.Long categoryId, @org.jetbrains.annotations.Nullable()
    java.lang.String categoryName, boolean isEditMode, @org.jetbrains.annotations.Nullable()
    com.kjurkovic.screen.transactions.item.TransactionAction dialogType, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.kjurkovic.data.common.TransactionType> transactionTypes) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getScreenTitle() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.kjurkovic.category_repo.models.CategoryModel> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.kjurkovic.category_repo.models.CategoryModel> getCategories() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAmount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.LocalDate component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.LocalDate getDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.common.TransactionType component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.common.TransactionType getType() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getCategoryId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCategoryName() {
        return null;
    }
    
    public final boolean component9() {
        return false;
    }
    
    public final boolean isEditMode() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.kjurkovic.screen.transactions.item.TransactionAction component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.kjurkovic.screen.transactions.item.TransactionAction getDialogType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.kjurkovic.data.common.TransactionType> component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.kjurkovic.data.common.TransactionType> getTransactionTypes() {
        return null;
    }
    
    public final boolean isFormValid() {
        return false;
    }
}