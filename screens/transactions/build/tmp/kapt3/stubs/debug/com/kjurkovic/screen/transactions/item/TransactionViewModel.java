package com.kjurkovic.screen.transactions.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u0000 \u00162\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u0016\u0017B3\b\u0007\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0019\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0094@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u0011\u0010\u0014\u001a\u00020\u0011H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0018"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionViewModel;", "Lcom/kjurkovic/common/framework/mvi/ComponentViewModel;", "Lcom/kjurkovic/screen/transactions/item/TransactionViewState;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect;", "transactionId", "Ljava/util/UUID;", "transactionsRepository", "Lcom/kjurkovic/repo/transaction_repo/TransactionsRepository;", "categoriesRepository", "Lcom/kjurkovic/category_repo/CategoriesRepository;", "navigator", "Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "(Ljava/util/UUID;Lcom/kjurkovic/repo/transaction_repo/TransactionsRepository;Lcom/kjurkovic/category_repo/CategoriesRepository;Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;Lcom/kjurkovic/common/dispatchers/DispatcherProvider;)V", "reduce", "", "intent", "(Lcom/kjurkovic/screen/transactions/item/TransactionIntent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "saveTransaction", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "Factory", "transactions_debug"})
public final class TransactionViewModel extends com.kjurkovic.common.framework.mvi.ComponentViewModel<com.kjurkovic.screen.transactions.item.TransactionViewState, com.kjurkovic.screen.transactions.item.TransactionIntent, com.kjurkovic.screen.transactions.item.TransactionSideEffect> {
    private final java.util.UUID transactionId = null;
    private final com.kjurkovic.repo.transaction_repo.TransactionsRepository transactionsRepository = null;
    private final com.kjurkovic.category_repo.CategoriesRepository categoriesRepository = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.screen.transactions.item.TransactionViewModel.Companion Companion = null;
    
    @dagger.assisted.AssistedInject()
    public TransactionViewModel(@org.jetbrains.annotations.Nullable()
    @dagger.assisted.Assisted(value = "transactionId")
    java.util.UUID transactionId, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.repo.transaction_repo.TransactionsRepository transactionsRepository, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.category_repo.CategoriesRepository categoriesRepository, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.framework.navigation.ScreenNavigator navigator, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Object reduce(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.transactions.item.TransactionIntent intent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    private final java.lang.Object saveTransaction(kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u00020\u00032\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionViewModel$Factory;", "", "create", "Lcom/kjurkovic/screen/transactions/item/TransactionViewModel;", "transactionId", "Ljava/util/UUID;", "transactions_debug"})
    @dagger.assisted.AssistedFactory()
    public static abstract interface Factory {
        
        @org.jetbrains.annotations.NotNull()
        public abstract com.kjurkovic.screen.transactions.item.TransactionViewModel create(@org.jetbrains.annotations.Nullable()
        @dagger.assisted.Assisted(value = "transactionId")
        java.util.UUID transactionId);
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007\u00a8\u0006\t"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionViewModel$Companion;", "", "()V", "create", "Lcom/kjurkovic/screen/transactions/item/TransactionViewModel;", "assistedFactory", "Lcom/kjurkovic/screen/transactions/item/TransactionViewModel$Factory;", "transactionId", "Ljava/util/UUID;", "transactions_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @androidx.compose.runtime.Composable()
        public final com.kjurkovic.screen.transactions.item.TransactionViewModel create(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.screen.transactions.item.TransactionViewModel.Factory assistedFactory, @org.jetbrains.annotations.Nullable()
        java.util.UUID transactionId) {
            return null;
        }
    }
}