package com.kjurkovic.screen.transactions.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000X\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a9\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032!\u0010\u0005\u001a\u001d\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\t\u0012\u0004\u0012\u00020\u00010\u0006H\u0007\u001a\u001e\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH\u0003\u001a\u0010\u0010\u0010\u001a\u00020\u00012\u0006\u0010\u0011\u001a\u00020\u0012H\u0007\u001a&\u0010\u0010\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0003\u001a\u001e\u0010\u0015\u001a\u00020\u00012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u00172\u0006\u0010\u0013\u001a\u00020\u0014H\u0003\u001a=\u0010\u0019\u001a\u00020\u00012\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2!\u0010\u001e\u001a\u001d\u0012\u0013\u0012\u00110\u001b\u00a2\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\u001f\u0012\u0004\u0012\u00020\u00010\u0006H\u0002\u00a8\u0006 "}, d2 = {"CategorySheet", "", "categories", "", "Lcom/kjurkovic/category_repo/models/CategoryModel;", "onCategorySelected", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "category", "TransactionDialog", "viewState", "Lcom/kjurkovic/screen/transactions/item/TransactionViewState;", "intentChannel", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "TransactionScreen", "viewModel", "Lcom/kjurkovic/screen/transactions/item/TransactionViewModel;", "sheetState", "Landroidx/compose/material/ModalBottomSheetState;", "TransactionSideEffects", "sideEffects", "Lkotlinx/coroutines/flow/SharedFlow;", "Lcom/kjurkovic/screen/transactions/item/TransactionSideEffect;", "showDatePicker", "selectedDate", "Ljava/time/LocalDate;", "context", "Landroid/content/Context;", "onDatePicked", "date", "transactions_debug"})
public final class TransactionScreenKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void TransactionScreen(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.transactions.item.TransactionViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void TransactionDialog(com.kjurkovic.screen.transactions.item.TransactionViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.transactions.item.TransactionIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    private static final void TransactionSideEffects(kotlinx.coroutines.flow.SharedFlow<? extends com.kjurkovic.screen.transactions.item.TransactionSideEffect> sideEffects, androidx.compose.material.ModalBottomSheetState sheetState) {
    }
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    private static final void TransactionScreen(com.kjurkovic.screen.transactions.item.TransactionViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.transactions.item.TransactionIntent> intentChannel, androidx.compose.material.ModalBottomSheetState sheetState) {
    }
    
    @androidx.compose.runtime.Composable()
    public static final void CategorySheet(@org.jetbrains.annotations.NotNull()
    java.util.List<com.kjurkovic.category_repo.models.CategoryModel> categories, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.kjurkovic.category_repo.models.CategoryModel, kotlin.Unit> onCategorySelected) {
    }
    
    private static final void showDatePicker(java.time.LocalDate selectedDate, android.content.Context context, kotlin.jvm.functions.Function1<? super java.time.LocalDate, kotlin.Unit> onDatePicked) {
    }
}