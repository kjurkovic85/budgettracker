package com.kjurkovic.screen.transactions.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\f\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0007\u00a8\u0006\u0003"}, d2 = {"stringResource", "", "Lcom/kjurkovic/data/common/TransactionType;", "transactions_debug"})
public final class TransactionTypeExtKt {
    
    @androidx.annotation.StringRes()
    public static final int stringResource(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.common.TransactionType $this$stringResource) {
        return 0;
    }
}