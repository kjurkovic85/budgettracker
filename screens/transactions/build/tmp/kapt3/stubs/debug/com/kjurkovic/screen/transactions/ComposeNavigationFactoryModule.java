package com.kjurkovic.screen.transactions;

import java.lang.System;

@dagger.hilt.InstallIn(value = {dagger.hilt.android.components.ActivityRetainedComponent.class})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\ba\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0006"}, d2 = {"Lcom/kjurkovic/screen/transactions/ComposeNavigationFactoryModule;", "", "bindNavigationFactory", "Lcom/kjurkovic/common/framework/navigation/NavigationFactory;", "factory", "Lcom/kjurkovic/screen/transactions/TransactionsNavigationFactory;", "transactions_debug"})
@dagger.Module()
public abstract interface ComposeNavigationFactoryModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.multibindings.IntoSet()
    @dagger.Binds()
    public abstract com.kjurkovic.common.framework.navigation.NavigationFactory bindNavigationFactory(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.transactions.TransactionsNavigationFactory factory);
}