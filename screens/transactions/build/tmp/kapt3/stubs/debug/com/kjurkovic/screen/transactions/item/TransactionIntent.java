package com.kjurkovic.screen.transactions.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u000b\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\rB\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000b\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "", "()V", "AmountChanged", "BackClicked", "CategoryChanged", "CategoryPickerRequested", "ConfirmDelete", "DateChanged", "DeleteClicked", "DescriptionChanged", "DismissDialog", "SaveClicked", "TransactionTypeChanged", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DescriptionChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$AmountChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DateChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$CategoryChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$TransactionTypeChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$CategoryPickerRequested;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$SaveClicked;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$BackClicked;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DeleteClicked;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$ConfirmDelete;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DismissDialog;", "transactions_debug"})
public abstract class TransactionIntent {
    
    private TransactionIntent() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DescriptionChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "description", "", "(Ljava/lang/String;)V", "getDescription", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "transactions_debug"})
    public static final class DescriptionChanged extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String description = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.screen.transactions.item.TransactionIntent.DescriptionChanged copy(@org.jetbrains.annotations.NotNull()
        java.lang.String description) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public DescriptionChanged(@org.jetbrains.annotations.NotNull()
        java.lang.String description) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDescription() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$AmountChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "amount", "", "(Ljava/lang/String;)V", "getAmount", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "transactions_debug"})
    public static final class AmountChanged extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String amount = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.screen.transactions.item.TransactionIntent.AmountChanged copy(@org.jetbrains.annotations.NotNull()
        java.lang.String amount) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public AmountChanged(@org.jetbrains.annotations.NotNull()
        java.lang.String amount) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getAmount() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DateChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "date", "Ljava/time/LocalDate;", "(Ljava/time/LocalDate;)V", "getDate", "()Ljava/time/LocalDate;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "transactions_debug"})
    public static final class DateChanged extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        private final java.time.LocalDate date = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.screen.transactions.item.TransactionIntent.DateChanged copy(@org.jetbrains.annotations.NotNull()
        java.time.LocalDate date) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public DateChanged(@org.jetbrains.annotations.NotNull()
        java.time.LocalDate date) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.time.LocalDate component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.time.LocalDate getDate() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$CategoryChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "categoryId", "", "categoryName", "", "(JLjava/lang/String;)V", "getCategoryId", "()J", "getCategoryName", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "transactions_debug"})
    public static final class CategoryChanged extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        private final long categoryId = 0L;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String categoryName = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.screen.transactions.item.TransactionIntent.CategoryChanged copy(long categoryId, @org.jetbrains.annotations.NotNull()
        java.lang.String categoryName) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public CategoryChanged(long categoryId, @org.jetbrains.annotations.NotNull()
        java.lang.String categoryName) {
            super();
        }
        
        public final long component1() {
            return 0L;
        }
        
        public final long getCategoryId() {
            return 0L;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getCategoryName() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$TransactionTypeChanged;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "type", "Lcom/kjurkovic/data/common/TransactionType;", "(Lcom/kjurkovic/data/common/TransactionType;)V", "getType", "()Lcom/kjurkovic/data/common/TransactionType;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "transactions_debug"})
    public static final class TransactionTypeChanged extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        private final com.kjurkovic.data.common.TransactionType type = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.screen.transactions.item.TransactionIntent.TransactionTypeChanged copy(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.data.common.TransactionType type) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public TransactionTypeChanged(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.data.common.TransactionType type) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.data.common.TransactionType component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.kjurkovic.data.common.TransactionType getType() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$CategoryPickerRequested;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "()V", "transactions_debug"})
    public static final class CategoryPickerRequested extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionIntent.CategoryPickerRequested INSTANCE = null;
        
        private CategoryPickerRequested() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$SaveClicked;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "()V", "transactions_debug"})
    public static final class SaveClicked extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionIntent.SaveClicked INSTANCE = null;
        
        private SaveClicked() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$BackClicked;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "()V", "transactions_debug"})
    public static final class BackClicked extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionIntent.BackClicked INSTANCE = null;
        
        private BackClicked() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DeleteClicked;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "()V", "transactions_debug"})
    public static final class DeleteClicked extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionIntent.DeleteClicked INSTANCE = null;
        
        private DeleteClicked() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$ConfirmDelete;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "()V", "transactions_debug"})
    public static final class ConfirmDelete extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionIntent.ConfirmDelete INSTANCE = null;
        
        private ConfirmDelete() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/kjurkovic/screen/transactions/item/TransactionIntent$DismissDialog;", "Lcom/kjurkovic/screen/transactions/item/TransactionIntent;", "()V", "transactions_debug"})
    public static final class DismissDialog extends com.kjurkovic.screen.transactions.item.TransactionIntent {
        @org.jetbrains.annotations.NotNull()
        public static final com.kjurkovic.screen.transactions.item.TransactionIntent.DismissDialog INSTANCE = null;
        
        private DismissDialog() {
            super();
        }
    }
}