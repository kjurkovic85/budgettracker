package com.kjurkovic.screen.transactions.list;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 \u00142\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u0014\u0015B\u001f\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0019\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0094@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0016"}, d2 = {"Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel;", "Lcom/kjurkovic/common/framework/mvi/ComponentViewModel;", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewState;", "Lcom/kjurkovic/screen/transactions/list/TransactionsIntent;", "Ljava/lang/Void;", "transactionsRepository", "Lcom/kjurkovic/repo/transaction_repo/TransactionsRepository;", "navigator", "Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "(Lcom/kjurkovic/repo/transaction_repo/TransactionsRepository;Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;Lcom/kjurkovic/common/dispatchers/DispatcherProvider;)V", "currentPage", "", "loading", "", "reduce", "", "intent", "(Lcom/kjurkovic/screen/transactions/list/TransactionsIntent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "Factory", "transactions_debug"})
public final class TransactionsViewModel extends com.kjurkovic.common.framework.mvi.ComponentViewModel<com.kjurkovic.screen.transactions.list.TransactionsViewState, com.kjurkovic.screen.transactions.list.TransactionsIntent, java.lang.Void> {
    private final com.kjurkovic.repo.transaction_repo.TransactionsRepository transactionsRepository = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.screen.transactions.list.TransactionsViewModel.Companion Companion = null;
    private static final int PAGE_SIZE = 30;
    private int currentPage = 1;
    private boolean loading = false;
    
    @dagger.assisted.AssistedInject()
    public TransactionsViewModel(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.repo.transaction_repo.TransactionsRepository transactionsRepository, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.framework.navigation.ScreenNavigator navigator, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Object reduce(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.transactions.list.TransactionsIntent intent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel$Factory;", "", "create", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel;", "transactions_debug"})
    @dagger.assisted.AssistedFactory()
    public static abstract interface Factory {
        
        @org.jetbrains.annotations.NotNull()
        public abstract com.kjurkovic.screen.transactions.list.TransactionsViewModel create();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel$Companion;", "", "()V", "PAGE_SIZE", "", "create", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel;", "assistedFactory", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel$Factory;", "transactions_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @androidx.compose.runtime.Composable()
        public final com.kjurkovic.screen.transactions.list.TransactionsViewModel create(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.screen.transactions.list.TransactionsViewModel.Factory assistedFactory) {
            return null;
        }
    }
}