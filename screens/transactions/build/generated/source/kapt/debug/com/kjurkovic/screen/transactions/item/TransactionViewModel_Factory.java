package com.kjurkovic.screen.transactions.item;

import com.kjurkovic.category_repo.CategoriesRepository;
import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.common.framework.navigation.ScreenNavigator;
import com.kjurkovic.repo.transaction_repo.TransactionsRepository;
import dagger.internal.DaggerGenerated;
import java.util.UUID;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TransactionViewModel_Factory {
  private final Provider<TransactionsRepository> transactionsRepositoryProvider;

  private final Provider<CategoriesRepository> categoriesRepositoryProvider;

  private final Provider<ScreenNavigator> navigatorProvider;

  private final Provider<DispatcherProvider> dispatcherProvider;

  public TransactionViewModel_Factory(
      Provider<TransactionsRepository> transactionsRepositoryProvider,
      Provider<CategoriesRepository> categoriesRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    this.transactionsRepositoryProvider = transactionsRepositoryProvider;
    this.categoriesRepositoryProvider = categoriesRepositoryProvider;
    this.navigatorProvider = navigatorProvider;
    this.dispatcherProvider = dispatcherProvider;
  }

  public TransactionViewModel get(UUID transactionId) {
    return newInstance(transactionId, transactionsRepositoryProvider.get(), categoriesRepositoryProvider.get(), navigatorProvider.get(), dispatcherProvider.get());
  }

  public static TransactionViewModel_Factory create(
      Provider<TransactionsRepository> transactionsRepositoryProvider,
      Provider<CategoriesRepository> categoriesRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    return new TransactionViewModel_Factory(transactionsRepositoryProvider, categoriesRepositoryProvider, navigatorProvider, dispatcherProvider);
  }

  public static TransactionViewModel newInstance(UUID transactionId,
      TransactionsRepository transactionsRepository, CategoriesRepository categoriesRepository,
      ScreenNavigator navigator, DispatcherProvider dispatcherProvider) {
    return new TransactionViewModel(transactionId, transactionsRepository, categoriesRepository, navigator, dispatcherProvider);
  }
}
