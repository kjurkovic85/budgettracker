package com.kjurkovic.screen.transactions.item;

import dagger.internal.DaggerGenerated;
import dagger.internal.InstanceFactory;
import java.util.UUID;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TransactionViewModel_Factory_Impl implements TransactionViewModel.Factory {
  private final TransactionViewModel_Factory delegateFactory;

  TransactionViewModel_Factory_Impl(TransactionViewModel_Factory delegateFactory) {
    this.delegateFactory = delegateFactory;
  }

  @Override
  public TransactionViewModel create(UUID transactionId) {
    return delegateFactory.get(transactionId);
  }

  public static Provider<TransactionViewModel.Factory> create(
      TransactionViewModel_Factory delegateFactory) {
    return InstanceFactory.create(new TransactionViewModel_Factory_Impl(delegateFactory));
  }
}
