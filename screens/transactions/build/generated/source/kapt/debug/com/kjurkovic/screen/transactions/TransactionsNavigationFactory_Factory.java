package com.kjurkovic.screen.transactions;

import com.kjurkovic.screen.transactions.item.TransactionViewModel;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TransactionsNavigationFactory_Factory implements Factory<TransactionsNavigationFactory> {
  private final Provider<TransactionViewModel.Factory> dashboardViewModelFactoryProvider;

  public TransactionsNavigationFactory_Factory(
      Provider<TransactionViewModel.Factory> dashboardViewModelFactoryProvider) {
    this.dashboardViewModelFactoryProvider = dashboardViewModelFactoryProvider;
  }

  @Override
  public TransactionsNavigationFactory get() {
    return newInstance(dashboardViewModelFactoryProvider.get());
  }

  public static TransactionsNavigationFactory_Factory create(
      Provider<TransactionViewModel.Factory> dashboardViewModelFactoryProvider) {
    return new TransactionsNavigationFactory_Factory(dashboardViewModelFactoryProvider);
  }

  public static TransactionsNavigationFactory newInstance(
      TransactionViewModel.Factory dashboardViewModelFactory) {
    return new TransactionsNavigationFactory(dashboardViewModelFactory);
  }
}
