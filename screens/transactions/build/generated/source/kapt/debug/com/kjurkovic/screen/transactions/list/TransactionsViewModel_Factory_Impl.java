package com.kjurkovic.screen.transactions.list;

import dagger.internal.DaggerGenerated;
import dagger.internal.InstanceFactory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TransactionsViewModel_Factory_Impl implements TransactionsViewModel.Factory {
  private final TransactionsViewModel_Factory delegateFactory;

  TransactionsViewModel_Factory_Impl(TransactionsViewModel_Factory delegateFactory) {
    this.delegateFactory = delegateFactory;
  }

  @Override
  public TransactionsViewModel create() {
    return delegateFactory.get();
  }

  public static Provider<TransactionsViewModel.Factory> create(
      TransactionsViewModel_Factory delegateFactory) {
    return InstanceFactory.create(new TransactionsViewModel_Factory_Impl(delegateFactory));
  }
}
