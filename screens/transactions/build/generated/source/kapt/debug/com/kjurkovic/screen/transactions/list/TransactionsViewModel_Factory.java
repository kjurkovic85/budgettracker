package com.kjurkovic.screen.transactions.list;

import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.common.framework.navigation.ScreenNavigator;
import com.kjurkovic.repo.transaction_repo.TransactionsRepository;
import dagger.internal.DaggerGenerated;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TransactionsViewModel_Factory {
  private final Provider<TransactionsRepository> transactionsRepositoryProvider;

  private final Provider<ScreenNavigator> navigatorProvider;

  private final Provider<DispatcherProvider> dispatcherProvider;

  public TransactionsViewModel_Factory(
      Provider<TransactionsRepository> transactionsRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    this.transactionsRepositoryProvider = transactionsRepositoryProvider;
    this.navigatorProvider = navigatorProvider;
    this.dispatcherProvider = dispatcherProvider;
  }

  public TransactionsViewModel get() {
    return newInstance(transactionsRepositoryProvider.get(), navigatorProvider.get(), dispatcherProvider.get());
  }

  public static TransactionsViewModel_Factory create(
      Provider<TransactionsRepository> transactionsRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    return new TransactionsViewModel_Factory(transactionsRepositoryProvider, navigatorProvider, dispatcherProvider);
  }

  public static TransactionsViewModel newInstance(TransactionsRepository transactionsRepository,
      ScreenNavigator navigator, DispatcherProvider dispatcherProvider) {
    return new TransactionsViewModel(transactionsRepository, navigator, dispatcherProvider);
  }
}
