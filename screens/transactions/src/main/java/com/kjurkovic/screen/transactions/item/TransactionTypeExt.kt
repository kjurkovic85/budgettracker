package com.kjurkovic.screen.transactions.item

import androidx.annotation.StringRes
import com.kjurkovic.data.common.TransactionType
import com.kjurkovic.screen.transactions.R

@StringRes
fun TransactionType.stringResource(): Int =
    when (this) {
        TransactionType.INCOME -> R.string.transaction_type_income
        TransactionType.EXPENSE -> R.string.transaction_type_expense
    }
