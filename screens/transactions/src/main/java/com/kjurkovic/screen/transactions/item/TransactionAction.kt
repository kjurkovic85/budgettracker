package com.kjurkovic.screen.transactions.item

import androidx.annotation.StringRes
import com.kjurkovic.screen.transactions.R

enum class TransactionAction(
    @StringRes val title: Int,
    @StringRes val description: Int,
    @StringRes val confirmButton: Int,
    @StringRes val cancelButton: Int? = null,
    val actionIntent: TransactionIntent,
) {
    DELETE(
        title = R.string.transaction_confirm_delete_title,
        description = R.string.transaction_confirm_delete_description,
        confirmButton = R.string.txt_ok,
        cancelButton = R.string.txt_cancel,
        actionIntent = TransactionIntent.ConfirmDelete,
    ),
    ERROR(
        title = R.string.transaction_error_title,
        description = R.string.transaction_error_description,
        confirmButton = R.string.txt_ok,
        actionIntent = TransactionIntent.DismissDialog,
    ),
    SUCCESS(
        title = R.string.transaction_success_title,
        description = R.string.transaction_success_description,
        confirmButton = R.string.txt_ok,
        actionIntent = TransactionIntent.BackClicked,
    )
}