package com.kjurkovic.screen.transactions.item

import android.content.Context
import android.widget.DatePicker
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.key
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.imePadding
import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.data.common.TransactionType
import com.kjurkovic.resources.formatters.stringify
import com.kjurkovic.resources.formatters.toPrice
import com.kjurkovic.resources.theme.allColors
import com.kjurkovic.resources.theme.icons
import com.kjurkovic.screen.transactions.R
import com.kjurkovic.ui.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import timber.log.Timber
import java.time.LocalDate

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun TransactionScreen(
    viewModel: TransactionViewModel,
) = viewModel.Screen { viewState, intentChannel, sideEffects ->
    val sheetState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)

    TransactionScreen(viewState = viewState, intentChannel = intentChannel, sheetState = sheetState)
    TransactionDialog(viewState = viewState, intentChannel = intentChannel)
    TransactionSideEffects(sideEffects = sideEffects, sheetState = sheetState)
}

@Composable
private fun TransactionDialog(
    viewState: TransactionViewState,
    intentChannel: MutableSharedFlow<TransactionIntent>,
) {
    if (viewState.dialogType != null) {
        Alert(
            title = stringResource(viewState.dialogType.title),
            text = stringResource(viewState.dialogType.description),
            confirmText = stringResource(viewState.dialogType.confirmButton),
            cancelText = viewState.dialogType.cancelButton?.let { stringResource(it) },
            onConfirm = { intentChannel.tryEmit(viewState.dialogType.actionIntent) },
            onCancel = viewState.dialogType.cancelButton?.let {
                { intentChannel.tryEmit(TransactionIntent.DismissDialog) }
            },
            onDismiss = { intentChannel.tryEmit(TransactionIntent.DismissDialog) }
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun TransactionSideEffects(
    sideEffects: SharedFlow<TransactionSideEffect>,
    sheetState: ModalBottomSheetState,
) {
    LaunchedEffect(sideEffects) {
        sideEffects.collect { sideEffect ->
            when (sideEffect) {
                TransactionSideEffect.CloseBottomSheet -> {
                    try {
                        sheetState.hide()
                    } catch (ex: Throwable) {
                        Timber.e(ex)
                    }
                }
                TransactionSideEffect.OpenBottomSheet -> {
                    try {
                        sheetState.animateTo(ModalBottomSheetValue.HalfExpanded)
                    } catch (ex: Throwable) {
                        Timber.e(ex)
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun TransactionScreen(
    viewState: TransactionViewState,
    intentChannel: MutableSharedFlow<TransactionIntent>,
    sheetState: ModalBottomSheetState,
) {
    ScreenComponent(
        navigationBarColor = MaterialTheme.allColors.background,
        topBarContent = {
            ScreenToolbar(
                title = viewState.screenTitle ?: stringResource(R.string.transaction_add_new),
                onBackClicked = { intentChannel.tryEmit(TransactionIntent.BackClicked) },
                rightContent = {
                    if (viewState.isEditMode) {
                        IconButton(onClick = { intentChannel.tryEmit(TransactionIntent.DeleteClicked) }) {
                            Icon(
                                imageVector = MaterialTheme.icons.Delete,
                                contentDescription = ""
                            )
                        }
                    }
                }
            )
        }
    ) { innerPadding ->
        ModalBottomSheetLayout(
            modifier = Modifier
                .fillMaxSize()
                .padding(
                    top = innerPadding.calculateTopPadding(),
                    bottom = innerPadding.calculateBottomPadding(),
                ),
            sheetState = sheetState,
            sheetContent = {
                CategorySheet(viewState.categories) {
                    intentChannel.tryEmit(TransactionIntent.CategoryChanged(it.id!!, it.name))
                }
            },
            scrimColor = MaterialTheme.allColors.scrimColor,
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .imePadding()
                    .verticalScroll(rememberScrollState())
                    .padding(
                        top = innerPadding.calculateTopPadding(),
                        start = dimensionResource(R.dimen.size_16),
                        end = dimensionResource(R.dimen.size_16),
                    )
            ) {
                val interactionSource = remember { MutableInteractionSource() }
                val context = LocalContext.current

                TextInput(
                    value = viewState.description,
                    placeholder = stringResource(R.string.transaction_description_label),
                    onValueChange = { intentChannel.tryEmit(TransactionIntent.DescriptionChanged(it)) },
                    modifier = Modifier
                        .padding(top = dimensionResource(R.dimen.size_24))
                        .fillMaxWidth()
                )
                TextInput(
                    value = viewState.amount,
                    placeholder = stringResource(R.string.transaction_amount_label),
                    onValueChange = { intentChannel.tryEmit(TransactionIntent.AmountChanged(it)) },
                    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                    modifier = Modifier
                        .padding(top = dimensionResource(R.dimen.size_16))
                        .fillMaxWidth()
                )
                TextInput(
                    value = viewState.categoryName ?: "",
                    placeholder = stringResource(R.string.transaction_category_label),
                    enabled = false,
                    modifier = Modifier
                        .padding(top = dimensionResource(R.dimen.size_16))
                        .fillMaxWidth()
                        .clickable { intentChannel.tryEmit(TransactionIntent.CategoryPickerRequested) }
                )
                TextInput(
                    value = viewState.date.stringify(),
                    placeholder = stringResource(R.string.transaction_date_label),
                    enabled = false,
                    modifier = Modifier
                        .padding(top = dimensionResource(R.dimen.size_16))
                        .fillMaxWidth()
                        .clickable(interactionSource = interactionSource, indication = null) {
                            showDatePicker(viewState.date, context) { date ->
                                intentChannel.tryEmit(TransactionIntent.DateChanged(date))
                            }
                        }
                )

                viewState.transactionTypes.forEach { type ->
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.clickable { intentChannel.tryEmit(TransactionIntent.TransactionTypeChanged(type)) }
                    ) {
                        RadioButton(
                            selected = viewState.type == type,
                            onClick = { intentChannel.tryEmit(TransactionIntent.TransactionTypeChanged(type)) },
                            enabled = true,
                            colors = RadioButtonDefaults.colors(selectedColor = MaterialTheme.allColors.primary)
                        )
                        Text(
                            text = stringResource(type.stringResource()),
                            modifier = Modifier.padding(start = dimensionResource(R.dimen.size_8))
                        )
                    }

                }
                Spacer(modifier = Modifier.weight(1f))
                PrimaryButton(
                    text = stringResource(R.string.transaction_save),
                    enabled = viewState.isFormValid(),
                    onClick = { intentChannel.tryEmit(TransactionIntent.SaveClicked) },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = dimensionResource(R.dimen.size_16))
                )
            }
        }
    }
}

@Composable
fun CategorySheet(
    categories: List<CategoryModel>,
    onCategorySelected: (category: CategoryModel) -> Unit
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        items(categories) {
            key(it.id!!) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable { onCategorySelected(it) }
                ) {
                    Body(
                        text = it.name,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                horizontal = dimensionResource(R.dimen.size_16),
                                vertical = dimensionResource(R.dimen.size_16)
                            )
                    )
                    Divider(
                        startIndent = dimensionResource(R.dimen.size_16)
                    )
                }
            }
        }
    }
}

private fun showDatePicker(selectedDate: LocalDate?, context: Context, onDatePicked: (date: LocalDate) -> Unit) {
    AppDatePicker(context, object : AppDatePicker.AppOnDateSetListener {
        override fun onDateSet(view: DatePicker, date: LocalDate) {
            onDatePicked(date)
        }
    }, selectedDate ?: LocalDate.now()).show()
}