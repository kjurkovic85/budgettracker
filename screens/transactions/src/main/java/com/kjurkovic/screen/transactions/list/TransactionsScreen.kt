package com.kjurkovic.screen.transactions.list

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.key
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.kjurkovic.data.common.TransactionType
import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import com.kjurkovic.resources.formatters.stringify
import com.kjurkovic.resources.formatters.toPrice
import com.kjurkovic.resources.theme.allColors
import com.kjurkovic.resources.theme.icons
import com.kjurkovic.screen.transactions.R
import com.kjurkovic.ui.*
import kotlinx.coroutines.flow.MutableSharedFlow
import java.time.LocalDate

@Composable
fun TransactionsScreen(
    viewModel: TransactionsViewModel,
) = viewModel.Screen { viewState, intentChannel, _ ->
    TransactionsScreen(
        viewState = viewState,
        intentChannel = intentChannel
    )
}

@Composable
private fun TransactionsScreen(
    viewState: TransactionsViewState,
    intentChannel: MutableSharedFlow<TransactionsIntent>,
) {
    ScreenComponent(
        topBarContent = {
            ScreenToolbar(
                title = stringResource(R.string.transactions),
                rightContent = {
                    IconButton(onClick = { intentChannel.tryEmit(TransactionsIntent.NewTransactionClicked) }) {
                        Icon(
                            imageVector = MaterialTheme.icons.Add,
                            contentDescription = ""
                        )
                    }
                }
            )
        }
    ) {
        if (viewState.transactions.isEmpty()) {
            EmptyScreen(
                modifier = Modifier.padding(it),
                text = R.string.transactions_empty
            )
        } else {
            TransactionList(
                viewState = viewState,
                intentChannel = intentChannel,
                modifier = Modifier.padding(it),
            )
        }
    }
}


@Composable
private fun TransactionList(
    viewState: TransactionsViewState,
    intentChannel: MutableSharedFlow<TransactionsIntent>,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier = modifier.fillMaxSize()
    ) {
        items(viewState.transactions.size) { index ->
            if (viewState.transactions.size - 1 == index) {
                intentChannel.tryEmit(TransactionsIntent.NextPageRequested)
            }

            val transaction = viewState.transactions[index]

            if (index == 0 || transaction.date != viewState.transactions[index - 1].date) {
                key(transaction.date) {
                    HeaderRow(date = transaction.date)
                }
            }

            key(transaction.id) {
                TransactionRow(
                    transaction = transaction,
                    onClick = { intentChannel.tryEmit(TransactionsIntent.TransactionClicked(transaction)) }
                )
            }
        }
    }
}

@Composable
private fun HeaderRow(date: LocalDate) {
    Row(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.allColors.primaryVariant),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Body(
            text = date.stringify(),
            color = MaterialTheme.allColors.onPrimary,
            modifier = Modifier.padding(
                vertical = dimensionResource(R.dimen.size_8),
                horizontal = dimensionResource(R.dimen.size_16),
            )
        )
    }
}

@Composable
private fun TransactionRow(
    transaction: TransactionModel,
    onClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = dimensionResource(R.dimen.size_16),
                    end = dimensionResource(R.dimen.size_16),
                    top = dimensionResource(R.dimen.size_12),
                ),
        ) {
            Body(
                text = transaction.description,
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.Medium)
            )
            Body(
                text = transaction.amount.toPrice(),
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.Bold),
                color = if (transaction.type == TransactionType.INCOME)
                    MaterialTheme.allColors.positiveAmount
                else
                    MaterialTheme.allColors.negativeAmount
            )
        }

        Caption(
            text = transaction.category?.name ?: stringResource(R.string.transaction_category_unknown),
            modifier = Modifier.padding(
                top = dimensionResource(R.dimen.size_4),
                start = dimensionResource(R.dimen.size_16),
                end = dimensionResource(R.dimen.size_16),
            ),
        )


        Divider(
            startIndent = dimensionResource(R.dimen.size_16),
            modifier = Modifier.padding(top = dimensionResource(R.dimen.size_12))
        )
    }
}
