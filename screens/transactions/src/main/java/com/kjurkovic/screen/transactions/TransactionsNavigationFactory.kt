package com.kjurkovic.screen.transactions

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.HiltNavigationFactory
import com.kjurkovic.common.framework.navigation.NavigationFactory
import com.kjurkovic.screen.transactions.item.TransactionScreen
import com.kjurkovic.screen.transactions.item.TransactionViewModel
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.multibindings.IntoSet
import java.util.*
import javax.inject.Inject

@ActivityRetainedScoped
@HiltNavigationFactory
class TransactionsNavigationFactory @Inject constructor(
    private val dashboardViewModelFactory: TransactionViewModel.Factory,
) : NavigationFactory {

    override fun create(builder: NavGraphBuilder, navController: NavHostController) {
        builder.addTransactionRoute()
        builder.editTransactionRoute()
    }

    private fun NavGraphBuilder.addTransactionRoute() {
        composable(route = Destination.Transactions.Add.fullRoute) {
            val viewModel = TransactionViewModel.create(
                assistedFactory = dashboardViewModelFactory,
            )
            TransactionScreen(viewModel)
        }
    }

    private fun NavGraphBuilder.editTransactionRoute() {
        composable(route = Destination.Transactions.Edit.fullRoute) {
            val transactionIdString = it.arguments!!.getString(Destination.Transactions.Edit.TRANSACTION_ID)
            val transactionId = UUID.fromString(transactionIdString)
            val viewModel = TransactionViewModel.create(
                assistedFactory = dashboardViewModelFactory,
                transactionId = transactionId
            )
            TransactionScreen(viewModel)
        }
    }
}

@Module
@InstallIn(ActivityRetainedComponent::class)
internal interface ComposeNavigationFactoryModule {
    @Binds
    @IntoSet
    fun bindNavigationFactory(factory: TransactionsNavigationFactory): NavigationFactory
}
