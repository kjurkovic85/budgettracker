package com.kjurkovic.screen.transactions.list

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import com.kjurkovic.repo.transaction_repo.TransactionsRepository
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class TransactionsViewModel @AssistedInject constructor(
    private val transactionsRepository: TransactionsRepository,
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
) : ComponentViewModel<TransactionsViewState, TransactionsIntent, Void>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = TransactionsViewState()
) {

    @AssistedFactory
    interface Factory {
        fun create(): TransactionsViewModel
    }

    companion object {
        private const val PAGE_SIZE = 30

        @Composable
        fun create(
            assistedFactory: Factory,
        ): TransactionsViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        })
    }

    private var currentPage = 1
    private var loading = false

    init {
        launchMain {
            val transactions = transactionsRepository.transactions(PAGE_SIZE, currentPage)
            setState { copy(transactions = transactions) }
        }
    }

    override suspend fun reduce(intent: TransactionsIntent) {
        when (intent) {
            TransactionsIntent.NewTransactionClicked -> {
                navigator.navigateTo(Destination.Transactions.Add.fullRoute)
            }
            TransactionsIntent.NextPageRequested -> launchIo {
                if (!loading) {
                    loading = true
                    val newTransactions =
                        transactionsRepository.transactions(PAGE_SIZE, ++currentPage)
                    setState { copy(transactions = transactions + newTransactions) }
                    loading = false
                }
            }
            is TransactionsIntent.TransactionClicked -> navigator.navigateTo(Destination.Transactions.Edit(intent.transaction.id!!.toString()))
        }
    }
}
