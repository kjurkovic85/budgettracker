package com.kjurkovic.screen.transactions.item

import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.data.common.TransactionType
import java.lang.Exception
import java.math.BigDecimal
import java.time.LocalDate

data class TransactionViewState(
    val screenTitle: String? = null,
    val categories: List<CategoryModel> = emptyList(),
    val description: String = "",
    val amount: String = "",
    val date: LocalDate = LocalDate.now(),
    val type: TransactionType = TransactionType.EXPENSE,
    val categoryId: Long? = null,
    val categoryName: String? = null,
    val isEditMode: Boolean = false,
    val dialogType: TransactionAction? = null,
    val transactionTypes: List<TransactionType> = TransactionType.values().toList(),
) {
    fun isFormValid() = try {
        description.isNotBlank() && BigDecimal(amount) > BigDecimal.ZERO
    } catch (e: Exception) {
        false
    }
}

sealed class TransactionIntent {
    data class DescriptionChanged(val description: String) : TransactionIntent()
    data class AmountChanged(val amount: String) : TransactionIntent()
    data class DateChanged(val date: LocalDate) : TransactionIntent()
    data class CategoryChanged(val categoryId: Long, val categoryName: String) : TransactionIntent()
    data class TransactionTypeChanged(val type: TransactionType) : TransactionIntent()
    object CategoryPickerRequested : TransactionIntent()
    object SaveClicked : TransactionIntent()
    object BackClicked : TransactionIntent()
    object DeleteClicked : TransactionIntent()
    object ConfirmDelete : TransactionIntent()
    object DismissDialog : TransactionIntent()
}

sealed class TransactionSideEffect {
    object OpenBottomSheet : TransactionSideEffect()
    object CloseBottomSheet : TransactionSideEffect()
}
