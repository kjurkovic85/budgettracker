package com.kjurkovic.screen.transactions.item

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.category_repo.CategoriesRepository
import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import com.kjurkovic.repo.transaction_repo.TransactionsRepository
import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*

class TransactionViewModel @AssistedInject constructor(
    @Assisted("transactionId") private val transactionId: UUID?,
    private val transactionsRepository: TransactionsRepository,
    private val categoriesRepository: CategoriesRepository,
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
) : ComponentViewModel<TransactionViewState, TransactionIntent, TransactionSideEffect>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = TransactionViewState()
) {

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("transactionId") transactionId: UUID?,
        ): TransactionViewModel
    }

    companion object {
        @Composable
        fun create(
            assistedFactory: Factory,
            transactionId: UUID? = null,
        ): TransactionViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create(
                    transactionId = transactionId,
                ) as T
            }
        })
    }

    init {
        launchMain {
            launch {
                transactionId?.let {
                    transactionsRepository.transaction(it).collect { transaction ->
                        setState {
                            copy(
                                screenTitle = transaction.description,
                                description = transaction.description,
                                date = transaction.date,
                                amount = transaction.amount.toPlainString(),
                                categoryId = transaction.category?.id,
                                categoryName = transaction.category?.name,
                                isEditMode = true,
                            )
                        }
                    }
                }
            }
            launch {
                categoriesRepository.categories().collect { items ->
                    setState { copy(categories = items) }
                }
            }
        }
    }

    override suspend fun reduce(intent: TransactionIntent) {
        when (intent) {
            TransactionIntent.SaveClicked -> {
                runCatching { saveTransaction() }
                    .onSuccess { setState { copy(dialogType = TransactionAction.SUCCESS) } }
                    .onFailure { setState { copy(dialogType = TransactionAction.ERROR) } }
            }
            TransactionIntent.BackClicked -> navigator.navigateBack()
            TransactionIntent.DeleteClicked -> setState { copy(dialogType = TransactionAction.DELETE) }
            TransactionIntent.ConfirmDelete -> {
                transactionsRepository.delete(transactionId!!)
                navigator.navigateBack()
            }
            TransactionIntent.DismissDialog -> setState { copy(dialogType = null) }
            TransactionIntent.CategoryPickerRequested -> _sideEffects.tryEmit(TransactionSideEffect.OpenBottomSheet)
            is TransactionIntent.AmountChanged -> runCatching {
                setState { copy(amount = intent.amount) }
            }
            is TransactionIntent.CategoryChanged -> {
                setState {
                    copy(
                        categoryId = intent.categoryId,
                        categoryName = intent.categoryName
                    )

                }
                _sideEffects.tryEmit(TransactionSideEffect.CloseBottomSheet)
            }
            is TransactionIntent.DateChanged -> setState { copy(date = intent.date) }
            is TransactionIntent.DescriptionChanged -> setState { copy(description = intent.description) }
            is TransactionIntent.TransactionTypeChanged -> setState { copy(type = intent.type) }
        }
    }

    private suspend fun saveTransaction() {
        if (viewState.value.isFormValid()) {
            val category = if (viewState.value.categoryId != null) {
                CategoryModel(viewState.value.categoryId, viewState.value.categoryName!!)
            } else null
            transactionsRepository.save(
                TransactionModel(
                    id = transactionId,
                    description = viewState.value.description,
                    amount = BigDecimal(viewState.value.amount),
                    date = viewState.value.date,
                    category = category,
                    type = viewState.value.type,
                )
            )
        }
    }
}
