package com.kjurkovic.screen.transactions.list

import com.kjurkovic.repo.transaction_repo.model.TransactionModel

data class TransactionsViewState(
    val transactions: List<TransactionModel> = emptyList()
)

sealed class TransactionsIntent {
    data class TransactionClicked(val transaction: TransactionModel) : TransactionsIntent()
    object NewTransactionClicked : TransactionsIntent()
    object NextPageRequested : TransactionsIntent()
}
