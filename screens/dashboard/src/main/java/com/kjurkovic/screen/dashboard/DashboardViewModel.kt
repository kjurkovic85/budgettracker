package com.kjurkovic.screen.dashboard

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import com.kjurkovic.screen.home.HomeViewModel
import com.kjurkovic.screen.transactions.list.TransactionsViewModel
import com.kjurkovic.screens.categories.list.CategoriesViewModel
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class DashboardViewModel @AssistedInject constructor(
    internal val homeViewModelFactory: HomeViewModel.Factory,
    internal val categoryViewModelFactory: CategoriesViewModel.Factory,
    internal val transactionsViewModelFactory: TransactionsViewModel.Factory,
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
) : ComponentViewModel<DashboardViewState, DashboardIntent, Void>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = DashboardViewState(
        selectedTab = DashboardTab.HOME
    )
) {

    @AssistedFactory
    interface Factory {
        fun create(): DashboardViewModel
    }

    companion object {
        @Composable
        fun create(
            assistedFactory: Factory,
        ): DashboardViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        })
    }

    override suspend fun reduce(intent: DashboardIntent) {
        when (intent) {
            is DashboardIntent.TabSelected -> setState { copy(selectedTab = intent.dashboardTab) }
            DashboardIntent.CloseApp -> navigator.closeApp()
        }
    }
}