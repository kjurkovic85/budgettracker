package com.kjurkovic.screen.dashboard

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.HiltNavigationFactory
import com.kjurkovic.common.framework.navigation.NavigationFactory
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.multibindings.IntoSet
import javax.inject.Inject

@ActivityRetainedScoped
@HiltNavigationFactory
class DashboardNavigationFactory @Inject constructor(
    private val dashboardViewModelFactory: DashboardViewModel.Factory,
) : NavigationFactory {

    override fun create(builder: NavGraphBuilder, navController: NavHostController) {
        builder.composable(route = Destination.Dashboard.fullRoute) {
            val viewModel = DashboardViewModel.create(
                assistedFactory = dashboardViewModelFactory,
            )
            DashboardScreen(viewModel)
        }
    }
}

@Module
@InstallIn(ActivityRetainedComponent::class)
internal interface ComposeNavigationFactoryModule {
    @Binds
    @IntoSet
    fun bindNavigationFactory(factory: DashboardNavigationFactory): NavigationFactory
}
