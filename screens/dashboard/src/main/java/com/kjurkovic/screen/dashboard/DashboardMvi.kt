package com.kjurkovic.screen.dashboard

data class DashboardViewState(
    val selectedTab: DashboardTab,
)

sealed class DashboardIntent {
    data class TabSelected(val dashboardTab: DashboardTab) : DashboardIntent()
    object CloseApp : DashboardIntent()
}