package com.kjurkovic.screen.dashboard


import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

enum class DashboardTab(
    val route: String,
    @StringRes val labelRes: Int,
    @DrawableRes val iconRes: Int,
) {
    HOME("home", R.string.dashboard_tab_home, R.drawable.ic_dashboard),
    TRANSACTIONS("portfolio", R.string.dashboard_tab_transactions, R.drawable.ic_transactions),
    CATEGORIES("transfers", R.string.dashboard_tab_categories, R.drawable.ic_categories);
}
