package com.kjurkovic.screen.dashboard

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues
import com.kjurkovic.resources.theme.allColors
import com.kjurkovic.screen.home.HomeScreen
import com.kjurkovic.screen.home.HomeViewModel
import com.kjurkovic.screen.transactions.list.TransactionsScreen
import com.kjurkovic.screen.transactions.list.TransactionsViewModel
import com.kjurkovic.screens.categories.list.CategoriesScreen
import com.kjurkovic.screens.categories.list.CategoriesViewModel
import com.kjurkovic.ui.Caption
import com.kjurkovic.ui.ScreenComponent
import kotlinx.coroutines.flow.MutableSharedFlow

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DashboardScreen(
    viewModel: DashboardViewModel,
) = viewModel.Screen { viewState, intentChannel, _ ->

    val homeViewModel = viewModel.homeViewModelFactory.create()
    val categoryViewModel = viewModel.categoryViewModelFactory.create()
    val transactionsViewModel = viewModel.transactionsViewModelFactory.create()

    DashboardScreen(
        viewState = viewState,
        intentChannel = intentChannel,
        homeViewModel = homeViewModel,
        categoryViewModel = categoryViewModel,
        transactionsViewModel = transactionsViewModel,
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun DashboardScreen(
    viewState: DashboardViewState,
    intentChannel: MutableSharedFlow<DashboardIntent>,
    homeViewModel: HomeViewModel,
    categoryViewModel: CategoriesViewModel,
    transactionsViewModel: TransactionsViewModel,
) {
    BackHandler {
        if (viewState.selectedTab != DashboardTab.HOME) {
            intentChannel.tryEmit(DashboardIntent.TabSelected(DashboardTab.HOME))
        } else {
            intentChannel.tryEmit(DashboardIntent.CloseApp)
        }
    }

    ScreenComponent(
        addStatusBarPadding = false,
        addNavigationBarPadding = false,
        navigationBarDarkIcons = true,
        bottomBarContent = {
            DashboardBottomBar(
                viewState = viewState,
                intentChannel = intentChannel,
            )
        },
    ) { innerPadding ->
        Box(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
        ) {
            when (viewState.selectedTab) {
                DashboardTab.HOME -> HomeScreen(viewModel = homeViewModel)
                DashboardTab.TRANSACTIONS -> TransactionsScreen(viewModel = transactionsViewModel)
                DashboardTab.CATEGORIES -> CategoriesScreen(viewModel = categoryViewModel)
            }
        }
    }
}

@Composable
private fun DashboardBottomBar(
    viewState: DashboardViewState,
    intentChannel: MutableSharedFlow<DashboardIntent>,
) {
    val navigationBarPadding = rememberInsetsPaddingValues(
        insets = LocalWindowInsets.current.navigationBars,
        applyBottom = true,
    )
    val navigationBarInset = dimensionResource(R.dimen.size_16)
    val bottomPadding = remember(navigationBarPadding) {
        navigationBarPadding.calculateBottomPadding()
            .let { if (it > 0.dp) it - navigationBarInset else 0.dp }
    }

    BottomNavigation(
        modifier = Modifier.height(88.dp + bottomPadding),
        backgroundColor = MaterialTheme.allColors.surface,
        elevation = dimensionResource(R.dimen.elevation_low),
    ) {
        enumValues<DashboardTab>().forEach { dashboardTab ->
            BottomNavigationItem(
                icon = {
                    BottomNavigationIcon(
                        dashboardTab = dashboardTab,
                        selectedTab = viewState.selectedTab,
                    )
                },
                selected = dashboardTab == viewState.selectedTab,
                onClick = { intentChannel.tryEmit(DashboardIntent.TabSelected(dashboardTab)) },
                modifier = Modifier.padding(bottom = bottomPadding),
            )
        }
    }
}

@Composable
private fun BottomNavigationIcon(
    dashboardTab: DashboardTab,
    selectedTab: DashboardTab,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        val iconTint = androidx.compose.ui.graphics.ColorFilter.Companion.tint(
            if (dashboardTab == selectedTab) MaterialTheme.allColors.primary
            else MaterialTheme.allColors.placeholder
        )

        Image(
            painter = painterResource(dashboardTab.iconRes),
            contentDescription = null,
            colorFilter = iconTint,
        )
        Spacer(modifier = Modifier.height(dimensionResource(R.dimen.size_8)))
        Caption(
            text = stringResource(dashboardTab.labelRes),
            color = if (dashboardTab == selectedTab) MaterialTheme.colors.primaryVariant else MaterialTheme.colors.onSurface
        )
    }
}
