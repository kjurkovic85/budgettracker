package com.kjurkovic.screen.dashboard;

import dagger.internal.DaggerGenerated;
import dagger.internal.InstanceFactory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DashboardViewModel_Factory_Impl implements DashboardViewModel.Factory {
  private final DashboardViewModel_Factory delegateFactory;

  DashboardViewModel_Factory_Impl(DashboardViewModel_Factory delegateFactory) {
    this.delegateFactory = delegateFactory;
  }

  @Override
  public DashboardViewModel create() {
    return delegateFactory.get();
  }

  public static Provider<DashboardViewModel.Factory> create(
      DashboardViewModel_Factory delegateFactory) {
    return InstanceFactory.create(new DashboardViewModel_Factory_Impl(delegateFactory));
  }
}
