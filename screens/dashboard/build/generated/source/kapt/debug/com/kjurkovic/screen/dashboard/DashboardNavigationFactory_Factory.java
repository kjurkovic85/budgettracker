package com.kjurkovic.screen.dashboard;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DashboardNavigationFactory_Factory implements Factory<DashboardNavigationFactory> {
  private final Provider<DashboardViewModel.Factory> dashboardViewModelFactoryProvider;

  public DashboardNavigationFactory_Factory(
      Provider<DashboardViewModel.Factory> dashboardViewModelFactoryProvider) {
    this.dashboardViewModelFactoryProvider = dashboardViewModelFactoryProvider;
  }

  @Override
  public DashboardNavigationFactory get() {
    return newInstance(dashboardViewModelFactoryProvider.get());
  }

  public static DashboardNavigationFactory_Factory create(
      Provider<DashboardViewModel.Factory> dashboardViewModelFactoryProvider) {
    return new DashboardNavigationFactory_Factory(dashboardViewModelFactoryProvider);
  }

  public static DashboardNavigationFactory newInstance(
      DashboardViewModel.Factory dashboardViewModelFactory) {
    return new DashboardNavigationFactory(dashboardViewModelFactory);
  }
}
