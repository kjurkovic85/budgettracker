package com.kjurkovic.screen.dashboard;

import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.codegen.OriginatingElement;
import javax.annotation.processing.Generated;

@OriginatingElement(
    topLevelClass = ComposeNavigationFactoryModule.class
)
@InstallIn(ActivityRetainedComponent.class)
@Module(
    includes = ComposeNavigationFactoryModule.class
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.PkgPrivateModuleGenerator")
public final class HiltWrapper_ComposeNavigationFactoryModule {
}
