package com.kjurkovic.screen.dashboard;

import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.common.framework.navigation.ScreenNavigator;
import com.kjurkovic.screen.home.HomeViewModel;
import com.kjurkovic.screen.transactions.list.TransactionsViewModel;
import com.kjurkovic.screens.categories.list.CategoriesViewModel;
import dagger.internal.DaggerGenerated;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DashboardViewModel_Factory {
  private final Provider<HomeViewModel.Factory> homeViewModelFactoryProvider;

  private final Provider<CategoriesViewModel.Factory> categoryViewModelFactoryProvider;

  private final Provider<TransactionsViewModel.Factory> transactionsViewModelFactoryProvider;

  private final Provider<ScreenNavigator> navigatorProvider;

  private final Provider<DispatcherProvider> dispatcherProvider;

  public DashboardViewModel_Factory(Provider<HomeViewModel.Factory> homeViewModelFactoryProvider,
      Provider<CategoriesViewModel.Factory> categoryViewModelFactoryProvider,
      Provider<TransactionsViewModel.Factory> transactionsViewModelFactoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    this.homeViewModelFactoryProvider = homeViewModelFactoryProvider;
    this.categoryViewModelFactoryProvider = categoryViewModelFactoryProvider;
    this.transactionsViewModelFactoryProvider = transactionsViewModelFactoryProvider;
    this.navigatorProvider = navigatorProvider;
    this.dispatcherProvider = dispatcherProvider;
  }

  public DashboardViewModel get() {
    return newInstance(homeViewModelFactoryProvider.get(), categoryViewModelFactoryProvider.get(), transactionsViewModelFactoryProvider.get(), navigatorProvider.get(), dispatcherProvider.get());
  }

  public static DashboardViewModel_Factory create(
      Provider<HomeViewModel.Factory> homeViewModelFactoryProvider,
      Provider<CategoriesViewModel.Factory> categoryViewModelFactoryProvider,
      Provider<TransactionsViewModel.Factory> transactionsViewModelFactoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    return new DashboardViewModel_Factory(homeViewModelFactoryProvider, categoryViewModelFactoryProvider, transactionsViewModelFactoryProvider, navigatorProvider, dispatcherProvider);
  }

  public static DashboardViewModel newInstance(HomeViewModel.Factory homeViewModelFactory,
      CategoriesViewModel.Factory categoryViewModelFactory,
      TransactionsViewModel.Factory transactionsViewModelFactory, ScreenNavigator navigator,
      DispatcherProvider dispatcherProvider) {
    return new DashboardViewModel(homeViewModelFactory, categoryViewModelFactory, transactionsViewModelFactory, navigator, dispatcherProvider);
  }
}
