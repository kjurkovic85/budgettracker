package com.kjurkovic.screen.dashboard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 \u001a2\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u001a\u001bB/\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0003H\u0094@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019R\u0014\u0010\u0007\u001a\u00020\bX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\u00020\u0006X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\t\u001a\u00020\nX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001c"}, d2 = {"Lcom/kjurkovic/screen/dashboard/DashboardViewModel;", "Lcom/kjurkovic/common/framework/mvi/ComponentViewModel;", "Lcom/kjurkovic/screen/dashboard/DashboardViewState;", "Lcom/kjurkovic/screen/dashboard/DashboardIntent;", "Ljava/lang/Void;", "homeViewModelFactory", "Lcom/kjurkovic/screen/home/HomeViewModel$Factory;", "categoryViewModelFactory", "Lcom/kjurkovic/screens/categories/list/CategoriesViewModel$Factory;", "transactionsViewModelFactory", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel$Factory;", "navigator", "Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "(Lcom/kjurkovic/screen/home/HomeViewModel$Factory;Lcom/kjurkovic/screens/categories/list/CategoriesViewModel$Factory;Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel$Factory;Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;Lcom/kjurkovic/common/dispatchers/DispatcherProvider;)V", "getCategoryViewModelFactory$dashboard_debug", "()Lcom/kjurkovic/screens/categories/list/CategoriesViewModel$Factory;", "getHomeViewModelFactory$dashboard_debug", "()Lcom/kjurkovic/screen/home/HomeViewModel$Factory;", "getTransactionsViewModelFactory$dashboard_debug", "()Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel$Factory;", "reduce", "", "intent", "(Lcom/kjurkovic/screen/dashboard/DashboardIntent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "Factory", "dashboard_debug"})
public final class DashboardViewModel extends com.kjurkovic.common.framework.mvi.ComponentViewModel<com.kjurkovic.screen.dashboard.DashboardViewState, com.kjurkovic.screen.dashboard.DashboardIntent, java.lang.Void> {
    @org.jetbrains.annotations.NotNull()
    private final com.kjurkovic.screen.home.HomeViewModel.Factory homeViewModelFactory = null;
    @org.jetbrains.annotations.NotNull()
    private final com.kjurkovic.screens.categories.list.CategoriesViewModel.Factory categoryViewModelFactory = null;
    @org.jetbrains.annotations.NotNull()
    private final com.kjurkovic.screen.transactions.list.TransactionsViewModel.Factory transactionsViewModelFactory = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.screen.dashboard.DashboardViewModel.Companion Companion = null;
    
    @dagger.assisted.AssistedInject()
    public DashboardViewModel(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.home.HomeViewModel.Factory homeViewModelFactory, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.screens.categories.list.CategoriesViewModel.Factory categoryViewModelFactory, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.transactions.list.TransactionsViewModel.Factory transactionsViewModelFactory, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.framework.navigation.ScreenNavigator navigator, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.screen.home.HomeViewModel.Factory getHomeViewModelFactory$dashboard_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.screens.categories.list.CategoriesViewModel.Factory getCategoryViewModelFactory$dashboard_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.screen.transactions.list.TransactionsViewModel.Factory getTransactionsViewModelFactory$dashboard_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Object reduce(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.dashboard.DashboardIntent intent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/kjurkovic/screen/dashboard/DashboardViewModel$Factory;", "", "create", "Lcom/kjurkovic/screen/dashboard/DashboardViewModel;", "dashboard_debug"})
    @dagger.assisted.AssistedFactory()
    public static abstract interface Factory {
        
        @org.jetbrains.annotations.NotNull()
        public abstract com.kjurkovic.screen.dashboard.DashboardViewModel create();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/kjurkovic/screen/dashboard/DashboardViewModel$Companion;", "", "()V", "create", "Lcom/kjurkovic/screen/dashboard/DashboardViewModel;", "assistedFactory", "Lcom/kjurkovic/screen/dashboard/DashboardViewModel$Factory;", "dashboard_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @androidx.compose.runtime.Composable()
        public final com.kjurkovic.screen.dashboard.DashboardViewModel create(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.screen.dashboard.DashboardViewModel.Factory assistedFactory) {
            return null;
        }
    }
}