package com.kjurkovic.screen.dashboard;

import java.lang.System;

@com.kjurkovic.common.framework.navigation.HiltNavigationFactory()
@dagger.hilt.android.scopes.ActivityRetainedScoped()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/kjurkovic/screen/dashboard/DashboardNavigationFactory;", "Lcom/kjurkovic/common/framework/navigation/NavigationFactory;", "dashboardViewModelFactory", "Lcom/kjurkovic/screen/dashboard/DashboardViewModel$Factory;", "(Lcom/kjurkovic/screen/dashboard/DashboardViewModel$Factory;)V", "create", "", "builder", "Landroidx/navigation/NavGraphBuilder;", "navController", "Landroidx/navigation/NavHostController;", "dashboard_debug"})
public final class DashboardNavigationFactory implements com.kjurkovic.common.framework.navigation.NavigationFactory {
    private final com.kjurkovic.screen.dashboard.DashboardViewModel.Factory dashboardViewModelFactory = null;
    
    @javax.inject.Inject()
    public DashboardNavigationFactory(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.dashboard.DashboardViewModel.Factory dashboardViewModelFactory) {
        super();
    }
    
    @java.lang.Override()
    public void create(@org.jetbrains.annotations.NotNull()
    androidx.navigation.NavGraphBuilder builder, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavHostController navController) {
    }
}