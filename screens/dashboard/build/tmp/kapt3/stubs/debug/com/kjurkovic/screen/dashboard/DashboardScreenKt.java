package com.kjurkovic.screen.dashboard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000:\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\u0003\u001a\u001e\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0003\u001a\u0010\u0010\u000b\u001a\u00020\u00012\u0006\u0010\f\u001a\u00020\rH\u0007\u001a6\u0010\u000b\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0003\u00a8\u0006\u0014"}, d2 = {"BottomNavigationIcon", "", "dashboardTab", "Lcom/kjurkovic/screen/dashboard/DashboardTab;", "selectedTab", "DashboardBottomBar", "viewState", "Lcom/kjurkovic/screen/dashboard/DashboardViewState;", "intentChannel", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/kjurkovic/screen/dashboard/DashboardIntent;", "DashboardScreen", "viewModel", "Lcom/kjurkovic/screen/dashboard/DashboardViewModel;", "homeViewModel", "Lcom/kjurkovic/screen/home/HomeViewModel;", "categoryViewModel", "Lcom/kjurkovic/screens/categories/list/CategoriesViewModel;", "transactionsViewModel", "Lcom/kjurkovic/screen/transactions/list/TransactionsViewModel;", "dashboard_debug"})
public final class DashboardScreenKt {
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    public static final void DashboardScreen(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.dashboard.DashboardViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    @kotlin.OptIn(markerClass = {androidx.compose.material.ExperimentalMaterialApi.class})
    private static final void DashboardScreen(com.kjurkovic.screen.dashboard.DashboardViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.dashboard.DashboardIntent> intentChannel, com.kjurkovic.screen.home.HomeViewModel homeViewModel, com.kjurkovic.screens.categories.list.CategoriesViewModel categoryViewModel, com.kjurkovic.screen.transactions.list.TransactionsViewModel transactionsViewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void DashboardBottomBar(com.kjurkovic.screen.dashboard.DashboardViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.dashboard.DashboardIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void BottomNavigationIcon(com.kjurkovic.screen.dashboard.DashboardTab dashboardTab, com.kjurkovic.screen.dashboard.DashboardTab selectedTab) {
    }
}