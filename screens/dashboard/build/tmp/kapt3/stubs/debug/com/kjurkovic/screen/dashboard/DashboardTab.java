package com.kjurkovic.screen.dashboard;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B#\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/kjurkovic/screen/dashboard/DashboardTab;", "", "route", "", "labelRes", "", "iconRes", "(Ljava/lang/String;ILjava/lang/String;II)V", "getIconRes", "()I", "getLabelRes", "getRoute", "()Ljava/lang/String;", "HOME", "TRANSACTIONS", "CATEGORIES", "dashboard_debug"})
public enum DashboardTab {
    /*public static final*/ HOME /* = new HOME(null, 0, 0) */,
    /*public static final*/ TRANSACTIONS /* = new TRANSACTIONS(null, 0, 0) */,
    /*public static final*/ CATEGORIES /* = new CATEGORIES(null, 0, 0) */;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String route = null;
    private final int labelRes = 0;
    private final int iconRes = 0;
    
    DashboardTab(java.lang.String route, @androidx.annotation.StringRes()
    int labelRes, @androidx.annotation.DrawableRes()
    int iconRes) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRoute() {
        return null;
    }
    
    public final int getLabelRes() {
        return 0;
    }
    
    public final int getIconRes() {
        return 0;
    }
}