plugins {
    id("base-android-compose-library")
}

dependencies {
    implementation(libs.bundles.feature)

    implementation(projects.common.annotations)
    implementation(projects.common.framework.mvi)
    implementation(projects.common.framework.navigation)
    implementation(projects.common.resources)
    implementation(projects.common.ui)
    implementation(projects.common.wrappers.dispatchers)

    implementation(projects.screens.home)
    implementation(projects.screens.categories)
    implementation(projects.screens.transactions)

    kapt(libs.hilt.androidCompiler)
}
