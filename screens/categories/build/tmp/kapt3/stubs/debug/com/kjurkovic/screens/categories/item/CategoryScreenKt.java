package com.kjurkovic.screens.categories.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003\u001a\u0010\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\tH\u0007\u001a\u001e\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003\u00a8\u0006\n"}, d2 = {"CategoryDialog", "", "viewState", "Lcom/kjurkovic/screens/categories/item/CategoryViewState;", "intentChannel", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/kjurkovic/screens/categories/item/CategoryIntent;", "CategoryScreen", "viewModel", "Lcom/kjurkovic/screens/categories/item/CategoryViewModel;", "categories_debug"})
public final class CategoryScreenKt {
    
    @androidx.compose.runtime.Composable()
    public static final void CategoryScreen(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screens.categories.item.CategoryViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void CategoryDialog(com.kjurkovic.screens.categories.item.CategoryViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screens.categories.item.CategoryIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void CategoryScreen(com.kjurkovic.screens.categories.item.CategoryViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screens.categories.item.CategoryIntent> intentChannel) {
    }
}