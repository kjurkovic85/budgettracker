package com.kjurkovic.screens.categories.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B9\b\u0002\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0015\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000e\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015\u00a8\u0006\u0016"}, d2 = {"Lcom/kjurkovic/screens/categories/item/CategoryAction;", "", "title", "", "description", "confirmButton", "cancelButton", "actionIntent", "Lcom/kjurkovic/screens/categories/item/CategoryIntent;", "(Ljava/lang/String;IIIILjava/lang/Integer;Lcom/kjurkovic/screens/categories/item/CategoryIntent;)V", "getActionIntent", "()Lcom/kjurkovic/screens/categories/item/CategoryIntent;", "getCancelButton", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getConfirmButton", "()I", "getDescription", "getTitle", "DELETE", "ERROR", "SUCCESS", "categories_debug"})
public enum CategoryAction {
    /*public static final*/ DELETE /* = new DELETE(0, 0, 0, null, null) */,
    /*public static final*/ ERROR /* = new ERROR(0, 0, 0, null, null) */,
    /*public static final*/ SUCCESS /* = new SUCCESS(0, 0, 0, null, null) */;
    private final int title = 0;
    private final int description = 0;
    private final int confirmButton = 0;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer cancelButton = null;
    @org.jetbrains.annotations.NotNull()
    private final com.kjurkovic.screens.categories.item.CategoryIntent actionIntent = null;
    
    CategoryAction(@androidx.annotation.StringRes()
    int title, @androidx.annotation.StringRes()
    int description, @androidx.annotation.StringRes()
    int confirmButton, @androidx.annotation.StringRes()
    java.lang.Integer cancelButton, com.kjurkovic.screens.categories.item.CategoryIntent actionIntent) {
    }
    
    public final int getTitle() {
        return 0;
    }
    
    public final int getDescription() {
        return 0;
    }
    
    public final int getConfirmButton() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCancelButton() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.screens.categories.item.CategoryIntent getActionIntent() {
        return null;
    }
}