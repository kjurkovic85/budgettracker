package com.kjurkovic.screens.categories.list;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u00004\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a(\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\bH\u0003\u001a\u0010\u0010\t\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u000bH\u0007\u001a\u001e\u0010\t\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003\u001a\u001e\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00010\u0010H\u0003\u00a8\u0006\u0011"}, d2 = {"CategoriesList", "", "viewState", "Lcom/kjurkovic/screens/categories/list/CategoriesViewState;", "intentChannel", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/kjurkovic/screens/categories/list/CategoriesIntent;", "modifier", "Landroidx/compose/ui/Modifier;", "CategoriesScreen", "viewModel", "Lcom/kjurkovic/screens/categories/list/CategoriesViewModel;", "CategoryRow", "category", "Lcom/kjurkovic/category_repo/models/CategoryModel;", "onClick", "Lkotlin/Function0;", "categories_debug"})
public final class CategoriesScreenKt {
    
    @androidx.compose.runtime.Composable()
    public static final void CategoriesScreen(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screens.categories.list.CategoriesViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void CategoriesScreen(com.kjurkovic.screens.categories.list.CategoriesViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screens.categories.list.CategoriesIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void CategoriesList(com.kjurkovic.screens.categories.list.CategoriesViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screens.categories.list.CategoriesIntent> intentChannel, androidx.compose.ui.Modifier modifier) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void CategoryRow(com.kjurkovic.category_repo.models.CategoryModel category, kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
    }
}