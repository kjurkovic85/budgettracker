package com.kjurkovic.screens.categories.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B;\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\tH\u00c6\u0003J?\u0010\u0016\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tH\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00062\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u000fR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\f\u00a8\u0006\u001c"}, d2 = {"Lcom/kjurkovic/screens/categories/item/CategoryViewState;", "", "screenTitle", "", "categoryName", "isFormValid", "", "isEditMode", "dialogType", "Lcom/kjurkovic/screens/categories/item/CategoryAction;", "(Ljava/lang/String;Ljava/lang/String;ZZLcom/kjurkovic/screens/categories/item/CategoryAction;)V", "getCategoryName", "()Ljava/lang/String;", "getDialogType", "()Lcom/kjurkovic/screens/categories/item/CategoryAction;", "()Z", "getScreenTitle", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "categories_debug"})
public final class CategoryViewState {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String screenTitle = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String categoryName = null;
    private final boolean isFormValid = false;
    private final boolean isEditMode = false;
    @org.jetbrains.annotations.Nullable()
    private final com.kjurkovic.screens.categories.item.CategoryAction dialogType = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.screens.categories.item.CategoryViewState copy(@org.jetbrains.annotations.Nullable()
    java.lang.String screenTitle, @org.jetbrains.annotations.NotNull()
    java.lang.String categoryName, boolean isFormValid, boolean isEditMode, @org.jetbrains.annotations.Nullable()
    com.kjurkovic.screens.categories.item.CategoryAction dialogType) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public CategoryViewState() {
        super();
    }
    
    public CategoryViewState(@org.jetbrains.annotations.Nullable()
    java.lang.String screenTitle, @org.jetbrains.annotations.NotNull()
    java.lang.String categoryName, boolean isFormValid, boolean isEditMode, @org.jetbrains.annotations.Nullable()
    com.kjurkovic.screens.categories.item.CategoryAction dialogType) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getScreenTitle() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCategoryName() {
        return null;
    }
    
    public final boolean component3() {
        return false;
    }
    
    public final boolean isFormValid() {
        return false;
    }
    
    public final boolean component4() {
        return false;
    }
    
    public final boolean isEditMode() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.kjurkovic.screens.categories.item.CategoryAction component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.kjurkovic.screens.categories.item.CategoryAction getDialogType() {
        return null;
    }
}