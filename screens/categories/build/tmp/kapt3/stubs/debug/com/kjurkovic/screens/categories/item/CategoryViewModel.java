package com.kjurkovic.screens.categories.item;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u0000 \u00152\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u0015\u0016B+\b\u0007\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rJ\u0019\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0003H\u0094@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J\u0011\u0010\u0013\u001a\u00020\u0010H\u0082@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000e\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0017"}, d2 = {"Lcom/kjurkovic/screens/categories/item/CategoryViewModel;", "Lcom/kjurkovic/common/framework/mvi/ComponentViewModel;", "Lcom/kjurkovic/screens/categories/item/CategoryViewState;", "Lcom/kjurkovic/screens/categories/item/CategoryIntent;", "Ljava/lang/Void;", "categoryId", "", "categoriesRepository", "Lcom/kjurkovic/category_repo/CategoriesRepository;", "navigator", "Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "(Ljava/lang/Long;Lcom/kjurkovic/category_repo/CategoriesRepository;Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;Lcom/kjurkovic/common/dispatchers/DispatcherProvider;)V", "Ljava/lang/Long;", "reduce", "", "intent", "(Lcom/kjurkovic/screens/categories/item/CategoryIntent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "saveCategory", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "Factory", "categories_debug"})
public final class CategoryViewModel extends com.kjurkovic.common.framework.mvi.ComponentViewModel<com.kjurkovic.screens.categories.item.CategoryViewState, com.kjurkovic.screens.categories.item.CategoryIntent, java.lang.Void> {
    private final java.lang.Long categoryId = null;
    private final com.kjurkovic.category_repo.CategoriesRepository categoriesRepository = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.screens.categories.item.CategoryViewModel.Companion Companion = null;
    
    @dagger.assisted.AssistedInject()
    public CategoryViewModel(@org.jetbrains.annotations.Nullable()
    @dagger.assisted.Assisted(value = "categoryId")
    java.lang.Long categoryId, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.category_repo.CategoriesRepository categoriesRepository, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.framework.navigation.ScreenNavigator navigator, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Object reduce(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screens.categories.item.CategoryIntent intent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    private final java.lang.Object saveCategory(kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u0019\u0010\u0002\u001a\u00020\u00032\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/kjurkovic/screens/categories/item/CategoryViewModel$Factory;", "", "create", "Lcom/kjurkovic/screens/categories/item/CategoryViewModel;", "categoryId", "", "(Ljava/lang/Long;)Lcom/kjurkovic/screens/categories/item/CategoryViewModel;", "categories_debug"})
    @dagger.assisted.AssistedFactory()
    public static abstract interface Factory {
        
        @org.jetbrains.annotations.NotNull()
        public abstract com.kjurkovic.screens.categories.item.CategoryViewModel create(@org.jetbrains.annotations.Nullable()
        @dagger.assisted.Assisted(value = "categoryId")
        java.lang.Long categoryId);
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J!\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0007\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/kjurkovic/screens/categories/item/CategoryViewModel$Companion;", "", "()V", "create", "Lcom/kjurkovic/screens/categories/item/CategoryViewModel;", "assistedFactory", "Lcom/kjurkovic/screens/categories/item/CategoryViewModel$Factory;", "categoryId", "", "(Lcom/kjurkovic/screens/categories/item/CategoryViewModel$Factory;Ljava/lang/Long;)Lcom/kjurkovic/screens/categories/item/CategoryViewModel;", "categories_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @androidx.compose.runtime.Composable()
        public final com.kjurkovic.screens.categories.item.CategoryViewModel create(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.screens.categories.item.CategoryViewModel.Factory assistedFactory, @org.jetbrains.annotations.Nullable()
        java.lang.Long categoryId) {
            return null;
        }
    }
}