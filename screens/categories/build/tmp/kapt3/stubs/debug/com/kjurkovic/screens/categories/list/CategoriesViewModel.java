package com.kjurkovic.screens.categories.list;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 \u00102\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u0010\u0011B\u001f\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0019\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003H\u0094@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0012"}, d2 = {"Lcom/kjurkovic/screens/categories/list/CategoriesViewModel;", "Lcom/kjurkovic/common/framework/mvi/ComponentViewModel;", "Lcom/kjurkovic/screens/categories/list/CategoriesViewState;", "Lcom/kjurkovic/screens/categories/list/CategoriesIntent;", "Ljava/lang/Void;", "categoriesRepository", "Lcom/kjurkovic/category_repo/CategoriesRepository;", "navigator", "Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "(Lcom/kjurkovic/category_repo/CategoriesRepository;Lcom/kjurkovic/common/framework/navigation/ScreenNavigator;Lcom/kjurkovic/common/dispatchers/DispatcherProvider;)V", "reduce", "", "intent", "(Lcom/kjurkovic/screens/categories/list/CategoriesIntent;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "Factory", "categories_debug"})
public final class CategoriesViewModel extends com.kjurkovic.common.framework.mvi.ComponentViewModel<com.kjurkovic.screens.categories.list.CategoriesViewState, com.kjurkovic.screens.categories.list.CategoriesIntent, java.lang.Void> {
    private final com.kjurkovic.category_repo.CategoriesRepository categoriesRepository = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.screens.categories.list.CategoriesViewModel.Companion Companion = null;
    
    @dagger.assisted.AssistedInject()
    public CategoriesViewModel(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.category_repo.CategoriesRepository categoriesRepository, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.framework.navigation.ScreenNavigator navigator, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider) {
        super(null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    protected java.lang.Object reduce(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screens.categories.list.CategoriesIntent intent, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0004"}, d2 = {"Lcom/kjurkovic/screens/categories/list/CategoriesViewModel$Factory;", "", "create", "Lcom/kjurkovic/screens/categories/list/CategoriesViewModel;", "categories_debug"})
    @dagger.assisted.AssistedFactory()
    public static abstract interface Factory {
        
        @org.jetbrains.annotations.NotNull()
        public abstract com.kjurkovic.screens.categories.list.CategoriesViewModel create();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/kjurkovic/screens/categories/list/CategoriesViewModel$Companion;", "", "()V", "create", "Lcom/kjurkovic/screens/categories/list/CategoriesViewModel;", "assistedFactory", "Lcom/kjurkovic/screens/categories/list/CategoriesViewModel$Factory;", "categories_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @androidx.compose.runtime.Composable()
        public final com.kjurkovic.screens.categories.list.CategoriesViewModel create(@org.jetbrains.annotations.NotNull()
        com.kjurkovic.screens.categories.list.CategoriesViewModel.Factory assistedFactory) {
            return null;
        }
    }
}