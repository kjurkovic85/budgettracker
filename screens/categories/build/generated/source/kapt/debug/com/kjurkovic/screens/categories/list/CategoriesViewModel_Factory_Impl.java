package com.kjurkovic.screens.categories.list;

import dagger.internal.DaggerGenerated;
import dagger.internal.InstanceFactory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoriesViewModel_Factory_Impl implements CategoriesViewModel.Factory {
  private final CategoriesViewModel_Factory delegateFactory;

  CategoriesViewModel_Factory_Impl(CategoriesViewModel_Factory delegateFactory) {
    this.delegateFactory = delegateFactory;
  }

  @Override
  public CategoriesViewModel create() {
    return delegateFactory.get();
  }

  public static Provider<CategoriesViewModel.Factory> create(
      CategoriesViewModel_Factory delegateFactory) {
    return InstanceFactory.create(new CategoriesViewModel_Factory_Impl(delegateFactory));
  }
}
