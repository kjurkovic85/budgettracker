package com.kjurkovic.screens.categories.list;

import com.kjurkovic.category_repo.CategoriesRepository;
import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.common.framework.navigation.ScreenNavigator;
import dagger.internal.DaggerGenerated;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoriesViewModel_Factory {
  private final Provider<CategoriesRepository> categoriesRepositoryProvider;

  private final Provider<ScreenNavigator> navigatorProvider;

  private final Provider<DispatcherProvider> dispatcherProvider;

  public CategoriesViewModel_Factory(Provider<CategoriesRepository> categoriesRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    this.categoriesRepositoryProvider = categoriesRepositoryProvider;
    this.navigatorProvider = navigatorProvider;
    this.dispatcherProvider = dispatcherProvider;
  }

  public CategoriesViewModel get() {
    return newInstance(categoriesRepositoryProvider.get(), navigatorProvider.get(), dispatcherProvider.get());
  }

  public static CategoriesViewModel_Factory create(
      Provider<CategoriesRepository> categoriesRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    return new CategoriesViewModel_Factory(categoriesRepositoryProvider, navigatorProvider, dispatcherProvider);
  }

  public static CategoriesViewModel newInstance(CategoriesRepository categoriesRepository,
      ScreenNavigator navigator, DispatcherProvider dispatcherProvider) {
    return new CategoriesViewModel(categoriesRepository, navigator, dispatcherProvider);
  }
}
