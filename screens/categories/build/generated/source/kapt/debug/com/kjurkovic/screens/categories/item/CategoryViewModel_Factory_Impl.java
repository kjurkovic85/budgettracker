package com.kjurkovic.screens.categories.item;

import dagger.internal.DaggerGenerated;
import dagger.internal.InstanceFactory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoryViewModel_Factory_Impl implements CategoryViewModel.Factory {
  private final CategoryViewModel_Factory delegateFactory;

  CategoryViewModel_Factory_Impl(CategoryViewModel_Factory delegateFactory) {
    this.delegateFactory = delegateFactory;
  }

  @Override
  public CategoryViewModel create(Long categoryId) {
    return delegateFactory.get(categoryId);
  }

  public static Provider<CategoryViewModel.Factory> create(
      CategoryViewModel_Factory delegateFactory) {
    return InstanceFactory.create(new CategoryViewModel_Factory_Impl(delegateFactory));
  }
}
