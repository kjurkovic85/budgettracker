package com.kjurkovic.screens.categories;

import com.kjurkovic.screens.categories.item.CategoryViewModel;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoriesNavigationFactory_Factory implements Factory<CategoriesNavigationFactory> {
  private final Provider<CategoryViewModel.Factory> dashboardViewModelFactoryProvider;

  public CategoriesNavigationFactory_Factory(
      Provider<CategoryViewModel.Factory> dashboardViewModelFactoryProvider) {
    this.dashboardViewModelFactoryProvider = dashboardViewModelFactoryProvider;
  }

  @Override
  public CategoriesNavigationFactory get() {
    return newInstance(dashboardViewModelFactoryProvider.get());
  }

  public static CategoriesNavigationFactory_Factory create(
      Provider<CategoryViewModel.Factory> dashboardViewModelFactoryProvider) {
    return new CategoriesNavigationFactory_Factory(dashboardViewModelFactoryProvider);
  }

  public static CategoriesNavigationFactory newInstance(
      CategoryViewModel.Factory dashboardViewModelFactory) {
    return new CategoriesNavigationFactory(dashboardViewModelFactory);
  }
}
