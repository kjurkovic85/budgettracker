package com.kjurkovic.screens.categories.item

import androidx.annotation.StringRes
import com.kjurkovic.screens.categories.R

enum class CategoryAction(
    @StringRes val title: Int,
    @StringRes val description: Int,
    @StringRes val confirmButton: Int,
    @StringRes val cancelButton: Int? = null,
    val actionIntent: CategoryIntent,
) {
    DELETE(
        title = R.string.categories_confirm_delete_title,
        description = R.string.categories_confirm_delete_description,
        confirmButton = R.string.txt_ok,
        cancelButton = R.string.txt_cancel,
        actionIntent = CategoryIntent.ConfirmDelete,
    ),
    ERROR(
        title = R.string.categories_error_title,
        description = R.string.categories_error_description,
        confirmButton = R.string.txt_ok,
        actionIntent = CategoryIntent.DismissDialog,
    ),
    SUCCESS(
        title = R.string.categories_success_title,
        description = R.string.categories_success_description,
        confirmButton = R.string.txt_ok,
        actionIntent = CategoryIntent.BackClicked,
    )
}