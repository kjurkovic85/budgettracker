package com.kjurkovic.screens.categories.item

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import com.google.accompanist.insets.imePadding
import com.kjurkovic.resources.theme.icons
import com.kjurkovic.screens.categories.R
import com.kjurkovic.ui.*
import kotlinx.coroutines.flow.MutableSharedFlow

@Composable
fun CategoryScreen(
    viewModel: CategoryViewModel,
) = viewModel.Screen { viewState, intentChannel, _ ->
    CategoryScreen(viewState = viewState, intentChannel = intentChannel)
    CategoryDialog(viewState = viewState, intentChannel = intentChannel)
}

@Composable
private fun CategoryDialog(
    viewState: CategoryViewState,
    intentChannel: MutableSharedFlow<CategoryIntent>,
) {
    if (viewState.dialogType != null) {
        Alert(
            title = stringResource(viewState.dialogType.title),
            text = stringResource(viewState.dialogType.description),
            confirmText = stringResource(viewState.dialogType.confirmButton),
            cancelText = viewState.dialogType.cancelButton?.let { stringResource(it) },
            onConfirm = { intentChannel.tryEmit(viewState.dialogType.actionIntent) },
            onCancel = viewState.dialogType.cancelButton?.let {
                { intentChannel.tryEmit(CategoryIntent.DismissDialog) }
            },
            onDismiss = { intentChannel.tryEmit(CategoryIntent.DismissDialog) }
        )
    }
}

@Composable
private fun CategoryScreen(
    viewState: CategoryViewState,
    intentChannel: MutableSharedFlow<CategoryIntent>,
) {
    ScreenComponent(
        topBarContent = {
            ScreenToolbar(
                title = viewState.screenTitle ?: stringResource(R.string.categories_add_new),
                onBackClicked = { intentChannel.tryEmit(CategoryIntent.BackClicked) },
                rightContent = {
                    if (viewState.isEditMode) {
                        IconButton(onClick = { intentChannel.tryEmit(CategoryIntent.DeleteClicked) }) {
                            Icon(
                                imageVector = MaterialTheme.icons.Delete,
                                contentDescription = ""
                            )
                        }
                    }
                }
            )
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .imePadding()
                .padding(
                    top = innerPadding.calculateTopPadding(),
                    bottom = innerPadding.calculateBottomPadding(),
                    start = dimensionResource(R.dimen.size_16),
                    end = dimensionResource(R.dimen.size_16),
                )
        ) {
            TextInput(
                value = viewState.categoryName,
                placeholder = stringResource(R.string.categories_name_label),
                onValueChange = { intentChannel.tryEmit(CategoryIntent.NameChanged(it)) },
                modifier = Modifier
                    .padding(top = dimensionResource(R.dimen.size_24))
                    .fillMaxWidth()
            )
            Spacer(modifier = Modifier.weight(1f))
            PrimaryButton(
                text = stringResource(R.string.categories_save),
                enabled = viewState.isFormValid,
                onClick = { intentChannel.tryEmit(CategoryIntent.SaveClicked) },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = dimensionResource(R.dimen.size_16))
            )
        }
    }
}
