package com.kjurkovic.screens.categories.list

import com.kjurkovic.category_repo.models.CategoryModel

data class CategoriesViewState(
    val categories: List<CategoryModel> = emptyList()
)

sealed class CategoriesIntent {
    data class CategoryClicked(val categoryId: Long): CategoriesIntent()
    object NewCategoryClicked: CategoriesIntent()
}