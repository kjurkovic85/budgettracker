package com.kjurkovic.screens.categories.list

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.key
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.resources.theme.allColors
import com.kjurkovic.resources.theme.icons
import com.kjurkovic.screens.categories.R
import com.kjurkovic.ui.Body
import com.kjurkovic.ui.EmptyScreen
import com.kjurkovic.ui.ScreenComponent
import com.kjurkovic.ui.ScreenToolbar
import kotlinx.coroutines.flow.MutableSharedFlow

@Composable
fun CategoriesScreen(
    viewModel: CategoriesViewModel,
) = viewModel.Screen { viewState, intentChannel, _ ->
    CategoriesScreen(
        viewState = viewState,
        intentChannel = intentChannel
    )
}

@Composable
private fun CategoriesScreen(
    viewState: CategoriesViewState,
    intentChannel: MutableSharedFlow<CategoriesIntent>,
) {
    ScreenComponent(
        topBarContent = {
            ScreenToolbar(
                title = stringResource(R.string.categories),
                rightContent = {
                    IconButton(onClick = { intentChannel.tryEmit(CategoriesIntent.NewCategoryClicked) }) {
                        Icon(
                            imageVector = MaterialTheme.icons.Add,
                            contentDescription = ""
                        )
                    }
                }
            )
        }
    ) {
        if (viewState.categories.isEmpty()) {
            EmptyScreen(
                modifier = Modifier.padding(it),
                text = R.string.categories_empty
            )
        } else {
            CategoriesList(
                viewState = viewState,
                intentChannel = intentChannel,
                modifier = Modifier.padding(it),
            )
        }
    }
}

@Composable
private fun CategoriesList(
    viewState: CategoriesViewState,
    intentChannel: MutableSharedFlow<CategoriesIntent>,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier = modifier.fillMaxSize()
    ) {
        items(viewState.categories) {
            key(it.id!!) {
                CategoryRow(
                    category = it,
                    onClick = { intentChannel.tryEmit(CategoriesIntent.CategoryClicked(it.id!!)) }
                )
            }
        }
    }
}

@Composable
private fun CategoryRow(
    category: CategoryModel,
    onClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = dimensionResource(R.dimen.size_16)),
        ) {
            Body(
                text = category.name,
                modifier = Modifier
                    .padding(vertical = dimensionResource(R.dimen.size_16))
            )
            Icon(
                imageVector = MaterialTheme.icons.KeyboardArrowRight,
                contentDescription = "",
                tint = MaterialTheme.allColors.placeholder,
            )
        }
        Divider(
            startIndent = dimensionResource(R.dimen.size_16)
        )
    }
}
