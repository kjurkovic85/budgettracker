package com.kjurkovic.screens.categories.item

data class CategoryViewState(
    val screenTitle: String? = null,
    val categoryName: String = "",
    val isFormValid: Boolean = false,
    val isEditMode: Boolean = false,
    val dialogType: CategoryAction? = null,
)

sealed class CategoryIntent {
    data class NameChanged(val name: String) : CategoryIntent()
    object SaveClicked : CategoryIntent()
    object BackClicked : CategoryIntent()
    object DeleteClicked : CategoryIntent()
    object ConfirmDelete : CategoryIntent()
    object DismissDialog : CategoryIntent()
}
