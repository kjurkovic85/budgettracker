package com.kjurkovic.screens.categories

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.HiltNavigationFactory
import com.kjurkovic.common.framework.navigation.NavigationFactory
import com.kjurkovic.screens.categories.item.CategoryScreen
import com.kjurkovic.screens.categories.item.CategoryViewModel
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dagger.multibindings.IntoSet
import javax.inject.Inject

@ActivityRetainedScoped
@HiltNavigationFactory
class CategoriesNavigationFactory @Inject constructor(
    private val dashboardViewModelFactory: CategoryViewModel.Factory,
) : NavigationFactory {

    override fun create(builder: NavGraphBuilder, navController: NavHostController) {
        builder.addCategoryRoute()
        builder.editCategoryRoute()
    }

    private fun NavGraphBuilder.addCategoryRoute() {
        composable(route = Destination.Categories.Add.fullRoute) {
            val viewModel = CategoryViewModel.create(
                assistedFactory = dashboardViewModelFactory,
            )
            CategoryScreen(viewModel)
        }
    }

    private fun NavGraphBuilder.editCategoryRoute() {
        composable(route = Destination.Categories.Edit.fullRoute) {
            val viewModel = CategoryViewModel.create(
                assistedFactory = dashboardViewModelFactory,
                categoryId = it.arguments!!.getString(Destination.Categories.Edit.CATEGORY_ID)!!.toLong()
            )
            CategoryScreen(viewModel)
        }
    }
}

@Module
@InstallIn(ActivityRetainedComponent::class)
internal interface ComposeNavigationFactoryModule {
    @Binds
    @IntoSet
    fun bindNavigationFactory(factory: CategoriesNavigationFactory): NavigationFactory
}
