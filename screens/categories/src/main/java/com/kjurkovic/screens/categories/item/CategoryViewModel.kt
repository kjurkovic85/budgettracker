package com.kjurkovic.screens.categories.item

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.category_repo.CategoriesRepository
import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class CategoryViewModel @AssistedInject constructor(
    @Assisted("categoryId") private val categoryId: Long?,
    private val categoriesRepository: CategoriesRepository,
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
) : ComponentViewModel<CategoryViewState, CategoryIntent, Void>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = CategoryViewState()
) {

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("categoryId") categoryId: Long?,
        ): CategoryViewModel
    }

    companion object {
        @Composable
        fun create(
            assistedFactory: Factory,
            categoryId: Long? = null,
        ): CategoryViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create(
                    categoryId = categoryId,
                ) as T
            }
        })
    }

    init {
        launchMain {
            categoryId?.let {
                val categoryName = categoriesRepository.category(it)?.name ?: ""
                setState {
                    copy(
                        screenTitle = categoryName,
                        categoryName = categoryName,
                        isEditMode = true,
                        isFormValid = categoryName.isNotBlank()
                    )
                }
            }
        }
    }

    override suspend fun reduce(intent: CategoryIntent) {
        when (intent) {
            is CategoryIntent.NameChanged -> setState {
                copy(
                    categoryName = intent.name,
                    isFormValid = intent.name.isNotBlank()
                )
            }
            CategoryIntent.SaveClicked -> {
                runCatching { saveCategory() }
                    .onSuccess { setState { copy(dialogType = CategoryAction.SUCCESS) } }
                    .onFailure { setState { copy(dialogType = CategoryAction.ERROR) } }
            }
            CategoryIntent.BackClicked -> navigator.navigateBack()
            CategoryIntent.DeleteClicked -> setState { copy(dialogType = CategoryAction.DELETE) }
            CategoryIntent.ConfirmDelete -> {
                // TODO: update transactions with category delete - or set cascade option on column
                categoriesRepository.delete(categoryId!!)
                navigator.navigateBack()
            }
            CategoryIntent.DismissDialog -> setState { copy(dialogType = null) }
        }
    }

    private suspend fun saveCategory() {
        if (viewState.value.isFormValid) {
            categoriesRepository.save(
                CategoryModel(
                    categoryId,
                    viewState.value.categoryName
                )
            )
        }
    }
}
