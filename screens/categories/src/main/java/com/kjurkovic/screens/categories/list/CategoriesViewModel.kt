package com.kjurkovic.screens.categories.list

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.category_repo.CategoriesRepository
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class CategoriesViewModel @AssistedInject constructor(
    private val categoriesRepository: CategoriesRepository,
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
) : ComponentViewModel<CategoriesViewState, CategoriesIntent, Void>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = CategoriesViewState()
) {

    @AssistedFactory
    interface Factory {
        fun create(): CategoriesViewModel
    }

    companion object {
        @Composable
        fun create(
            assistedFactory: Factory,
        ): CategoriesViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        })
    }

    init {
        launchMain {
            categoriesRepository.categories().collect {
                setState { copy(categories = it) }
            }
        }
    }

    override suspend fun reduce(intent: CategoriesIntent) {
        when (intent) {
            is CategoriesIntent.CategoryClicked -> {
                navigator.navigateTo(Destination.Categories.Edit(intent.categoryId))
            }
            CategoriesIntent.NewCategoryClicked -> {
                navigator.navigateTo(Destination.Categories.Add.fullRoute)
            }
        }
    }
}