package com.kjurkovic.screen.home

import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import java.math.BigDecimal

data class HomeViewState(
    val transactions: List<TransactionModel> = emptyList(),
    val expensePercentage: Double = 0.toDouble(),
    val balance: BigDecimal = BigDecimal.ZERO,
)

sealed class HomeIntent {
    data class TransactionClicked(val transaction: TransactionModel) : HomeIntent()
    object AddTransaction: HomeIntent()
}
