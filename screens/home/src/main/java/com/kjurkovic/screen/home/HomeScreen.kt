package com.kjurkovic.screen.home

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.key
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.kjurkovic.data.common.TransactionType
import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import com.kjurkovic.resources.formatters.stringify
import com.kjurkovic.resources.formatters.toPrice
import com.kjurkovic.resources.theme.allColors
import com.kjurkovic.ui.*
import kotlinx.coroutines.flow.MutableSharedFlow

@Composable
fun HomeScreen(
    viewModel: HomeViewModel,
) = viewModel.Screen { viewState, intentChannel, _ ->
    HomeScreen(
        viewState = viewState,
        intentChannel = intentChannel
    )
}

@Composable
private fun HomeScreen(
    viewState: HomeViewState,
    intentChannel: MutableSharedFlow<HomeIntent>,
) {
    ScreenComponent {
        if (viewState.transactions.isEmpty()) {
            EmptyHomeScreen(intentChannel, it)
        } else {
            BalanceScreen(viewState, intentChannel, it)
        }
    }
}

@Composable
private fun BalanceScreen(
    viewState: HomeViewState,
    intentChannel: MutableSharedFlow<HomeIntent>,
    paddingValues: PaddingValues,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = paddingValues.calculateTopPadding())
    ) {
        item {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        top = dimensionResource(R.dimen.size_32),
                        bottom = dimensionResource(R.dimen.size_32),
                    ),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Caption(text = stringResource(R.string.home_balance))
                Header(
                    text = viewState.balance.toPrice(),
                    style = MaterialTheme.typography.h1.copy(fontWeight = FontWeight.Bold)
                )
            }
        }

        item {
            HorizontalBar(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(10.dp)
                    .padding(horizontal = dimensionResource(R.dimen.size_24)),
                value = viewState.expensePercentage
            )
        }

        item {
            Body(
                text = stringResource(R.string.home_last_transactions).uppercase(),
                style = MaterialTheme.typography.body2.copy(fontWeight = FontWeight.Bold),
                modifier = Modifier.padding(
                    start = dimensionResource(R.dimen.size_16),
                    end = dimensionResource(R.dimen.size_16),
                    top = dimensionResource(R.dimen.size_32),
                )
            )
        }

        transactionListItems(viewState = viewState, intentChannel = intentChannel)

    }
}

@Composable
private fun EmptyHomeScreen(
    intentChannel: MutableSharedFlow<HomeIntent>,
    paddingValues: PaddingValues,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(paddingValues)
    ) {
        Spacer(modifier = Modifier.weight(1f))
        Body(
            text = stringResource(R.string.home_empty),
            style = MaterialTheme.typography.body2.copy(fontWeight = FontWeight.Bold),
            modifier = Modifier.padding(
                start = dimensionResource(R.dimen.size_16),
                end = dimensionResource(R.dimen.size_16),
                top = dimensionResource(R.dimen.size_32),
                bottom = dimensionResource(R.dimen.size_32),
            )
        )
        Spacer(modifier = Modifier.weight(1f))
        PrimaryButton(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = dimensionResource(R.dimen.size_16)),
            text = stringResource(R.string.home_add_transaction),
            onClick = { intentChannel.tryEmit(HomeIntent.AddTransaction) }
        )
    }
}

private fun LazyListScope.transactionListItems(
    viewState: HomeViewState,
    intentChannel: MutableSharedFlow<HomeIntent>,
) {
    items(viewState.transactions) { transaction ->
        key(transaction.id) {
            TransactionRow(
                transaction = transaction,
                onClick = { intentChannel.tryEmit(HomeIntent.TransactionClicked(transaction)) }
            )
        }
    }
}

@Composable
private fun TransactionRow(
    transaction: TransactionModel,
    onClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = dimensionResource(R.dimen.size_16),
                    end = dimensionResource(R.dimen.size_16),
                    top = dimensionResource(R.dimen.size_12),
                ),
        ) {
            Body(
                text = transaction.description,
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.Medium)
            )
            Body(
                text = transaction.amount.toPrice(),
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.Bold),
                color = if (transaction.type == TransactionType.INCOME)
                    MaterialTheme.allColors.positiveAmount
                else
                    MaterialTheme.allColors.negativeAmount
            )
        }

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = dimensionResource(R.dimen.size_16),
                    end = dimensionResource(R.dimen.size_16),
                    top = dimensionResource(R.dimen.size_4),
                ),
        ) {
            Caption(
                text = transaction.category?.name ?: stringResource(R.string.home_category_unknown),
            )
            Caption(
                text = transaction.date.stringify(),
            )
        }

        Divider(
            startIndent = dimensionResource(R.dimen.size_16),
            modifier = Modifier.padding(top = dimensionResource(R.dimen.size_12))
        )
    }
}
