package com.kjurkovic.screen.home

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import com.kjurkovic.data.common.TransactionType
import com.kjurkovic.repo.transaction_repo.TransactionsRepository
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import java.lang.Double.max
import java.math.BigDecimal

class HomeViewModel @AssistedInject constructor(
    private val transactionsRepository: TransactionsRepository,
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
) : ComponentViewModel<HomeViewState, HomeIntent, Void>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = HomeViewState()
) {

    @AssistedFactory
    interface Factory {
        fun create(): HomeViewModel
    }

    companion object {
        private const val PAGE_SIZE = 5
        private const val PAGE_NUM = 1

        @Composable
        fun create(
            assistedFactory: Factory,
        ): HomeViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        })
    }

    init {
        launchMain {
            launch {
                val transactions = transactionsRepository.transactions(PAGE_SIZE, PAGE_NUM)
                setState { copy(transactions = transactions) }
            }

            launch {
                val balance = transactionsRepository.balance()
                val allTransactions = transactionsRepository.transactions()
                val total = allTransactions
                    .map { it.amount }
                    .fold(BigDecimal.ZERO) { acc, next -> acc.plus(next) }
                val expenses = allTransactions
                    .filter { it.type == TransactionType.EXPENSE }
                    .map { it.amount }
                    .fold(BigDecimal.ZERO) { acc, next -> acc.plus(next) }

                val expensePercentage = if (total > BigDecimal.ZERO)
                    max(.1, expenses.toDouble() / total.toDouble())
                else 0.toDouble()

                setState {
                    copy(
                        expensePercentage = expensePercentage,
                        balance = balance,
                    )
                }
            }
        }
    }

    override suspend fun reduce(intent: HomeIntent) {
        when (intent) {
            is HomeIntent.TransactionClicked -> navigator.navigateTo(Destination.Transactions.Edit(intent.transaction.id.toString()))
            HomeIntent.AddTransaction -> navigator.navigateTo(Destination.Transactions.Add.fullRoute)
        }
    }
}
