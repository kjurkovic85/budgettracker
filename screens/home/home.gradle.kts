plugins {
    id("base-android-compose-library")
}

dependencies {
    implementation(libs.bundles.feature)

    implementation(projects.common.annotations)
    implementation(projects.common.framework.mvi)
    implementation(projects.common.framework.navigation)
    implementation(projects.common.resources)
    implementation(projects.common.ui)
    implementation(projects.common.wrappers.dispatchers)

    implementation(projects.data.repo.categoryRepo)
    implementation(projects.data.repo.transactionRepo)

    kapt(libs.hilt.androidCompiler)
}
