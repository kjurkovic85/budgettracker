package com.kjurkovic.screen.home;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000:\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a&\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\u0007\u001a\u00020\bH\u0003\u001a\u001e\u0010\t\u001a\u00020\u00012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\u0007\u001a\u00020\bH\u0003\u001a\u0010\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\fH\u0007\u001a\u001e\u0010\n\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0003\u001a\u001e\u0010\r\u001a\u00020\u00012\u0006\u0010\u000e\u001a\u00020\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00010\u0011H\u0003\u001a\"\u0010\u0012\u001a\u00020\u0001*\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0002\u00a8\u0006\u0014"}, d2 = {"BalanceScreen", "", "viewState", "Lcom/kjurkovic/screen/home/HomeViewState;", "intentChannel", "Lkotlinx/coroutines/flow/MutableSharedFlow;", "Lcom/kjurkovic/screen/home/HomeIntent;", "paddingValues", "Landroidx/compose/foundation/layout/PaddingValues;", "EmptyHomeScreen", "HomeScreen", "viewModel", "Lcom/kjurkovic/screen/home/HomeViewModel;", "TransactionRow", "transaction", "Lcom/kjurkovic/repo/transaction_repo/model/TransactionModel;", "onClick", "Lkotlin/Function0;", "transactionListItems", "Landroidx/compose/foundation/lazy/LazyListScope;", "home_debug"})
public final class HomeScreenKt {
    
    @androidx.compose.runtime.Composable()
    public static final void HomeScreen(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.screen.home.HomeViewModel viewModel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void HomeScreen(com.kjurkovic.screen.home.HomeViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.home.HomeIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void BalanceScreen(com.kjurkovic.screen.home.HomeViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.home.HomeIntent> intentChannel, androidx.compose.foundation.layout.PaddingValues paddingValues) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void EmptyHomeScreen(kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.home.HomeIntent> intentChannel, androidx.compose.foundation.layout.PaddingValues paddingValues) {
    }
    
    private static final void transactionListItems(androidx.compose.foundation.lazy.LazyListScope $this$transactionListItems, com.kjurkovic.screen.home.HomeViewState viewState, kotlinx.coroutines.flow.MutableSharedFlow<com.kjurkovic.screen.home.HomeIntent> intentChannel) {
    }
    
    @androidx.compose.runtime.Composable()
    private static final void TransactionRow(com.kjurkovic.repo.transaction_repo.model.TransactionModel transaction, kotlin.jvm.functions.Function0<kotlin.Unit> onClick) {
    }
}