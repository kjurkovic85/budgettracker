package com.kjurkovic.screen.home;

import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.common.framework.navigation.ScreenNavigator;
import com.kjurkovic.repo.transaction_repo.TransactionsRepository;
import dagger.internal.DaggerGenerated;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class HomeViewModel_Factory {
  private final Provider<TransactionsRepository> transactionsRepositoryProvider;

  private final Provider<ScreenNavigator> navigatorProvider;

  private final Provider<DispatcherProvider> dispatcherProvider;

  public HomeViewModel_Factory(Provider<TransactionsRepository> transactionsRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    this.transactionsRepositoryProvider = transactionsRepositoryProvider;
    this.navigatorProvider = navigatorProvider;
    this.dispatcherProvider = dispatcherProvider;
  }

  public HomeViewModel get() {
    return newInstance(transactionsRepositoryProvider.get(), navigatorProvider.get(), dispatcherProvider.get());
  }

  public static HomeViewModel_Factory create(
      Provider<TransactionsRepository> transactionsRepositoryProvider,
      Provider<ScreenNavigator> navigatorProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    return new HomeViewModel_Factory(transactionsRepositoryProvider, navigatorProvider, dispatcherProvider);
  }

  public static HomeViewModel newInstance(TransactionsRepository transactionsRepository,
      ScreenNavigator navigator, DispatcherProvider dispatcherProvider) {
    return new HomeViewModel(transactionsRepository, navigator, dispatcherProvider);
  }
}
