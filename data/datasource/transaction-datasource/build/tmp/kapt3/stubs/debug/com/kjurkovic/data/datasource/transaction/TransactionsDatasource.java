package com.kjurkovic.data.datasource.transaction;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\u0007\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0019\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eJ\u0019\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u00132\u0006\u0010\f\u001a\u00020\rH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eJ\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0015H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\'\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0017H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001a"}, d2 = {"Lcom/kjurkovic/data/datasource/transaction/TransactionsDatasource;", "Lcom/kjurkovic/repo/transaction_repo/TransactionsRepository;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "database", "Lcom/kjurkovic/data/database/Database;", "(Lcom/kjurkovic/common/dispatchers/DispatcherProvider;Lcom/kjurkovic/data/database/Database;)V", "balance", "Ljava/math/BigDecimal;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "delete", "", "id", "Ljava/util/UUID;", "(Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "save", "transaction", "Lcom/kjurkovic/repo/transaction_repo/model/TransactionModel;", "(Lcom/kjurkovic/repo/transaction_repo/model/TransactionModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Lkotlinx/coroutines/flow/Flow;", "transactions", "", "limit", "", "page", "(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "transaction-datasource_debug"})
public final class TransactionsDatasource implements com.kjurkovic.repo.transaction_repo.TransactionsRepository {
    private final com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider = null;
    private final com.kjurkovic.data.database.Database database = null;
    
    @javax.inject.Inject()
    public TransactionsDatasource(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.database.Database database) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object transactions(int limit, int page, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.kjurkovic.repo.transaction_repo.model.TransactionModel>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object transactions(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.kjurkovic.repo.transaction_repo.model.TransactionModel>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object transaction(@org.jetbrains.annotations.NotNull()
    java.util.UUID id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<com.kjurkovic.repo.transaction_repo.model.TransactionModel>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object balance(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.math.BigDecimal> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object delete(@org.jetbrains.annotations.NotNull()
    java.util.UUID id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object save(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.repo.transaction_repo.model.TransactionModel transaction, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}