package com.kjurkovic.data.datasource.transaction;

import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.data.database.Database;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TransactionsDatasource_Factory implements Factory<TransactionsDatasource> {
  private final Provider<DispatcherProvider> dispatcherProvider;

  private final Provider<Database> databaseProvider;

  public TransactionsDatasource_Factory(Provider<DispatcherProvider> dispatcherProvider,
      Provider<Database> databaseProvider) {
    this.dispatcherProvider = dispatcherProvider;
    this.databaseProvider = databaseProvider;
  }

  @Override
  public TransactionsDatasource get() {
    return newInstance(dispatcherProvider.get(), databaseProvider.get());
  }

  public static TransactionsDatasource_Factory create(
      Provider<DispatcherProvider> dispatcherProvider, Provider<Database> databaseProvider) {
    return new TransactionsDatasource_Factory(dispatcherProvider, databaseProvider);
  }

  public static TransactionsDatasource newInstance(DispatcherProvider dispatcherProvider,
      Database database) {
    return new TransactionsDatasource(dispatcherProvider, database);
  }
}
