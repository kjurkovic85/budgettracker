plugins {
    id("base-android-library")
}

dependencies {
    api(projects.data.repo.transactionRepo)
    implementation(libs.bundles.datasource)
    implementation(projects.common.wrappers.dispatchers)
    implementation(projects.data.database)
    implementation(projects.data.datasource.categoryDatasource)
    implementation(projects.data.repo.categoryRepo)
    kapt(libs.hilt.androidCompiler)
}
