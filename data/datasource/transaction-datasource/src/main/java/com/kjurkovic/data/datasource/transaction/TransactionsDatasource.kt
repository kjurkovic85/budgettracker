package com.kjurkovic.data.datasource.transaction

import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.data.common.TransactionType
import com.kjurkovic.data.database.Database
import com.kjurkovic.data.datasource.transaction.mapper.toEntity
import com.kjurkovic.data.datasource.transaction.mapper.toModel
import com.kjurkovic.repo.transaction_repo.TransactionsRepository
import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.withContext
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*
import javax.inject.Inject

class TransactionsDatasource @Inject constructor(
    private val dispatcherProvider: DispatcherProvider,
    private val database: Database,
) : TransactionsRepository {

    override suspend fun transactions(limit: Int, page: Int): List<TransactionModel> =
        withContext(dispatcherProvider.io()) {
            val currentPage = if (page <= 0) 1 else page
            database.transactionDao
                .transactions(limit, (currentPage - 1) * limit)
                .map { it.toModel() }
        }

    override suspend fun transactions(): List<TransactionModel> =
        withContext(dispatcherProvider.io()) {
            database.transactionDao.get().map { it.toModel() }
        }

    override suspend fun transaction(id: UUID): Flow<TransactionModel> =
        withContext(dispatcherProvider.io()) {
            database.transactionDao.transaction(id).mapNotNull { it?.toModel() }
        }

    override suspend fun balance(): BigDecimal =
        withContext(dispatcherProvider.io()) {
            database.transactionDao
                .get()
                .map { it.transactionEntity }
                .map { if (it.type == TransactionType.EXPENSE) it.amount.negate() else it.amount }
                .fold(BigDecimal.ZERO) { acc, next -> acc.plus(next) }
        }

    override suspend fun delete(id: UUID) =
        withContext(dispatcherProvider.io()) {
            database.transactionDao.delete(id)
        }

    override suspend fun save(transaction: TransactionModel) =
        withContext(dispatcherProvider.io()) {
            database.transactionDao.upsert(transaction.toEntity())
        }
}
