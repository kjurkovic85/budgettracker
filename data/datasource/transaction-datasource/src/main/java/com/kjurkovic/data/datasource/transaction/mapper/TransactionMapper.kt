package com.kjurkovic.data.datasource.transaction.mapper

import com.kjurkovic.data.database.entities.TransactionEntity
import com.kjurkovic.data.database.entities.result.TransactionCategoryResult
import com.kjurkovic.data.datasource.category.mapper.toModel
import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import java.util.*
import kotlin.reflect.typeOf

fun TransactionCategoryResult.toModel() = TransactionModel(
    id = transactionEntity.id,
    description = transactionEntity.description,
    amount = transactionEntity.amount,
    date = transactionEntity.date,
    type = transactionEntity.type,
    category = categoryEntity?.toModel(),
)

fun TransactionModel.toEntity() = TransactionEntity(
    id = id ?: UUID.randomUUID(),
    description = description,
    amount = amount,
    date = date,
    type = type,
    categoryId = category?.id
)
