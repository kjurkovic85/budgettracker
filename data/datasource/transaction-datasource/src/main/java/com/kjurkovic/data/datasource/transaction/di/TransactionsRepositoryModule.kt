package com.kjurkovic.data.datasource.transaction.di

import com.kjurkovic.data.datasource.transaction.TransactionsDatasource
import com.kjurkovic.repo.transaction_repo.TransactionsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
interface TransactionsRepositoryModule {
    @Binds
    fun bindSignalRepository(signalRepository: TransactionsDatasource): TransactionsRepository
}
