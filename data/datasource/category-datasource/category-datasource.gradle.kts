plugins {
    id("base-android-library")
}

dependencies {
    api(projects.data.repo.categoryRepo)
    implementation(libs.bundles.datasource)
    implementation(projects.common.wrappers.dispatchers)
    implementation(projects.data.database)
    kapt(libs.hilt.androidCompiler)
}
