package com.kjurkovic.data.datasource.category.mapper

import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.data.database.entities.CategoryEntity

fun CategoryEntity.toModel() = CategoryModel(
    id = id,
    name = name,
)

fun CategoryModel.toEntity() = CategoryEntity(
    id = id,
    name = name,
)
