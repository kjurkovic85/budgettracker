package com.kjurkovic.data.datasource.category

import com.kjurkovic.category_repo.CategoriesRepository
import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.data.datasource.category.mapper.toEntity
import com.kjurkovic.data.datasource.category.mapper.toModel
import com.kjurkovic.data.database.Database
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CategoriesDatasource @Inject constructor(
    private val database: Database,
    private val dispatcherProvider: DispatcherProvider,
) : CategoriesRepository {

    override suspend fun save(categoryModel: CategoryModel) = withContext(dispatcherProvider.io()) {
        database.categoryDao.upsert(categoryModel.toEntity())
    }

    override suspend fun categories(): Flow<List<CategoryModel>> =
        withContext(dispatcherProvider.io()) {
            database.categoryDao.observeCategories().map { it.map { it.toModel() } }
        }

    override suspend fun category(id: Long): CategoryModel? =
        withContext(dispatcherProvider.io()) {
            database.categoryDao.categoryById(id)?.toModel()
        }

    override suspend fun delete(categoryId: Long) = withContext(dispatcherProvider.io()) {
        database.categoryDao.delete(categoryId)
    }
}