package com.kjurkovic.data.datasource.category.di

import com.kjurkovic.category_repo.CategoriesRepository
import com.kjurkovic.data.datasource.category.CategoriesDatasource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@InstallIn(SingletonComponent::class)
@Module
interface CategoriesRepositoryModule {
    @Binds
    fun bindSignalRepository(signalRepository: CategoriesDatasource): CategoriesRepository
}
