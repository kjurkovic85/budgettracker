package com.kjurkovic.data.datasource.category;

import com.kjurkovic.common.dispatchers.DispatcherProvider;
import com.kjurkovic.data.database.Database;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoriesDatasource_Factory implements Factory<CategoriesDatasource> {
  private final Provider<Database> databaseProvider;

  private final Provider<DispatcherProvider> dispatcherProvider;

  public CategoriesDatasource_Factory(Provider<Database> databaseProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    this.databaseProvider = databaseProvider;
    this.dispatcherProvider = dispatcherProvider;
  }

  @Override
  public CategoriesDatasource get() {
    return newInstance(databaseProvider.get(), dispatcherProvider.get());
  }

  public static CategoriesDatasource_Factory create(Provider<Database> databaseProvider,
      Provider<DispatcherProvider> dispatcherProvider) {
    return new CategoriesDatasource_Factory(databaseProvider, dispatcherProvider);
  }

  public static CategoriesDatasource newInstance(Database database,
      DispatcherProvider dispatcherProvider) {
    return new CategoriesDatasource(database, dispatcherProvider);
  }
}
