package com.kjurkovic.data.datasource.category.mapper;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0002*\u00020\u0001\u00a8\u0006\u0004"}, d2 = {"toEntity", "Lcom/kjurkovic/data/database/entities/CategoryEntity;", "Lcom/kjurkovic/category_repo/models/CategoryModel;", "toModel", "category-datasource_debug"})
public final class CategoriesMapperKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.category_repo.models.CategoryModel toModel(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.database.entities.CategoryEntity $this$toModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.data.database.entities.CategoryEntity toEntity(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.category_repo.models.CategoryModel $this$toEntity) {
        return null;
    }
}