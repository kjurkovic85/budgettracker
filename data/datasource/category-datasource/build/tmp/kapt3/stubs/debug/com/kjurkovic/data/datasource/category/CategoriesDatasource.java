package com.kjurkovic.data.datasource.category;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u001b\u0010\f\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ\u0019\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000eH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ\u0019\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\nH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0016"}, d2 = {"Lcom/kjurkovic/data/datasource/category/CategoriesDatasource;", "Lcom/kjurkovic/category_repo/CategoriesRepository;", "database", "Lcom/kjurkovic/data/database/Database;", "dispatcherProvider", "Lcom/kjurkovic/common/dispatchers/DispatcherProvider;", "(Lcom/kjurkovic/data/database/Database;Lcom/kjurkovic/common/dispatchers/DispatcherProvider;)V", "categories", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/kjurkovic/category_repo/models/CategoryModel;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "category", "id", "", "(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "delete", "", "categoryId", "save", "categoryModel", "(Lcom/kjurkovic/category_repo/models/CategoryModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "category-datasource_debug"})
public final class CategoriesDatasource implements com.kjurkovic.category_repo.CategoriesRepository {
    private final com.kjurkovic.data.database.Database database = null;
    private final com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider = null;
    
    @javax.inject.Inject()
    public CategoriesDatasource(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.database.Database database, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.common.dispatchers.DispatcherProvider dispatcherProvider) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object save(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.category_repo.models.CategoryModel categoryModel, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object categories(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends java.util.List<com.kjurkovic.category_repo.models.CategoryModel>>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object category(long id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.kjurkovic.category_repo.models.CategoryModel> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object delete(long categoryId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}