plugins {
    id("base-android-library")
}

dependencies {
    api(libs.room.runtime)
    implementation(libs.room.ktx)
    implementation(libs.androidx.sqlite)
    implementation(libs.hilt.android)
    implementation(libs.kotlin.coroutines)
    implementation(projects.common.annotations)
    implementation(projects.data.common)
    kapt(libs.room.compiler)
    kapt(libs.hilt.androidCompiler)
}
