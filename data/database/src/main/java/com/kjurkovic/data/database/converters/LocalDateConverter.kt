package com.kjurkovic.data.database.converters

import androidx.room.TypeConverter
import java.time.LocalDate

object LocalDateConverter {
    @TypeConverter
    fun toDate(dateString: String?): LocalDate? =
        if (dateString != null) LocalDate.parse(dateString) else null

    @TypeConverter
    fun toDateString(date: LocalDate?): String? {
        return date?.toString()
    }
}
