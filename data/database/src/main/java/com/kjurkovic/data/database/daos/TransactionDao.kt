package com.kjurkovic.data.database.daos

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.kjurkovic.data.database.entities.TransactionEntity
import com.kjurkovic.data.database.entities.result.TransactionCategoryResult
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate
import java.util.*

@Dao
abstract class TransactionDao : BaseDao<TransactionEntity>() {

    @Transaction
    @Query(
        """
        SELECT * 
            FROM ${TransactionEntity.TABLE_NAME} 
            ORDER BY  ${TransactionEntity.COLUMN_DATE} DESC 
            LIMIT :limit 
            OFFSET :offset
        """
    )
    abstract fun transactions(limit: Int, offset: Int): List<TransactionCategoryResult>

    @Transaction
    @Query(
        """
        SELECT * 
            FROM ${TransactionEntity.TABLE_NAME} 
            WHERE ${TransactionEntity.COLUMN_DATE} >= :fromDate
            ORDER BY  ${TransactionEntity.COLUMN_DATE} DESC
        """
    )
    abstract fun transactions(fromDate: LocalDate): List<TransactionCategoryResult>

    @Transaction
    @Query("SELECT * FROM ${TransactionEntity.TABLE_NAME}")
    abstract fun get(): List<TransactionCategoryResult>

    @Transaction
    @Query("SELECT * FROM ${TransactionEntity.TABLE_NAME} WHERE ${TransactionEntity.COLUMN_ID} = :id")
    abstract fun transaction(id: UUID): Flow<TransactionCategoryResult?>

    @Transaction
    @Query("DELETE FROM ${TransactionEntity.TABLE_NAME} WHERE ${TransactionEntity.COLUMN_ID} = :id")
    abstract fun delete(id: UUID)
}
