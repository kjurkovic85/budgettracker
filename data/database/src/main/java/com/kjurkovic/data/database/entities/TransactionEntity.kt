package com.kjurkovic.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.kjurkovic.data.common.TransactionType
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*

@Entity(
    tableName = TransactionEntity.TABLE_NAME,
    indices = [Index(value = [TransactionEntity.COLUMN_DATE], orders = [Index.Order.DESC])]
)
data class TransactionEntity(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    val id: UUID = UUID.randomUUID(),
    @ColumnInfo(name = COLUMN_DESCRIPTION)
    val description: String,
    @ColumnInfo(name = COLUMN_AMOUNT)
    val amount: BigDecimal,
    @ColumnInfo(name = COLUMN_DATE)
    val date: LocalDate,
    @ColumnInfo(name = COLUMN_TYPE)
    val type: TransactionType,
    @ColumnInfo(name = COLUMN_CATEGORY_ID)
    val categoryId: Long?,
) {
    companion object {
        const val TABLE_NAME = "transactions"
        const val COLUMN_ID = "${TABLE_NAME}_id"
        const val COLUMN_DESCRIPTION = "${TABLE_NAME}_description"
        const val COLUMN_AMOUNT = "${TABLE_NAME}_amount"
        const val COLUMN_DATE = "${TABLE_NAME}_date"
        const val COLUMN_TYPE = "${TABLE_NAME}_type"
        const val COLUMN_CATEGORY_ID = "${TABLE_NAME}_categoryId"
    }
}
