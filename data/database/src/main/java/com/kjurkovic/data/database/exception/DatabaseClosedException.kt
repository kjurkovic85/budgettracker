package com.kjurkovic.data.database.exception

class DatabaseClosedException : Exception("Database is closed")