package com.kjurkovic.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.kjurkovic.data.database.converters.BigDecimalConverter
import com.kjurkovic.data.database.converters.LocalDateConverter
import com.kjurkovic.data.database.entities.CategoryEntity
import com.kjurkovic.data.database.entities.TransactionEntity

@Database(
    entities = [
        CategoryEntity::class,
        TransactionEntity::class,
    ],

    version = 1,
    exportSchema = false,
)
@TypeConverters(
    BigDecimalConverter::class,
    LocalDateConverter::class,
)
abstract class AppDatabase : RoomDatabase(), DaoProvider
