package com.kjurkovic.data.database

import android.content.Context
import androidx.room.Room
import com.kjurkovic.data.database.daos.CategoryDao
import com.kjurkovic.data.database.daos.TransactionDao
import com.kjurkovic.data.database.exception.DatabaseClosedException
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DatabaseInitializer @Inject constructor(
    @ApplicationContext private val context: Context,
) : Database {

    companion object {
        private const val DATABASE_NAME = "app.db"
    }

    private val database = AtomicReference<AppDatabase?>()

    init {
        val roomDatabase = Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
        database.set(roomDatabase)
    }

    override val categoryDao: CategoryDao
        get() = database.get()?.categoryDao ?: throw DatabaseClosedException()

    override val transactionDao: TransactionDao
        get() = database.get()?.transactionDao ?: throw DatabaseClosedException()
}
