package com.kjurkovic.data.database.converters

import androidx.room.TypeConverter
import java.math.BigDecimal

class BigDecimalConverter {
    @TypeConverter
    fun fromTimestamp(value: String?): BigDecimal? {
        return value?.let { BigDecimal(it) }
    }

    @TypeConverter
    fun dateToTimestamp(value: BigDecimal?): String? {
        return value?.toEngineeringString()
    }
}
