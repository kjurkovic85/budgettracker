package com.kjurkovic.data.database

import com.kjurkovic.data.database.daos.CategoryDao
import com.kjurkovic.data.database.daos.TransactionDao

interface DaoProvider {
    val categoryDao: CategoryDao
    val transactionDao: TransactionDao
}