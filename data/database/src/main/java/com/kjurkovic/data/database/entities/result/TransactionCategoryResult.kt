package com.kjurkovic.data.database.entities.result

import androidx.room.Embedded
import androidx.room.Relation
import com.kjurkovic.data.database.entities.CategoryEntity
import com.kjurkovic.data.database.entities.TransactionEntity

data class TransactionCategoryResult(
    @Embedded
    val transactionEntity: TransactionEntity,

    @Relation(
        entity = CategoryEntity::class,
        parentColumn = TransactionEntity.COLUMN_CATEGORY_ID,
        entityColumn = CategoryEntity.COLUMN_ID
    )
    val categoryEntity: CategoryEntity?
)
