package com.kjurkovic.data.database.daos

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.kjurkovic.data.database.entities.CategoryEntity
import kotlinx.coroutines.flow.Flow

@Dao
abstract class CategoryDao : BaseDao<CategoryEntity>() {

    @Transaction
    @Query("SELECT * FROM ${CategoryEntity.TABLE_NAME}")
    abstract fun observeCategories(): Flow<List<CategoryEntity>>

    @Transaction
    @Query("SELECT * FROM ${CategoryEntity.TABLE_NAME} WHERE ${CategoryEntity.COLUMN_ID} = :id")
    abstract fun categoryById(id: Long): CategoryEntity?

    @Transaction
    @Query("DELETE FROM ${CategoryEntity.TABLE_NAME} WHERE ${CategoryEntity.COLUMN_ID} = :id")
    abstract fun delete(id: Long)
}
