package com.kjurkovic.data.database.daos;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\'\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\'J\u0018\u0010\u000b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\n0\f2\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000e\u001a\u00020\u000fH\'J\u001e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\'\u00a8\u0006\u0013"}, d2 = {"Lcom/kjurkovic/data/database/daos/TransactionDao;", "Lcom/kjurkovic/data/database/daos/BaseDao;", "Lcom/kjurkovic/data/database/entities/TransactionEntity;", "()V", "delete", "", "id", "Ljava/util/UUID;", "get", "", "Lcom/kjurkovic/data/database/entities/result/TransactionCategoryResult;", "transaction", "Lkotlinx/coroutines/flow/Flow;", "transactions", "fromDate", "Ljava/time/LocalDate;", "limit", "", "offset", "database_debug"})
public abstract class TransactionDao extends com.kjurkovic.data.database.daos.BaseDao<com.kjurkovic.data.database.entities.TransactionEntity> {
    
    public TransactionDao() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "\n        SELECT * \n            FROM transactions \n            ORDER BY  transactions_date DESC \n            LIMIT :limit \n            OFFSET :offset\n        ")
    @androidx.room.Transaction()
    public abstract java.util.List<com.kjurkovic.data.database.entities.result.TransactionCategoryResult> transactions(int limit, int offset);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "\n        SELECT * \n            FROM transactions \n            WHERE transactions_date >= :fromDate\n            ORDER BY  transactions_date DESC\n        ")
    @androidx.room.Transaction()
    public abstract java.util.List<com.kjurkovic.data.database.entities.result.TransactionCategoryResult> transactions(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate fromDate);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM transactions")
    @androidx.room.Transaction()
    public abstract java.util.List<com.kjurkovic.data.database.entities.result.TransactionCategoryResult> get();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM transactions WHERE transactions_id = :id")
    @androidx.room.Transaction()
    public abstract kotlinx.coroutines.flow.Flow<com.kjurkovic.data.database.entities.result.TransactionCategoryResult> transaction(@org.jetbrains.annotations.NotNull()
    java.util.UUID id);
    
    @androidx.room.Query(value = "DELETE FROM transactions WHERE transactions_id = :id")
    @androidx.room.Transaction()
    public abstract void delete(@org.jetbrains.annotations.NotNull()
    java.util.UUID id);
}