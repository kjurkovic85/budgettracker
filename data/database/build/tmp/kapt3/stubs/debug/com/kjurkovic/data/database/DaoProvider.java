package com.kjurkovic.data.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/kjurkovic/data/database/DaoProvider;", "", "categoryDao", "Lcom/kjurkovic/data/database/daos/CategoryDao;", "getCategoryDao", "()Lcom/kjurkovic/data/database/daos/CategoryDao;", "transactionDao", "Lcom/kjurkovic/data/database/daos/TransactionDao;", "getTransactionDao", "()Lcom/kjurkovic/data/database/daos/TransactionDao;", "database_debug"})
public abstract interface DaoProvider {
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.kjurkovic.data.database.daos.CategoryDao getCategoryDao();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.kjurkovic.data.database.daos.TransactionDao getTransactionDao();
}