package com.kjurkovic.data.database.entities.result;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/kjurkovic/data/database/entities/result/TransactionCategoryResult;", "", "transactionEntity", "Lcom/kjurkovic/data/database/entities/TransactionEntity;", "categoryEntity", "Lcom/kjurkovic/data/database/entities/CategoryEntity;", "(Lcom/kjurkovic/data/database/entities/TransactionEntity;Lcom/kjurkovic/data/database/entities/CategoryEntity;)V", "getCategoryEntity", "()Lcom/kjurkovic/data/database/entities/CategoryEntity;", "getTransactionEntity", "()Lcom/kjurkovic/data/database/entities/TransactionEntity;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "database_debug"})
public final class TransactionCategoryResult {
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Embedded()
    private final com.kjurkovic.data.database.entities.TransactionEntity transactionEntity = null;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Relation(entity = com.kjurkovic.data.database.entities.CategoryEntity.class, parentColumn = "transactions_categoryId", entityColumn = "categories_id")
    private final com.kjurkovic.data.database.entities.CategoryEntity categoryEntity = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.database.entities.result.TransactionCategoryResult copy(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.database.entities.TransactionEntity transactionEntity, @org.jetbrains.annotations.Nullable()
    com.kjurkovic.data.database.entities.CategoryEntity categoryEntity) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public TransactionCategoryResult(@org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.database.entities.TransactionEntity transactionEntity, @org.jetbrains.annotations.Nullable()
    com.kjurkovic.data.database.entities.CategoryEntity categoryEntity) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.database.entities.TransactionEntity component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.database.entities.TransactionEntity getTransactionEntity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.kjurkovic.data.database.entities.CategoryEntity component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.kjurkovic.data.database.entities.CategoryEntity getCategoryEntity() {
        return null;
    }
}