package com.kjurkovic.data.database;

import java.lang.System;

@androidx.room.TypeConverters(value = {com.kjurkovic.data.database.converters.BigDecimalConverter.class, com.kjurkovic.data.database.converters.LocalDateConverter.class})
@androidx.room.Database(entities = {com.kjurkovic.data.database.entities.CategoryEntity.class, com.kjurkovic.data.database.entities.TransactionEntity.class}, version = 1, exportSchema = false)
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"}, d2 = {"Lcom/kjurkovic/data/database/AppDatabase;", "Landroidx/room/RoomDatabase;", "Lcom/kjurkovic/data/database/DaoProvider;", "()V", "database_debug"})
public abstract class AppDatabase extends androidx.room.RoomDatabase implements com.kjurkovic.data.database.DaoProvider {
    
    public AppDatabase() {
        super();
    }
}