package com.kjurkovic.data.database.entities;

import java.lang.System;

@androidx.room.Entity(tableName = "transactions", indices = {@androidx.room.Index(orders = {androidx.room.Index.Order.DESC}, value = {"transactions_date"})})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0087\b\u0018\u0000 *2\u00020\u0001:\u0001*B9\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\tH\u00c6\u0003J\t\u0010 \u001a\u00020\u000bH\u00c6\u0003J\u0010\u0010!\u001a\u0004\u0018\u00010\rH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0012JL\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rH\u00c6\u0001\u00a2\u0006\u0002\u0010#J\u0013\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\t\u0010)\u001a\u00020\u0005H\u00d6\u0001R\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\f\u001a\u0004\u0018\u00010\r8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\b\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0016\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b\u00a8\u0006+"}, d2 = {"Lcom/kjurkovic/data/database/entities/TransactionEntity;", "", "id", "Ljava/util/UUID;", "description", "", "amount", "Ljava/math/BigDecimal;", "date", "Ljava/time/LocalDate;", "type", "Lcom/kjurkovic/data/common/TransactionType;", "categoryId", "", "(Ljava/util/UUID;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/time/LocalDate;Lcom/kjurkovic/data/common/TransactionType;Ljava/lang/Long;)V", "getAmount", "()Ljava/math/BigDecimal;", "getCategoryId", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getDate", "()Ljava/time/LocalDate;", "getDescription", "()Ljava/lang/String;", "getId", "()Ljava/util/UUID;", "getType", "()Lcom/kjurkovic/data/common/TransactionType;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "(Ljava/util/UUID;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/time/LocalDate;Lcom/kjurkovic/data/common/TransactionType;Ljava/lang/Long;)Lcom/kjurkovic/data/database/entities/TransactionEntity;", "equals", "", "other", "hashCode", "", "toString", "Companion", "database_debug"})
public final class TransactionEntity {
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "transactions_id")
    @androidx.room.PrimaryKey()
    private final java.util.UUID id = null;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "transactions_description")
    private final java.lang.String description = null;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "transactions_amount")
    private final java.math.BigDecimal amount = null;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "transactions_date")
    private final java.time.LocalDate date = null;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.ColumnInfo(name = "transactions_type")
    private final com.kjurkovic.data.common.TransactionType type = null;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "transactions_categoryId")
    private final java.lang.Long categoryId = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.data.database.entities.TransactionEntity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TABLE_NAME = "transactions";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_ID = "transactions_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_DESCRIPTION = "transactions_description";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_AMOUNT = "transactions_amount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_DATE = "transactions_date";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_TYPE = "transactions_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COLUMN_CATEGORY_ID = "transactions_categoryId";
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.database.entities.TransactionEntity copy(@org.jetbrains.annotations.NotNull()
    java.util.UUID id, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    java.math.BigDecimal amount, @org.jetbrains.annotations.NotNull()
    java.time.LocalDate date, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.common.TransactionType type, @org.jetbrains.annotations.Nullable()
    java.lang.Long categoryId) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public TransactionEntity(@org.jetbrains.annotations.NotNull()
    java.util.UUID id, @org.jetbrains.annotations.NotNull()
    java.lang.String description, @org.jetbrains.annotations.NotNull()
    java.math.BigDecimal amount, @org.jetbrains.annotations.NotNull()
    java.time.LocalDate date, @org.jetbrains.annotations.NotNull()
    com.kjurkovic.data.common.TransactionType type, @org.jetbrains.annotations.Nullable()
    java.lang.Long categoryId) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.UUID component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.UUID getId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.math.BigDecimal component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.math.BigDecimal getAmount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.LocalDate component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.LocalDate getDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.common.TransactionType component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.kjurkovic.data.common.TransactionType getType() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getCategoryId() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/kjurkovic/data/database/entities/TransactionEntity$Companion;", "", "()V", "COLUMN_AMOUNT", "", "COLUMN_CATEGORY_ID", "COLUMN_DATE", "COLUMN_DESCRIPTION", "COLUMN_ID", "COLUMN_TYPE", "TABLE_NAME", "database_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}