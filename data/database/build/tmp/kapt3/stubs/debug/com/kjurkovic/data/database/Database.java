package com.kjurkovic.data.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001\u00a8\u0006\u0002"}, d2 = {"Lcom/kjurkovic/data/database/Database;", "Lcom/kjurkovic/data/database/DaoProvider;", "database_debug"})
public abstract interface Database extends com.kjurkovic.data.database.DaoProvider {
}