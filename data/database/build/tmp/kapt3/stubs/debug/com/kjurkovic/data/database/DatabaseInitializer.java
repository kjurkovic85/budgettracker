package com.kjurkovic.data.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0011"}, d2 = {"Lcom/kjurkovic/data/database/DatabaseInitializer;", "Lcom/kjurkovic/data/database/Database;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "categoryDao", "Lcom/kjurkovic/data/database/daos/CategoryDao;", "getCategoryDao", "()Lcom/kjurkovic/data/database/daos/CategoryDao;", "database", "Ljava/util/concurrent/atomic/AtomicReference;", "Lcom/kjurkovic/data/database/AppDatabase;", "transactionDao", "Lcom/kjurkovic/data/database/daos/TransactionDao;", "getTransactionDao", "()Lcom/kjurkovic/data/database/daos/TransactionDao;", "Companion", "database_debug"})
@javax.inject.Singleton()
public final class DatabaseInitializer implements com.kjurkovic.data.database.Database {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.kjurkovic.data.database.DatabaseInitializer.Companion Companion = null;
    private static final java.lang.String DATABASE_NAME = "app.db";
    private final java.util.concurrent.atomic.AtomicReference<com.kjurkovic.data.database.AppDatabase> database = null;
    
    @javax.inject.Inject()
    public DatabaseInitializer(@org.jetbrains.annotations.NotNull()
    @dagger.hilt.android.qualifiers.ApplicationContext()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.kjurkovic.data.database.daos.CategoryDao getCategoryDao() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.kjurkovic.data.database.daos.TransactionDao getTransactionDao() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/kjurkovic/data/database/DatabaseInitializer$Companion;", "", "()V", "DATABASE_NAME", "", "database_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}