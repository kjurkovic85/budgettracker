package com.kjurkovic.data.database.daos;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\b\'\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u000b0\nH\'\u00a8\u0006\f"}, d2 = {"Lcom/kjurkovic/data/database/daos/CategoryDao;", "Lcom/kjurkovic/data/database/daos/BaseDao;", "Lcom/kjurkovic/data/database/entities/CategoryEntity;", "()V", "categoryById", "id", "", "delete", "", "observeCategories", "Lkotlinx/coroutines/flow/Flow;", "", "database_debug"})
public abstract class CategoryDao extends com.kjurkovic.data.database.daos.BaseDao<com.kjurkovic.data.database.entities.CategoryEntity> {
    
    public CategoryDao() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM categories")
    @androidx.room.Transaction()
    public abstract kotlinx.coroutines.flow.Flow<java.util.List<com.kjurkovic.data.database.entities.CategoryEntity>> observeCategories();
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "SELECT * FROM categories WHERE categories_id = :id")
    @androidx.room.Transaction()
    public abstract com.kjurkovic.data.database.entities.CategoryEntity categoryById(long id);
    
    @androidx.room.Query(value = "DELETE FROM categories WHERE categories_id = :id")
    @androidx.room.Transaction()
    public abstract void delete(long id);
}