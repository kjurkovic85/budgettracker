package com.kjurkovic.data.database;

import android.content.Context;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DatabaseInitializer_Factory implements Factory<DatabaseInitializer> {
  private final Provider<Context> contextProvider;

  public DatabaseInitializer_Factory(Provider<Context> contextProvider) {
    this.contextProvider = contextProvider;
  }

  @Override
  public DatabaseInitializer get() {
    return newInstance(contextProvider.get());
  }

  public static DatabaseInitializer_Factory create(Provider<Context> contextProvider) {
    return new DatabaseInitializer_Factory(contextProvider);
  }

  public static DatabaseInitializer newInstance(Context context) {
    return new DatabaseInitializer(context);
  }
}
