package com.kjurkovic.data.database.daos;

import android.database.Cursor;
import androidx.collection.LongSparseArray;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomDatabaseKt;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.room.util.StringUtil;
import androidx.room.util.UUIDUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.kjurkovic.data.common.TransactionType;
import com.kjurkovic.data.database.converters.BigDecimalConverter;
import com.kjurkovic.data.database.converters.LocalDateConverter;
import com.kjurkovic.data.database.entities.CategoryEntity;
import com.kjurkovic.data.database.entities.TransactionEntity;
import com.kjurkovic.data.database.entities.result.TransactionCategoryResult;
import java.lang.Class;
import java.lang.Exception;
import java.lang.IllegalArgumentException;
import java.lang.Long;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.StringBuilder;
import java.lang.SuppressWarnings;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import javax.annotation.processing.Generated;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.flow.Flow;

@Generated("androidx.room.RoomProcessor")
@SuppressWarnings({"unchecked", "deprecation"})
public final class TransactionDao_Impl extends TransactionDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<TransactionEntity> __insertionAdapterOfTransactionEntity;

  private final BigDecimalConverter __bigDecimalConverter = new BigDecimalConverter();

  private final EntityDeletionOrUpdateAdapter<TransactionEntity> __updateAdapterOfTransactionEntity;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  public TransactionDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfTransactionEntity = new EntityInsertionAdapter<TransactionEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `transactions` (`transactions_id`,`transactions_description`,`transactions_amount`,`transactions_date`,`transactions_type`,`transactions_categoryId`) VALUES (?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TransactionEntity value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindBlob(1, UUIDUtil.convertUUIDToByte(value.getId()));
        }
        if (value.getDescription() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDescription());
        }
        final String _tmp = __bigDecimalConverter.dateToTimestamp(value.getAmount());
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, _tmp);
        }
        final String _tmp_1 = LocalDateConverter.INSTANCE.toDateString(value.getDate());
        if (_tmp_1 == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, _tmp_1);
        }
        if (value.getType() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, __TransactionType_enumToString(value.getType()));
        }
        if (value.getCategoryId() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindLong(6, value.getCategoryId());
        }
      }
    };
    this.__updateAdapterOfTransactionEntity = new EntityDeletionOrUpdateAdapter<TransactionEntity>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `transactions` SET `transactions_id` = ?,`transactions_description` = ?,`transactions_amount` = ?,`transactions_date` = ?,`transactions_type` = ?,`transactions_categoryId` = ? WHERE `transactions_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, TransactionEntity value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindBlob(1, UUIDUtil.convertUUIDToByte(value.getId()));
        }
        if (value.getDescription() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDescription());
        }
        final String _tmp = __bigDecimalConverter.dateToTimestamp(value.getAmount());
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, _tmp);
        }
        final String _tmp_1 = LocalDateConverter.INSTANCE.toDateString(value.getDate());
        if (_tmp_1 == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, _tmp_1);
        }
        if (value.getType() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, __TransactionType_enumToString(value.getType()));
        }
        if (value.getCategoryId() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindLong(6, value.getCategoryId());
        }
        if (value.getId() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindBlob(7, UUIDUtil.convertUUIDToByte(value.getId()));
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM transactions WHERE transactions_id = ?";
        return _query;
      }
    };
  }

  @Override
  public long insert(final TransactionEntity obj) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfTransactionEntity.insertAndReturnId(obj);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public Object insert(final List<? extends TransactionEntity> obj,
      final Continuation<? super List<Long>> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<List<Long>>() {
      @Override
      public List<Long> call() throws Exception {
        __db.beginTransaction();
        try {
          List<Long> _result = __insertionAdapterOfTransactionEntity.insertAndReturnIdsList(obj);
          __db.setTransactionSuccessful();
          return _result;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object update(final TransactionEntity obj, final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __updateAdapterOfTransactionEntity.handle(obj);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object update(final List<? extends TransactionEntity> obj,
      final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __updateAdapterOfTransactionEntity.handleMultiple(obj);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object upsert(final TransactionEntity obj, final Continuation<? super Unit> continuation) {
    return RoomDatabaseKt.withTransaction(__db, new Function1<Continuation<? super Unit>, Object>() {
      @Override
      public Object invoke(Continuation<? super Unit> __cont) {
        return TransactionDao_Impl.super.upsert(obj, __cont);
      }
    }, continuation);
  }

  @Override
  public Object upsert(final List<? extends TransactionEntity> objList,
      final Continuation<? super Unit> continuation) {
    return RoomDatabaseKt.withTransaction(__db, new Function1<Continuation<? super Unit>, Object>() {
      @Override
      public Object invoke(Continuation<? super Unit> __cont) {
        return TransactionDao_Impl.super.upsert(objList, __cont);
      }
    }, continuation);
  }

  @Override
  public void delete(final UUID id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    int _argIndex = 1;
    if (id == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindBlob(_argIndex, UUIDUtil.convertUUIDToByte(id));
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public List<TransactionCategoryResult> transactions(final int limit, final int offset) {
    final String _sql = "\n"
            + "        SELECT * \n"
            + "            FROM transactions \n"
            + "            ORDER BY  transactions_date DESC \n"
            + "            LIMIT ? \n"
            + "            OFFSET ?\n"
            + "        ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, limit);
    _argIndex = 2;
    _statement.bindLong(_argIndex, offset);
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      final Cursor _cursor = DBUtil.query(__db, _statement, true, null);
      try {
        final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_id");
        final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_description");
        final int _cursorIndexOfAmount = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_amount");
        final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_date");
        final int _cursorIndexOfType = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_type");
        final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_categoryId");
        final LongSparseArray<CategoryEntity> _collectionCategoryEntity = new LongSparseArray<CategoryEntity>();
        while (_cursor.moveToNext()) {
          if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
            final long _tmpKey = _cursor.getLong(_cursorIndexOfCategoryId);
            _collectionCategoryEntity.put(_tmpKey, null);
          }
        }
        _cursor.moveToPosition(-1);
        __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(_collectionCategoryEntity);
        final List<TransactionCategoryResult> _result = new ArrayList<TransactionCategoryResult>(_cursor.getCount());
        while(_cursor.moveToNext()) {
          final TransactionCategoryResult _item;
          final TransactionEntity _tmpTransactionEntity;
          if (! (_cursor.isNull(_cursorIndexOfId) && _cursor.isNull(_cursorIndexOfDescription) && _cursor.isNull(_cursorIndexOfAmount) && _cursor.isNull(_cursorIndexOfDate) && _cursor.isNull(_cursorIndexOfType) && _cursor.isNull(_cursorIndexOfCategoryId))) {
            final UUID _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = UUIDUtil.convertByteToUUID(_cursor.getBlob(_cursorIndexOfId));
            }
            final String _tmpDescription;
            if (_cursor.isNull(_cursorIndexOfDescription)) {
              _tmpDescription = null;
            } else {
              _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            }
            final BigDecimal _tmpAmount;
            final String _tmp;
            if (_cursor.isNull(_cursorIndexOfAmount)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getString(_cursorIndexOfAmount);
            }
            _tmpAmount = __bigDecimalConverter.fromTimestamp(_tmp);
            final LocalDate _tmpDate;
            final String _tmp_1;
            if (_cursor.isNull(_cursorIndexOfDate)) {
              _tmp_1 = null;
            } else {
              _tmp_1 = _cursor.getString(_cursorIndexOfDate);
            }
            _tmpDate = LocalDateConverter.INSTANCE.toDate(_tmp_1);
            final TransactionType _tmpType;
            _tmpType = __TransactionType_stringToEnum(_cursor.getString(_cursorIndexOfType));
            final Long _tmpCategoryId;
            if (_cursor.isNull(_cursorIndexOfCategoryId)) {
              _tmpCategoryId = null;
            } else {
              _tmpCategoryId = _cursor.getLong(_cursorIndexOfCategoryId);
            }
            _tmpTransactionEntity = new TransactionEntity(_tmpId,_tmpDescription,_tmpAmount,_tmpDate,_tmpType,_tmpCategoryId);
          }  else  {
            _tmpTransactionEntity = null;
          }
          CategoryEntity _tmpCategoryEntity = null;
          if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
            final long _tmpKey_1 = _cursor.getLong(_cursorIndexOfCategoryId);
            _tmpCategoryEntity = _collectionCategoryEntity.get(_tmpKey_1);
          }
          _item = new TransactionCategoryResult(_tmpTransactionEntity,_tmpCategoryEntity);
          _result.add(_item);
        }
        __db.setTransactionSuccessful();
        return _result;
      } finally {
        _cursor.close();
        _statement.release();
      }
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<TransactionCategoryResult> transactions(final LocalDate fromDate) {
    final String _sql = "\n"
            + "        SELECT * \n"
            + "            FROM transactions \n"
            + "            WHERE transactions_date >= ?\n"
            + "            ORDER BY  transactions_date DESC\n"
            + "        ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    final String _tmp = LocalDateConverter.INSTANCE.toDateString(fromDate);
    if (_tmp == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, _tmp);
    }
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      final Cursor _cursor = DBUtil.query(__db, _statement, true, null);
      try {
        final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_id");
        final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_description");
        final int _cursorIndexOfAmount = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_amount");
        final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_date");
        final int _cursorIndexOfType = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_type");
        final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_categoryId");
        final LongSparseArray<CategoryEntity> _collectionCategoryEntity = new LongSparseArray<CategoryEntity>();
        while (_cursor.moveToNext()) {
          if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
            final long _tmpKey = _cursor.getLong(_cursorIndexOfCategoryId);
            _collectionCategoryEntity.put(_tmpKey, null);
          }
        }
        _cursor.moveToPosition(-1);
        __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(_collectionCategoryEntity);
        final List<TransactionCategoryResult> _result = new ArrayList<TransactionCategoryResult>(_cursor.getCount());
        while(_cursor.moveToNext()) {
          final TransactionCategoryResult _item;
          final TransactionEntity _tmpTransactionEntity;
          if (! (_cursor.isNull(_cursorIndexOfId) && _cursor.isNull(_cursorIndexOfDescription) && _cursor.isNull(_cursorIndexOfAmount) && _cursor.isNull(_cursorIndexOfDate) && _cursor.isNull(_cursorIndexOfType) && _cursor.isNull(_cursorIndexOfCategoryId))) {
            final UUID _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = UUIDUtil.convertByteToUUID(_cursor.getBlob(_cursorIndexOfId));
            }
            final String _tmpDescription;
            if (_cursor.isNull(_cursorIndexOfDescription)) {
              _tmpDescription = null;
            } else {
              _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            }
            final BigDecimal _tmpAmount;
            final String _tmp_1;
            if (_cursor.isNull(_cursorIndexOfAmount)) {
              _tmp_1 = null;
            } else {
              _tmp_1 = _cursor.getString(_cursorIndexOfAmount);
            }
            _tmpAmount = __bigDecimalConverter.fromTimestamp(_tmp_1);
            final LocalDate _tmpDate;
            final String _tmp_2;
            if (_cursor.isNull(_cursorIndexOfDate)) {
              _tmp_2 = null;
            } else {
              _tmp_2 = _cursor.getString(_cursorIndexOfDate);
            }
            _tmpDate = LocalDateConverter.INSTANCE.toDate(_tmp_2);
            final TransactionType _tmpType;
            _tmpType = __TransactionType_stringToEnum(_cursor.getString(_cursorIndexOfType));
            final Long _tmpCategoryId;
            if (_cursor.isNull(_cursorIndexOfCategoryId)) {
              _tmpCategoryId = null;
            } else {
              _tmpCategoryId = _cursor.getLong(_cursorIndexOfCategoryId);
            }
            _tmpTransactionEntity = new TransactionEntity(_tmpId,_tmpDescription,_tmpAmount,_tmpDate,_tmpType,_tmpCategoryId);
          }  else  {
            _tmpTransactionEntity = null;
          }
          CategoryEntity _tmpCategoryEntity = null;
          if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
            final long _tmpKey_1 = _cursor.getLong(_cursorIndexOfCategoryId);
            _tmpCategoryEntity = _collectionCategoryEntity.get(_tmpKey_1);
          }
          _item = new TransactionCategoryResult(_tmpTransactionEntity,_tmpCategoryEntity);
          _result.add(_item);
        }
        __db.setTransactionSuccessful();
        return _result;
      } finally {
        _cursor.close();
        _statement.release();
      }
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<TransactionCategoryResult> get() {
    final String _sql = "SELECT * FROM transactions";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      final Cursor _cursor = DBUtil.query(__db, _statement, true, null);
      try {
        final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_id");
        final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_description");
        final int _cursorIndexOfAmount = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_amount");
        final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_date");
        final int _cursorIndexOfType = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_type");
        final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_categoryId");
        final LongSparseArray<CategoryEntity> _collectionCategoryEntity = new LongSparseArray<CategoryEntity>();
        while (_cursor.moveToNext()) {
          if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
            final long _tmpKey = _cursor.getLong(_cursorIndexOfCategoryId);
            _collectionCategoryEntity.put(_tmpKey, null);
          }
        }
        _cursor.moveToPosition(-1);
        __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(_collectionCategoryEntity);
        final List<TransactionCategoryResult> _result = new ArrayList<TransactionCategoryResult>(_cursor.getCount());
        while(_cursor.moveToNext()) {
          final TransactionCategoryResult _item;
          final TransactionEntity _tmpTransactionEntity;
          if (! (_cursor.isNull(_cursorIndexOfId) && _cursor.isNull(_cursorIndexOfDescription) && _cursor.isNull(_cursorIndexOfAmount) && _cursor.isNull(_cursorIndexOfDate) && _cursor.isNull(_cursorIndexOfType) && _cursor.isNull(_cursorIndexOfCategoryId))) {
            final UUID _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = UUIDUtil.convertByteToUUID(_cursor.getBlob(_cursorIndexOfId));
            }
            final String _tmpDescription;
            if (_cursor.isNull(_cursorIndexOfDescription)) {
              _tmpDescription = null;
            } else {
              _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            }
            final BigDecimal _tmpAmount;
            final String _tmp;
            if (_cursor.isNull(_cursorIndexOfAmount)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getString(_cursorIndexOfAmount);
            }
            _tmpAmount = __bigDecimalConverter.fromTimestamp(_tmp);
            final LocalDate _tmpDate;
            final String _tmp_1;
            if (_cursor.isNull(_cursorIndexOfDate)) {
              _tmp_1 = null;
            } else {
              _tmp_1 = _cursor.getString(_cursorIndexOfDate);
            }
            _tmpDate = LocalDateConverter.INSTANCE.toDate(_tmp_1);
            final TransactionType _tmpType;
            _tmpType = __TransactionType_stringToEnum(_cursor.getString(_cursorIndexOfType));
            final Long _tmpCategoryId;
            if (_cursor.isNull(_cursorIndexOfCategoryId)) {
              _tmpCategoryId = null;
            } else {
              _tmpCategoryId = _cursor.getLong(_cursorIndexOfCategoryId);
            }
            _tmpTransactionEntity = new TransactionEntity(_tmpId,_tmpDescription,_tmpAmount,_tmpDate,_tmpType,_tmpCategoryId);
          }  else  {
            _tmpTransactionEntity = null;
          }
          CategoryEntity _tmpCategoryEntity = null;
          if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
            final long _tmpKey_1 = _cursor.getLong(_cursorIndexOfCategoryId);
            _tmpCategoryEntity = _collectionCategoryEntity.get(_tmpKey_1);
          }
          _item = new TransactionCategoryResult(_tmpTransactionEntity,_tmpCategoryEntity);
          _result.add(_item);
        }
        __db.setTransactionSuccessful();
        return _result;
      } finally {
        _cursor.close();
        _statement.release();
      }
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public Flow<TransactionCategoryResult> transaction(final UUID id) {
    final String _sql = "SELECT * FROM transactions WHERE transactions_id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindBlob(_argIndex, UUIDUtil.convertUUIDToByte(id));
    }
    return CoroutinesRoom.createFlow(__db, true, new String[]{"categories","transactions"}, new Callable<TransactionCategoryResult>() {
      @Override
      public TransactionCategoryResult call() throws Exception {
        __db.beginTransaction();
        try {
          final Cursor _cursor = DBUtil.query(__db, _statement, true, null);
          try {
            final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_id");
            final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_description");
            final int _cursorIndexOfAmount = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_amount");
            final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_date");
            final int _cursorIndexOfType = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_type");
            final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "transactions_categoryId");
            final LongSparseArray<CategoryEntity> _collectionCategoryEntity = new LongSparseArray<CategoryEntity>();
            while (_cursor.moveToNext()) {
              if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
                final long _tmpKey = _cursor.getLong(_cursorIndexOfCategoryId);
                _collectionCategoryEntity.put(_tmpKey, null);
              }
            }
            _cursor.moveToPosition(-1);
            __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(_collectionCategoryEntity);
            final TransactionCategoryResult _result;
            if(_cursor.moveToFirst()) {
              final TransactionEntity _tmpTransactionEntity;
              if (! (_cursor.isNull(_cursorIndexOfId) && _cursor.isNull(_cursorIndexOfDescription) && _cursor.isNull(_cursorIndexOfAmount) && _cursor.isNull(_cursorIndexOfDate) && _cursor.isNull(_cursorIndexOfType) && _cursor.isNull(_cursorIndexOfCategoryId))) {
                final UUID _tmpId;
                if (_cursor.isNull(_cursorIndexOfId)) {
                  _tmpId = null;
                } else {
                  _tmpId = UUIDUtil.convertByteToUUID(_cursor.getBlob(_cursorIndexOfId));
                }
                final String _tmpDescription;
                if (_cursor.isNull(_cursorIndexOfDescription)) {
                  _tmpDescription = null;
                } else {
                  _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
                }
                final BigDecimal _tmpAmount;
                final String _tmp;
                if (_cursor.isNull(_cursorIndexOfAmount)) {
                  _tmp = null;
                } else {
                  _tmp = _cursor.getString(_cursorIndexOfAmount);
                }
                _tmpAmount = __bigDecimalConverter.fromTimestamp(_tmp);
                final LocalDate _tmpDate;
                final String _tmp_1;
                if (_cursor.isNull(_cursorIndexOfDate)) {
                  _tmp_1 = null;
                } else {
                  _tmp_1 = _cursor.getString(_cursorIndexOfDate);
                }
                _tmpDate = LocalDateConverter.INSTANCE.toDate(_tmp_1);
                final TransactionType _tmpType;
                _tmpType = __TransactionType_stringToEnum(_cursor.getString(_cursorIndexOfType));
                final Long _tmpCategoryId;
                if (_cursor.isNull(_cursorIndexOfCategoryId)) {
                  _tmpCategoryId = null;
                } else {
                  _tmpCategoryId = _cursor.getLong(_cursorIndexOfCategoryId);
                }
                _tmpTransactionEntity = new TransactionEntity(_tmpId,_tmpDescription,_tmpAmount,_tmpDate,_tmpType,_tmpCategoryId);
              }  else  {
                _tmpTransactionEntity = null;
              }
              CategoryEntity _tmpCategoryEntity = null;
              if (!_cursor.isNull(_cursorIndexOfCategoryId)) {
                final long _tmpKey_1 = _cursor.getLong(_cursorIndexOfCategoryId);
                _tmpCategoryEntity = _collectionCategoryEntity.get(_tmpKey_1);
              }
              _result = new TransactionCategoryResult(_tmpTransactionEntity,_tmpCategoryEntity);
            } else {
              _result = null;
            }
            __db.setTransactionSuccessful();
            return _result;
          } finally {
            _cursor.close();
          }
        } finally {
          __db.endTransaction();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }

  private String __TransactionType_enumToString(final TransactionType _value) {
    if (_value == null) {
      return null;
    } switch (_value) {
      case INCOME: return "INCOME";
      case EXPENSE: return "EXPENSE";
      default: throw new IllegalArgumentException("Can't convert enum to string, unknown enum value: " + _value);
    }
  }

  private void __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(
      final LongSparseArray<CategoryEntity> _map) {
    if (_map.isEmpty()) {
      return;
    }
    // check if the size is too big, if so divide;
    if(_map.size() > RoomDatabase.MAX_BIND_PARAMETER_CNT) {
      LongSparseArray<CategoryEntity> _tmpInnerMap = new LongSparseArray<CategoryEntity>(androidx.room.RoomDatabase.MAX_BIND_PARAMETER_CNT);
      int _tmpIndex = 0;
      int _mapIndex = 0;
      final int _limit = _map.size();
      while(_mapIndex < _limit) {
        _tmpInnerMap.put(_map.keyAt(_mapIndex), null);
        _mapIndex++;
        _tmpIndex++;
        if(_tmpIndex == RoomDatabase.MAX_BIND_PARAMETER_CNT) {
          __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(_tmpInnerMap);
          _map.putAll(_tmpInnerMap);
          _tmpInnerMap = new LongSparseArray<CategoryEntity>(RoomDatabase.MAX_BIND_PARAMETER_CNT);
          _tmpIndex = 0;
        }
      }
      if(_tmpIndex > 0) {
        __fetchRelationshipcategoriesAscomKjurkovicDataDatabaseEntitiesCategoryEntity(_tmpInnerMap);
        _map.putAll(_tmpInnerMap);
      }
      return;
    }
    StringBuilder _stringBuilder = StringUtil.newStringBuilder();
    _stringBuilder.append("SELECT `categories_id`,`categories_name` FROM `categories` WHERE `categories_id` IN (");
    final int _inputSize = _map.size();
    StringUtil.appendPlaceholders(_stringBuilder, _inputSize);
    _stringBuilder.append(")");
    final String _sql = _stringBuilder.toString();
    final int _argCount = 0 + _inputSize;
    final RoomSQLiteQuery _stmt = RoomSQLiteQuery.acquire(_sql, _argCount);
    int _argIndex = 1;
    for (int i = 0; i < _map.size(); i++) {
      long _item = _map.keyAt(i);
      _stmt.bindLong(_argIndex, _item);
      _argIndex ++;
    }
    final Cursor _cursor = DBUtil.query(__db, _stmt, false, null);
    try {
      final int _itemKeyIndex = CursorUtil.getColumnIndex(_cursor, "categories_id");
      if (_itemKeyIndex == -1) {
        return;
      }
      final int _cursorIndexOfId = 0;
      final int _cursorIndexOfName = 1;
      while(_cursor.moveToNext()) {
        if (!_cursor.isNull(_itemKeyIndex)) {
          final long _tmpKey = _cursor.getLong(_itemKeyIndex);
          if (_map.containsKey(_tmpKey)) {
            final CategoryEntity _item_1;
            final Long _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getLong(_cursorIndexOfId);
            }
            final String _tmpName;
            if (_cursor.isNull(_cursorIndexOfName)) {
              _tmpName = null;
            } else {
              _tmpName = _cursor.getString(_cursorIndexOfName);
            }
            _item_1 = new CategoryEntity(_tmpId,_tmpName);
            _map.put(_tmpKey, _item_1);
          }
        }
      }
    } finally {
      _cursor.close();
    }
  }

  private TransactionType __TransactionType_stringToEnum(final String _value) {
    if (_value == null) {
      return null;
    } switch (_value) {
      case "INCOME": return TransactionType.INCOME;
      case "EXPENSE": return TransactionType.EXPENSE;
      default: throw new IllegalArgumentException("Can't convert value to enum, unknown value: " + _value);
    }
  }
}
