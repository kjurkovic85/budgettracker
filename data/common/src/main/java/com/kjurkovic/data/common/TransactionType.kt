package com.kjurkovic.data.common

enum class TransactionType {
    INCOME, EXPENSE
}