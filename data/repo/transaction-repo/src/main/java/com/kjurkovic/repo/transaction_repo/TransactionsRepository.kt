package com.kjurkovic.repo.transaction_repo

import com.kjurkovic.repo.transaction_repo.model.TransactionModel
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal
import java.util.*

interface TransactionsRepository {
    suspend fun transactions(limit: Int, page: Int): List<TransactionModel>
    suspend fun transactions(): List<TransactionModel>
    suspend fun transaction(id: UUID): Flow<TransactionModel>
    suspend fun balance(): BigDecimal
    suspend fun delete(id: UUID)
    suspend fun save(transaction: TransactionModel)
}
