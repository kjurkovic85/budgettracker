package com.kjurkovic.repo.transaction_repo.model

import com.kjurkovic.category_repo.models.CategoryModel
import com.kjurkovic.data.common.TransactionType
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*

data class TransactionModel(
    val id: UUID? = null,
    val description: String,
    val amount: BigDecimal,
    val date: LocalDate,
    val category: CategoryModel?,
    val type: TransactionType
)
