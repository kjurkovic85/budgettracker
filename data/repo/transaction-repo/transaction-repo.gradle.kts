plugins {
    id("base-android-library")
}

dependencies {
    api(projects.data.common)
    api(projects.data.repo.categoryRepo)

    implementation(libs.kotlin.coroutines)
}
