/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.kjurkovic.category_repo;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.kjurkovic.category_repo";
  public static final String BUILD_TYPE = "debug";
}
