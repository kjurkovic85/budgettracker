package com.kjurkovic.category_repo

import com.kjurkovic.category_repo.models.CategoryModel
import kotlinx.coroutines.flow.Flow

interface CategoriesRepository {
    suspend fun save(categoryModel: CategoryModel)
    suspend fun categories(): Flow<List<CategoryModel>>
    suspend fun category(id: Long): CategoryModel?
    suspend fun delete(categoryId: Long)
}
