package com.kjurkovic.category_repo.models

data class CategoryModel(
    val id: Long?,
    val name: String,
)
