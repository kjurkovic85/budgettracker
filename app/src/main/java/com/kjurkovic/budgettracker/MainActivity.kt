package com.kjurkovic.budgettracker

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import com.kjurkovic.budgettracker.screen.MainScreen
import com.kjurkovic.budgettracker.screen.MainViewModel
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainViewModelFactory: MainViewModel.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            val mainViewModel = MainViewModel.create(
                assistedFactory = mainViewModelFactory,
            )
            MainScreen(mainViewModel)
        }
    }
}