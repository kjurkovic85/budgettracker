package com.kjurkovic.budgettracker.screen

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.google.accompanist.insets.ProvideWindowInsets
import com.kjurkovic.common.framework.navigation.NavigationIntent
import com.kjurkovic.resources.theme.AppTheme
import kotlinx.coroutines.flow.SharedFlow
import timber.log.Timber

@Composable
fun MainScreen(viewModel: MainViewModel) =
    viewModel.Screen { viewState, _, _ ->

        val navController = rememberNavController()

        MainScreen(
            viewState = viewState,
            navController = navController,
        )

        MainNavigationIntents(
            navigationChannel = viewModel.navigationChannel,
            navController = navController,
        )
    }

@Composable
private fun MainScreen(
    viewState: MainViewState,
    navController: NavHostController,
) {
    AppTheme {
        ProvideWindowInsets {
            NavHost(
                navController = navController,
                startDestination = viewState.startDestination,
            ) {
                viewState.navigationFactories.forEach { factory ->
                    factory.create(this, navController)
                }
            }
        }
    }
}

@Composable
private fun MainNavigationIntents(
    navigationChannel: SharedFlow<NavigationIntent>,
    navController: NavHostController,
) {
    val activity = (LocalContext.current as? Activity)

    LaunchedEffect(activity, navController, navigationChannel) {
        Timber.d("LaunchedEffect: $navController $navigationChannel")
        navigationChannel.collect { intent ->
            Timber.d("NavigatorIntent: $intent $navController")
            when (intent) {
                is NavigationIntent.NavigateBack -> {
                    if (intent.route != null) {
                        navController.popBackStack(intent.route!!, intent.inclusive)
                    } else {
                        navController.popBackStack()
                    }
                }
                is NavigationIntent.NavigateTo -> {
                    navController.navigate(intent.route) {
                        launchSingleTop = intent.isSingleTop
                        intent.popUpToRoute?.let { popUpToRoute ->
                            popUpTo(popUpToRoute) { inclusive = intent.inclusive }
                        }
                    }
                }
                is NavigationIntent.CloseApp -> {
                    activity?.finish()
                }
            }
        }
    }
}
