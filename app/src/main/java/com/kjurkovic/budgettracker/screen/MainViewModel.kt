package com.kjurkovic.budgettracker.screen

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import com.kjurkovic.common.dispatchers.DispatcherProvider
import com.kjurkovic.common.framework.mvi.ComponentViewModel
import com.kjurkovic.common.framework.navigation.Destination
import com.kjurkovic.common.framework.navigation.NavigationFactory
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class MainViewModel @AssistedInject constructor(
    navigator: ScreenNavigator,
    dispatcherProvider: DispatcherProvider,
    navigationFactories: @JvmSuppressWildcards Set<NavigationFactory>,
) : ComponentViewModel<MainViewState, Void, Void>(
    navigator = navigator,
    dispatcherProvider = dispatcherProvider,
    viewState = MainViewState(
        navigationFactories = navigationFactories,
        startDestination = Destination.Dashboard.fullRoute
    )
) {

    @AssistedFactory
    interface Factory {
        fun create(): MainViewModel
    }

    companion object {
        @Composable
        fun create(
            assistedFactory: Factory,
        ): MainViewModel = viewModel(factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create() as T
            }
        })
    }

    internal val navigationChannel = navigator.navigationChannel

    override suspend fun reduce(intent: Void) {
        // no-op
    }

}