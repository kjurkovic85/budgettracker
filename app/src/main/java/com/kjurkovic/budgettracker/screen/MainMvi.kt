package com.kjurkovic.budgettracker.screen

import com.kjurkovic.common.framework.navigation.NavigationFactory

data class MainViewState(
    val navigationFactories: Set<NavigationFactory> = emptySet(),
    val startDestination: String = "",
)
