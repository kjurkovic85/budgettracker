package com.kjurkovic.budgettracker.navigation

import com.kjurkovic.common.framework.navigation.NavigationIntent
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.withContext

class AppNavigator: ScreenNavigator {

    private val _navigationChannel = MutableSharedFlow<NavigationIntent>(
        extraBufferCapacity = Int.MAX_VALUE,
        onBufferOverflow = BufferOverflow.DROP_LATEST,
    )

    override val navigationChannel: SharedFlow<NavigationIntent> = _navigationChannel.asSharedFlow()

    override suspend fun navigateBack(
        route: String?,
        inclusive: Boolean,
    ) = withContext(Dispatchers.Main) {
        _navigationChannel.emit(NavigationIntent.NavigateBack(
            route = route,
            inclusive = inclusive
        ))
    }

    override fun tryNavigateBack(
        route: String?,
        inclusive: Boolean,
    ) {
        _navigationChannel.tryEmit(NavigationIntent.NavigateBack(
            route = route,
            inclusive = inclusive
        ))
    }

    override suspend fun navigateTo(
        route: String,
        popUpToRoute: String?,
        inclusive: Boolean,
        isSingleTop: Boolean,
    ) = withContext(Dispatchers.Main) {
        _navigationChannel.emit(NavigationIntent.NavigateTo(
            route = route,
            popUpToRoute = popUpToRoute,
            inclusive = inclusive,
            isSingleTop = isSingleTop,
        ))
    }

    override fun tryNavigateTo(
        route: String,
        popUpToRoute: String?,
        inclusive: Boolean,
        isSingleTop: Boolean,
    ) {
        _navigationChannel.tryEmit(NavigationIntent.NavigateTo(
            route = route,
            popUpToRoute = popUpToRoute,
            inclusive = inclusive,
            isSingleTop = isSingleTop,
        ))
    }

    override suspend fun closeApp() = withContext(Dispatchers.Main) {
        _navigationChannel.emit(NavigationIntent.CloseApp)
    }
}