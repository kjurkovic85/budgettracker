package com.kjurkovic.budgettracker.di

import android.app.Application
import android.content.res.Resources
import com.kjurkovic.annotations.qualifiers.ApplicationId
import com.kjurkovic.annotations.qualifiers.DebugBuild
import com.kjurkovic.budgettracker.BuildConfig
import com.kjurkovic.budgettracker.navigation.AppNavigator
import com.kjurkovic.common.framework.navigation.ScreenNavigator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

    @Provides
    @Singleton
    fun provideNavigator(): ScreenNavigator = AppNavigator()

    @Provides
    @ApplicationId
    fun provideApplicationId(application: Application): String = application.packageName

    @Provides
    @DebugBuild
    fun provideIsDebugBuild(): Boolean = BuildConfig.DEBUG

    @Provides
    fun provideResources(application: Application): Resources = application.resources
}