import com.kjurkovic.app

plugins {
    id("com.android.application")
    id("dagger.hilt.android.plugin")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdk = app.compileSdk
    defaultConfig {
        applicationId = app.applicationId
        minSdk = app.minSdk
        targetSdk = app.targetSdk
        versionCode = app.versionCode
        versionName = app.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        debug {
            isDebuggable = true
        }
    }
    compileOptions {
//        isCoreLibraryDesugaringEnabled = true

        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs = listOf("-Xopt-in=kotlin.RequiresOptIn")
    }

    buildFeatures {
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.compose.get()
    }
}

hilt {
    enableAggregatingTask = true
}

dependencies {

    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.core.splashscreen)
    implementation(libs.androidx.navigation.navigationCompose)
    implementation(libs.bundles.accompanist)
    implementation(libs.material)
    implementation(libs.hilt.android)
    implementation(libs.timber)
    implementation(libs.kotlin.core)
    implementation(libs.kotlin.reflect)

    implementation(projects.common.annotations)
    implementation(projects.common.framework.initializers)
    implementation(projects.common.framework.mvi)
    implementation(projects.common.framework.navigation)
    implementation(projects.common.resources)
    implementation(projects.common.ui)
    implementation(projects.common.wrappers.dispatchers)
    implementation(projects.common.wrappers.logger)

    implementation(projects.data.database)
    implementation(projects.data.datasource.categoryDatasource)
    implementation(projects.data.datasource.transactionDatasource)

    implementation(projects.screens.dashboard)

    kapt(libs.hilt.compiler)
    kapt(libs.hilt.androidCompiler)
}