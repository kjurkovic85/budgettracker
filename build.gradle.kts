// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
        @Suppress("UnstableApiUsage")
        val libs = project.extensions.getByType<VersionCatalogsExtension>().named("libs") as org.gradle.accessors.dm.LibrariesForLibs
        classpath("dev.zacsweers:kgp-160-patcher:1.0.0") // TODO Remove after updating compose and gradlePlugins.kotlin versions

        classpath(libs.gradlePlugins.android)
        classpath(libs.gradlePlugins.hilt)
        classpath(libs.gradlePlugins.kotlin)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
