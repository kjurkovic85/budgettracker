@file:Suppress("UnstableApiUsage")

val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")

plugins {
    `kotlin-dsl`
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}

repositories {
    google()
    mavenCentral()
}

dependencies{
    implementation(libs.findDependency("gradlePlugins.android").get())
    implementation(libs.findDependency("kotlin.core").get())
    implementation(libs.findDependency("kotlin.reflect").get())
    implementation(libs.findDependency("gradlePlugins.kotlin").get())
}
