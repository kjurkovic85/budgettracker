@file:Suppress("UnstableApiUsage")

val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")

plugins {
    id("base-android-library")
}

android {
    buildFeatures {
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = libs.findVersion("compose").get().displayName
    }
}
