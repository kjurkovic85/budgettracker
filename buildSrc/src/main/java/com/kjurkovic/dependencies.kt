@file:Suppress("unused", "ClassName", "MemberVisibilityCanBePrivate")

package com.kjurkovic

object app {
    const val applicationId = "com.kjurkovic.budgettracker"

    const val compileSdk = 31
    const val minSdk = 26
    const val targetSdk = compileSdk

    const val versionCode = 1
    const val versionName = "0.1.0"
}
